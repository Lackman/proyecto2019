package Vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import com.toedter.calendar.JDateChooser;
import Controlador.ControladorPadre;
import Modelo.Txt;
import Modelo.TxtCalendario;
import Modelo.TxtNumero;
import Modelo.TxtTexto;

import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class AgregarCurso extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JLabel lblError;
	private ControladorPadre controlador;
	private JLabel lblNombre;
	private JLabel lblDesde;
	private JLabel lblHasta;
	private JLabel lblLimiteDeCupos;
	private JDateChooser dateInicio;
	private JDateChooser dateFinalizacion;
	private JButton btnHorarios;
	private JButton btnGuardar;
	private JButton btnCancelar;
	private JButton btnProfesores;
	private ArrayList<Txt> campos;
	private JTextField spLimiteDeCupos;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 * 
	 * @param controladorAgregarCurso
	 */

	public AgregarCurso(ControladorPadre controlador) {
		setResizable(false);
		setTitle("Cursos");
		this.setControlador(controlador);
		this.setCampos(new ArrayList<Txt>());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 338, 349);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setLocationRelativeTo(null);

		lblError = new JLabel("Los campos con un * son obligatorios");
		lblError.setBounds(5, 258, 322, 14);
		lblError.setVisible(false);
		lblError.setForeground(Color.RED);
		lblError.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblCa = new JLabel("Agregar Curso");
		lblCa.setBounds(10, 11, 317, 31);
		lblCa.setHorizontalAlignment(SwingConstants.CENTER);
		lblCa.setFont(new Font("Arial Black", Font.PLAIN, 14));

		dateInicio = new JDateChooser();
		dateInicio.setBounds(138, 100, 174, 20);
		dateInicio.setName("dateInicio");
		dateInicio.addFocusListener(getControlador());

		lblNombre = new JLabel("Nombre:*");
		lblNombre.setBounds(10, 66, 110, 20);
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));

		lblDesde = new JLabel("Inicio:*");
		lblDesde.setBounds(10, 100, 110, 20);
		lblDesde.setFont(new Font("Tahoma", Font.PLAIN, 13));

		lblHasta = new JLabel("Finalizacion:*");
		lblHasta.setBounds(10, 131, 110, 20);
		lblHasta.setFont(new Font("Tahoma", Font.PLAIN, 13));

		lblLimiteDeCupos = new JLabel("Limite de cupos:*");
		lblLimiteDeCupos.setBounds(10, 162, 110, 20);
		lblLimiteDeCupos.setFont(new Font("Tahoma", Font.PLAIN, 13));

		txtNombre = new JTextField();
		txtNombre.setBounds(138, 69, 174, 20);
		txtNombre.setName("txtNombre");
		txtNombre.setColumns(10);
		txtNombre.addFocusListener(getControlador());
		txtNombre.addKeyListener(getControlador());
		this.getCampos()
				.add(new TxtTexto(lblError, "Se debe ingresar un nombre", txtNombre, lblNombre, true, 1, 100, ""));

		dateFinalizacion = new JDateChooser();
		dateFinalizacion.setBounds(138, 131, 174, 20);
		dateFinalizacion.addFocusListener(getControlador());
		dateFinalizacion.setName("dateFinalizacion");

		this.getCampos().add(new TxtCalendario(lblError, "La fecha final debe ser mayor a la de inicio", dateInicio,
				dateFinalizacion, lblDesde, lblHasta));

		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(5, 282, 103, 31);
		btnGuardar.addActionListener(getControlador());

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(224, 282, 103, 31);
		btnCancelar.addActionListener(getControlador());

		SpinnerNumberModel rangoSpinner = new SpinnerNumberModel();
		rangoSpinner.setMaximum(100);
		rangoSpinner.setMinimum(0);

		btnHorarios = new JButton("Horarios");
		btnHorarios.setBounds(5, 224, 322, 23);

		btnProfesores = new JButton("Profesores");
		btnProfesores.setBounds(5, 193, 322, 23);

		spLimiteDeCupos = new JTextField();
		spLimiteDeCupos.setBounds(138, 163, 174, 20);
		spLimiteDeCupos.setColumns(10);
		this.getCampos().add(new TxtNumero(lblError, "Ingrese el limite de cupos", spLimiteDeCupos, lblLimiteDeCupos, 1, 2));
		contentPane.setLayout(null);
		contentPane.add(btnGuardar);
		contentPane.add(btnCancelar);
		contentPane.add(lblCa);
		contentPane.add(lblNombre);
		contentPane.add(txtNombre);
		contentPane.add(lblDesde);
		contentPane.add(lblHasta);
		contentPane.add(lblLimiteDeCupos);
		contentPane.add(dateFinalizacion);
		contentPane.add(dateInicio);
		contentPane.add(spLimiteDeCupos);
		contentPane.add(btnProfesores);
		contentPane.add(btnHorarios);
		contentPane.add(lblError);
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JLabel getLblError() {
		return lblError;
	}

	public void setLblError(JLabel lblError) {
		this.lblError = lblError;
	}

	public ControladorPadre getControlador() {
		return controlador;
	}

	public void getControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}

	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}

	public JLabel getLblNombre() {
		return lblNombre;
	}

	public void setLblNombre(JLabel lblNombre) {
		this.lblNombre = lblNombre;
	}

	public JTextField getSpLimiteDeCupos() {
		return spLimiteDeCupos;
	}

	public void setSpLimiteDeCupos(JTextField spLimiteDeCupos) {
		this.spLimiteDeCupos = spLimiteDeCupos;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JLabel getLblDesde() {
		return lblDesde;
	}

	public void setLblDesde(JLabel lblDesde) {
		this.lblDesde = lblDesde;
	}

	public JLabel getLblHasta() {
		return lblHasta;
	}

	public void setLblHasta(JLabel lblHasta) {
		this.lblHasta = lblHasta;
	}

	public JLabel getLblLimiteDeCupos() {
		return lblLimiteDeCupos;
	}

	public void setLblLimiteDeCupos(JLabel lblLimiteDeCupos) {
		this.lblLimiteDeCupos = lblLimiteDeCupos;
	}

	public JDateChooser getDateInicio() {
		return dateInicio;
	}

	public void setDateInicio(JDateChooser dateInicio) {
		this.dateInicio = dateInicio;
	}

	public JDateChooser getDateFinalizacion() {
		return dateFinalizacion;
	}

	public void setDateFinalizacion(JDateChooser dateFinalizacion) {
		this.dateFinalizacion = dateFinalizacion;
	}

	public JButton getBtnHorarios() {
		return btnHorarios;
	}

	public void setBtnHorarios(JButton btnHorarios) {
		this.btnHorarios = btnHorarios;
	}

	public JButton getBtnProfesores() {
		return btnProfesores;
	}

	public void setBtnProfesores(JButton btnProfesores) {
		this.btnProfesores = btnProfesores;
	}

	public ArrayList<Txt> getCampos() {
		return campos;
	}

	public void setCampos(ArrayList<Txt> campos) {
		this.campos = campos;
	}
}
