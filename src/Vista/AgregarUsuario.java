 package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorPadre;
import Modelo.Txt;
import Modelo.TxtCbx;
import Modelo.TxtContrasennaDoble;
import Modelo.TxtNumero;
import Modelo.TxtTexto;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Color;

public class AgregarUsuario extends JFrame {

	private JPanel contentPane;
	private JLabel lblAgregarUsuario;
	private JTextField txtTelefono;
	private JTextField txtDni;
	private JTextField txtApellido;
	private JTextField txtNombre;
	private JPasswordField txtContrasenia;
	private JPasswordField txtConfirmarContrasenia;
	private ControladorPadre controlador;
	private JComboBox cbxTipoUsuario;
	private JLabel lblGenero;
	private JComboBox cbxProvincia;
	private JComboBox cbxLocalidad;
	private JComboBox cbxGenero;
	private JComboBox cbxPregunta;
	private JButton btnGuardar;
	private JButton btnCancelar;
	private JTextField txtRespuesta;
	private JLabel lblError;
	private JLabel lblNombre;
	private JLabel lblApellido;
	private JLabel lblDni;
	private JLabel lblTipoUsuario;
	private JLabel lblTelefono;
	private JLabel lblProvincia;
	private JLabel lblLocalidad;
	private JLabel lblContrasenia;
	private JLabel lblConfirmarContrasenia;
	private JLabel lblPreguntaSecreta;
	private ArrayList<Txt> campos;
	private TxtCbx txtTipoUsuario;
	private TxtCbx txtProvincia;
	private TxtCbx tbxLocalidad;
	private TxtCbx tbxGenero;
	private TxtCbx tbxPregunta;
	/**
	 * /** Create the frame.
	 */
	public AgregarUsuario(ControladorPadre controlador) {
		setResizable(false);
		this.setCampos(new ArrayList<Txt>());
		this.setControlador(controlador);
		setTitle("Usuarios");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 420, 524);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setLocationRelativeTo(null);
		
		lblError = new JLabel("Los campos con un * son obligatorios");
		lblError.setBounds(5, 428, 404, 20);
		lblError.setVisible(false);
		lblError.setForeground(Color.RED);
		lblError.setHorizontalAlignment(SwingConstants.CENTER);

		lblAgregarUsuario = new JLabel("Agregar usuario");
		lblAgregarUsuario.setBounds(37, 5, 362, 37);
		lblAgregarUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblAgregarUsuario.setFont(new Font("Arial Black", Font.PLAIN, 15));

		lblNombre = new JLabel("Nombre:*");
		lblNombre.setBounds(5, 75, 151, 17);
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));

		lblApellido = new JLabel("Apellido:*");
		lblApellido.setBounds(5, 101, 151, 16);
		lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 12));

		lblDni = new JLabel("DNI:*");
		lblDni.setBounds(5, 49, 151, 16);
		lblDni.setFont(new Font("Tahoma", Font.PLAIN, 12));

		lblTipoUsuario = new JLabel("Tipo de usuario:*");
		lblTipoUsuario.setBounds(5, 128, 151, 16);
		lblTipoUsuario.setFont(new Font("Tahoma", Font.PLAIN, 12));

		cbxTipoUsuario = new JComboBox();
		cbxTipoUsuario.setBounds(166, 126, 233, 22);
		cbxTipoUsuario.addFocusListener(getControlador());
		cbxTipoUsuario.setName("cbxTipoUsuario");
		this.setTxtTipoUsuario(new TxtCbx(lblError, "Se debe seleccionar un tipo", cbxTipoUsuario, lblTipoUsuario));
		this.getCampos().add(this.getTxtTipoUsuario());

		lblTelefono = new JLabel("Telefono:*");
		lblTelefono.setBounds(5, 155, 151, 16);
		lblTelefono.setFont(new Font("Tahoma", Font.PLAIN, 12));

		lblContrasenia = new JLabel("Contrase\u00F1a:*");
		lblContrasenia.setBounds(5, 266, 151, 16);
		lblContrasenia.setFont(new Font("Tahoma", Font.PLAIN, 12));

		txtTelefono = new JTextField();
		txtTelefono.setBounds(166, 154, 233, 20);
		txtTelefono.setName("txtTelefono");
		txtTelefono.addFocusListener(getControlador());
		txtTelefono.addKeyListener(getControlador());
		txtTelefono.setColumns(10);
		this.getCampos().add(new TxtNumero(lblError, "Falta ingresar el tel�fono sin el 0 y sin el 15", txtTelefono, lblTelefono, 7, 10));
		
		txtDni = new JTextField();
		txtDni.setBounds(166, 48, 233, 20);
		txtDni.setName("txtDni");
		txtDni.addFocusListener(getControlador());
		txtDni.addKeyListener(getControlador());
		txtDni.setColumns(10);
		this.getCampos().add(new TxtNumero(lblError, "Dni ingresado no es correcto", txtDni, lblDni, 8, 8, "usuario", "dni", "estado", "Desea volver a reactivar al usuario?")); 
		
		txtApellido = new JTextField();
		txtApellido.setBounds(166, 100, 233, 20);
		txtApellido.setName("txtApellido");
		txtApellido.addFocusListener(getControlador());
		txtApellido.addKeyListener(getControlador());
		txtApellido.setColumns(10);
		this.getCampos().add(new TxtTexto(lblError, "Falta ingresar el apellido", txtApellido, lblApellido, true, 1, 100, ""));
		
		txtNombre = new JTextField();
		txtNombre.setBounds(166, 74, 233, 20);
		txtNombre.setName("txtNombre");
		txtNombre.addFocusListener(getControlador());
		txtNombre.addKeyListener(getControlador());
		txtNombre.setColumns(10);
		this.getCampos().add(new TxtTexto(lblError, "Falta ingresar el nombre", txtNombre, lblNombre, true, 1, 100, ""));
		
		txtContrasenia = new JPasswordField();
		txtContrasenia.setBounds(166, 264, 233, 22);
		txtContrasenia.setName("txtContrasenia");
		txtContrasenia.addFocusListener(getControlador());
		txtContrasenia.addKeyListener(getControlador());
		
		lblConfirmarContrasenia = new JLabel("Conf. contrase\u00F1a:*");
		lblConfirmarContrasenia.setBounds(5, 294, 151, 16);
		lblConfirmarContrasenia.setFont(new Font("Tahoma", Font.PLAIN, 12));

		txtConfirmarContrasenia = new JPasswordField();
		txtConfirmarContrasenia.setBounds(166, 292, 233, 22);
		txtConfirmarContrasenia.setName("txtConfirmarContrasenia");
		txtConfirmarContrasenia.addFocusListener(getControlador());
		txtConfirmarContrasenia.addKeyListener(getControlador());

		this.getCampos().add(new TxtContrasennaDoble(lblError, "Las contrase�as deben ser iguales y con un m�nimo de 4", txtContrasenia, txtConfirmarContrasenia, lblContrasenia, lblConfirmarContrasenia, true, 4, 100));

		
		lblGenero = new JLabel("Genero:*");
		lblGenero.setBounds(5, 238, 151, 16);
		lblGenero.setFont(new Font("Tahoma", Font.PLAIN, 12));

		cbxGenero = new JComboBox();
		cbxGenero.setBounds(166, 236, 233, 22);
		cbxGenero.setName("cbxGenero");
		cbxGenero.addFocusListener(getControlador());
		this.setTbxGenero(new TxtCbx(lblError, "Se debe seleccionar un genero", cbxGenero, lblGenero));
		this.getCampos().add(this.getTbxGenero());

		cbxPregunta = new JComboBox();
		cbxPregunta.setBounds(5, 364, 404, 22);
		cbxPregunta.addItemListener(getControlador());
		cbxPregunta.setName("cbxPregunta");
		cbxPregunta.addFocusListener(getControlador());

		lblPreguntaSecreta = new JLabel("Pregunta secreta:*");
		lblPreguntaSecreta.setBounds(5, 332, 394, 21);
		lblPreguntaSecreta.setHorizontalAlignment(SwingConstants.CENTER);
		lblPreguntaSecreta.setFont(new Font("Tahoma", Font.PLAIN, 12));

		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(5, 459, 89, 23);
		btnGuardar.addActionListener(getControlador());

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(320, 459, 89, 23);
		btnCancelar.addActionListener(getControlador());

		lblProvincia = new JLabel("Provincia:*");
		lblProvincia.setBounds(5, 182, 151, 16);
		lblProvincia.setFont(new Font("Tahoma", Font.PLAIN, 12));

		cbxProvincia = new JComboBox();
		cbxProvincia.setBounds(166, 180, 233, 22);
		cbxProvincia.addItemListener(getControlador());
		cbxProvincia.setName("cbxProvincia");
		cbxProvincia.addFocusListener(getControlador());

		lblLocalidad = new JLabel("Localidad:*");
		lblLocalidad.setBounds(5, 210, 151, 16);
		lblLocalidad.setFont(new Font("Tahoma", Font.PLAIN, 12));

		cbxLocalidad = new JComboBox();
		cbxLocalidad.setBounds(166, 208, 233, 22);
		cbxLocalidad.setEnabled(false);
		cbxLocalidad.setName("cbxLocalidad");
		cbxLocalidad.addFocusListener(getControlador());
		this.setTxtProvincia(new TxtCbx(lblError, "Se debe seleccionar una provincia", cbxProvincia, lblProvincia));
		this.getCampos().add(this.getTxtProvincia());	
		this.setTbxLocalidad(new TxtCbx(lblError, "Se debe seleccionar una ciudad", cbxLocalidad, cbxProvincia, lblLocalidad, true));
		this.getCampos().add(this.getTbxLocalidad());
		txtRespuesta = new JTextField();
		txtRespuesta.setEnabled(false);
		txtRespuesta.setBounds(5, 397, 404, 20);
		txtRespuesta.setName("txtRespuesta");
		txtRespuesta.addFocusListener(getControlador());
		txtRespuesta.addKeyListener(getControlador());
		contentPane.setLayout(null);
		txtRespuesta.setColumns(10);
		contentPane.add(txtRespuesta);
		contentPane.add(lblError);
		contentPane.add(lblConfirmarContrasenia);
		contentPane.add(lblContrasenia);
		contentPane.add(lblGenero);
		contentPane.add(lblLocalidad);
		contentPane.add(lblProvincia);
		contentPane.add(lblTelefono);
		contentPane.add(lblTipoUsuario);
		contentPane.add(lblApellido);
		contentPane.add(lblNombre);
		contentPane.add(lblDni);
		contentPane.add(txtNombre);
		contentPane.add(cbxProvincia);
		contentPane.add(txtApellido);
		contentPane.add(cbxTipoUsuario);
		contentPane.add(txtTelefono);
		contentPane.add(cbxLocalidad);
		contentPane.add(cbxGenero);
		contentPane.add(txtContrasenia);
		contentPane.add(txtConfirmarContrasenia);
		contentPane.add(txtDni);
		contentPane.add(lblAgregarUsuario);
		contentPane.add(btnGuardar);
		contentPane.add(btnCancelar);
		contentPane.add(lblPreguntaSecreta);
		contentPane.add(cbxPregunta);
		TxtTexto t = new TxtTexto(lblError, "Ingres una respuesta valida", txtRespuesta, null, "");
		this.setTbxPregunta(new TxtCbx(lblError, "Seleccione una pregunte", cbxPregunta, t, lblPreguntaSecreta, true));
		this.getCampos().add(this.getTbxPregunta());
		this.getCampos().add(t);
	}

	public JComboBox getCbxTipoUsuario() {
		return cbxTipoUsuario;
	}

	public void setCbxTipoUsuario(JComboBox cbxTipoUsuario) {
		this.cbxTipoUsuario = cbxTipoUsuario;
	}

	public JLabel getLblAgregarUsuario() {
		return lblAgregarUsuario;
	}

	public void setLblAgregarUsuario(JLabel lblAgregarUsuario) {
		this.lblAgregarUsuario = lblAgregarUsuario;
	}

	public ControladorPadre getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	public JTextField getTxtDni() {
		return txtDni;
	}

	public void setTxtDni(JTextField txtDni) {
		this.txtDni = txtDni;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JPasswordField getTxtContrasenia() {
		return txtContrasenia;
	}

	public void setTxtContrasenia(JPasswordField txtContrasenia) {
		this.txtContrasenia = txtContrasenia;
	}

	public JPasswordField getTxtConfirmarContrasenia() {
		return txtConfirmarContrasenia;
	}

	public void setTxtConfirmarContrasenia(JPasswordField txtConfirmarContrasenia) {
		this.txtConfirmarContrasenia = txtConfirmarContrasenia;
	}

	public JLabel getLblGenero() {
		return lblGenero;
	}

	public void setLblGenero(JLabel lblGenero) {
		this.lblGenero = lblGenero;
	}

	public JComboBox getCbxProvincia() {
		return cbxProvincia;
	}

	public void setCbxProvincia(JComboBox cbxProvincia) {
		this.cbxProvincia = cbxProvincia;
	}

	public JComboBox getCbxLocalidad() {
		return cbxLocalidad;
	}

	public void setCbxLocalidad(JComboBox cbxLocalidad) {
		this.cbxLocalidad = cbxLocalidad;
	}

	public JComboBox getCbxGenero() {
		return cbxGenero;
	}

	public void setCbxGenero(JComboBox cbxGenero) {
		this.cbxGenero = cbxGenero;
	}

	public JComboBox getCbxPregunta() {
		return cbxPregunta;
	}

	public void setCbxPregunta(JComboBox cbxPregunta) {
		this.cbxPregunta = cbxPregunta;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JTextField getTxtRespuesta() {
		return txtRespuesta;
	}

	public void setTxtRespuesta(JTextField txtRespuesta) {
		this.txtRespuesta = txtRespuesta;
	}

	public JLabel getLblError() {
		return lblError;
	}

	public void setLblError(JLabel lblError) {
		this.lblError = lblError;
	}

	public JLabel getLblNombre() {
		return lblNombre;
	}

	public void setLblNombre(JLabel lblNombre) {
		this.lblNombre = lblNombre;
	}

	public JLabel getLblApellido() {
		return lblApellido;
	}

	public void setLblApellido(JLabel lblApellido) {
		this.lblApellido = lblApellido;
	}

	public JLabel getLblDni() {
		return lblDni;
	}

	public void setLblDni(JLabel lblDni) {
		this.lblDni = lblDni;
	}

	public JLabel getLblTipoUsuario() {
		return lblTipoUsuario;
	}

	public void setLblTipoUsuario(JLabel lblTipoUsuario) {
		this.lblTipoUsuario = lblTipoUsuario;
	}

	public JLabel getLblTelefono() {
		return lblTelefono;
	}

	public void setLblTelefono(JLabel lblTelefono) {
		this.lblTelefono = lblTelefono;
	}

	public JLabel getLblProvincia() {
		return lblProvincia;
	}

	public void setLblProvincia(JLabel lblProvincia) {
		this.lblProvincia = lblProvincia;
	}

	public JLabel getLblLocalidad() {
		return lblLocalidad;
	}

	public void setLblLocalidad(JLabel lblLocalidad) {
		this.lblLocalidad = lblLocalidad;
	}

	public JLabel getLblPreguntaSecreta() {
		return lblPreguntaSecreta;
	}

	public void setLblPreguntaSecreta(JLabel lblPreguntaSecreta) {
		this.lblPreguntaSecreta = lblPreguntaSecreta;
	}

	public JLabel getLblContrasenia() {
		return lblContrasenia;
	}

	public void setLblContrasenia(JLabel lblContrasenia) {
		this.lblContrasenia = lblContrasenia;
	}

	public JLabel getLblConfirmarContrasenia() {
		return lblConfirmarContrasenia;
	}

	public void setLblConfirmarContrasenia(JLabel lblConfirmarContrasenia) {
		this.lblConfirmarContrasenia = lblConfirmarContrasenia;
	}


	public ArrayList<Txt> getCampos() {
		return campos;
	}

	public void setCampos(ArrayList<Txt> campos) {
		this.campos = campos;
	}

	public TxtCbx getTxtTipoUsuario() {
		return txtTipoUsuario;
	}

	public TxtCbx getTxtProvincia() {
		return txtProvincia;
	}

	public TxtCbx getTbxLocalidad() {
		return tbxLocalidad;
	}

	public TxtCbx getTbxGenero() {
		return tbxGenero;
	}

	public TxtCbx getTbxPregunta() {
		return tbxPregunta;
	}

	public void setTxtTipoUsuario(TxtCbx txtTipoUsuario) {
		this.txtTipoUsuario = txtTipoUsuario;
	}

	public void setTxtProvincia(TxtCbx txtProvincia) {
		this.txtProvincia = txtProvincia;
	}

	public void setTbxLocalidad(TxtCbx tbxLocalidad) {
		this.tbxLocalidad = tbxLocalidad;
	}

	public void setTbxGenero(TxtCbx tbxGenero) {
		this.tbxGenero = tbxGenero;
	}

	public void setTbxPregunta(TxtCbx tbxPregunta) {
		this.tbxPregunta = tbxPregunta;
	}
}
