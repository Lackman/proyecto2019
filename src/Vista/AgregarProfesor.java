package Vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class AgregarProfesor extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private JButton btnAgregar;
	private JButton btnQuitar;
	private JTable tableProfesores;
	private JTable tableProfesoresAgregados;
	private JButton btnGuardar;
	private JButton btnCancelar;

	public AgregarProfesor() {
		setResizable(false);
		setTitle("Profesores");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 726, 409);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(410, 47, 284, 278);
		contentPane.add(scrollPane_1);

		tableProfesoresAgregados = new JTable();
		tableProfesoresAgregados.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nombre", "Apellido"
			}
		));
		scrollPane_1.setViewportView(tableProfesoresAgregados);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 49, 284, 278);
		contentPane.add(scrollPane);

		tableProfesores = new JTable();
		scrollPane.setViewportView(tableProfesores);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(304, 93, 96, 23);
		contentPane.add(btnAgregar);

		btnQuitar = new JButton("Quitar");
		btnQuitar.setBounds(304, 127, 96, 23);
		contentPane.add(btnQuitar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(10, 338, 89, 23);
		contentPane.add(btnGuardar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(605, 336, 89, 23);
		contentPane.add(btnCancelar);
	}

/*	public JTextField getTxtBuscar() {
		return txtBuscar;
	}

	public void setTxtBuscar(JTextField txtBuscar) {
		this.txtBuscar = txtBuscar;
	}*/

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnQuitar() {
		return btnQuitar;
	}

	public void setBtnQuitar(JButton btnQuitar) {
		this.btnQuitar = btnQuitar;
	}

	public JTable getTableProfesores() {
		return tableProfesores;
	}

	public void setTableProfesores(JTable tableProfesores) {
		this.tableProfesores = tableProfesores;
	}

	public JTable getTableProfesoresAgregados() {
		return tableProfesoresAgregados;
	}

	public void setTableProfesoresAgregados(JTable tableProfesoresAgregados) {
		this.tableProfesoresAgregados = tableProfesoresAgregados;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}
}
