package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import Controlador.ControladorPadre;
import Modelo.Txt;
import Modelo.TxtCbx;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class VentanaProfesor extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JComboBox comboBox;
	private JLabel lblCurso;
	private JScrollPane scrollPane;
	private JLabel lblListadoDeAlumnos;
	private JMenu mnGestion;
	private JMenuItem mntmSalir;
	private ControladorPadre controlador;
	private JButton btnCalificar;
	private JButton btnReporte;
	private ArrayList<Txt> campos;
	private JLabel lblError;
	private TxtCbx txtMateria;

	/**
	 * Launch the application.
	 */
	/**
	 * Create the frame.
	 */
	public VentanaProfesor(ControladorPadre controlador) {
		setResizable(false);
		setTitle("Profesor");
		this.setCampos(new ArrayList<Txt>());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 566, 371);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);

		lblCurso = new JLabel("Curso:");
		lblCurso.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurso.setBounds(10, 28, 66, 24);
		contentPane.add(lblCurso);

		comboBox = new JComboBox();
		comboBox.setBounds(89, 31, 220, 21);
		contentPane.add(comboBox);
		comboBox.addFocusListener(getControlador());
		this.setTxtMateria(new TxtCbx(lblError, "", comboBox, lblCurso));
		this.getCampos().add(getTxtMateria());
		
		lblListadoDeAlumnos = new JLabel("Listado de alumnos:");
		lblListadoDeAlumnos.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblListadoDeAlumnos.setBounds(10, 53, 139, 24);
		contentPane.add(lblListadoDeAlumnos);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 88, 517, 213);
		contentPane.add(scrollPane);

		table = new JTable();
		this.getTable().getTableHeader().setReorderingAllowed(false);
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Nombre", "Apellido", "Dni", "Nota" }));
		scrollPane.setViewportView(table);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 550, 21);
		contentPane.add(menuBar);

		mnGestion = new JMenu("Gestion");
		menuBar.add(mnGestion);

		mntmSalir = new JMenuItem("Salir");
		mnGestion.add(mntmSalir);

		btnCalificar = new JButton("Calificar");
		btnCalificar.addActionListener(getControlador());
		btnCalificar.setEnabled(false);
		btnCalificar.setBounds(433, 30, 89, 23);
		contentPane.add(btnCalificar);

		btnReporte = new JButton("Reporte");
		btnReporte.addActionListener(getControlador());
		btnReporte.setEnabled(false);
		btnReporte.setBounds(334, 30, 89, 23);
		contentPane.add(btnReporte);

	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	public JLabel getLblCurso() {
		return lblCurso;
	}

	public void setLblCurso(JLabel lblCurso) {
		this.lblCurso = lblCurso;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JLabel getLblListadoDeAlumnos() {
		return lblListadoDeAlumnos;
	}

	public void setLblListadoDeAlumnos(JLabel lblListadoDeAlumnos) {
		this.lblListadoDeAlumnos = lblListadoDeAlumnos;
	}

	public JMenuItem getMntmSalir() {
		return mntmSalir;
	}

	public void setMntmSalir(JMenuItem mntmSalir) {
		this.mntmSalir = mntmSalir;
	}

	public ControladorPadre getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnCalificar() {
		return btnCalificar;
	}

	public void setBtnCalificar(JButton btnCalificar) {
		this.btnCalificar = btnCalificar;
	}

	public JButton getBtnReporte() {
		return btnReporte;
	}

	public void setBtnReporte(JButton btnReporte) {
		this.btnReporte = btnReporte;
	}

	public ArrayList<Txt> getCampos() {
		return campos;
	}

	public void setCampos(ArrayList<Txt> campos) {
		this.campos = campos;
	}

	public JLabel getLblError() {
		return lblError;
	}

	public void setLblError(JLabel lblError) {
		this.lblError = lblError;
	}

	public TxtCbx getTxtMateria() {
		return txtMateria;
	}

	public void setTxtMateria(TxtCbx txtMateria) {
		this.txtMateria = txtMateria;
	}
}
