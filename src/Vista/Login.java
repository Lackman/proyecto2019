package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorPadre;
import Modelo.Txt;
import Modelo.TxtContrasennaSimple;
import Modelo.TxtNumero;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.SwingConstants;

public class Login extends JFrame {
	private JButton btnIniciarSesion;
	private ControladorPadre controlador;
	private JLabel lblUsuario;
	private JLabel lblContrasenia;
	private JLabel lblContrseaIncorrecta;
	private JButton btnRecuperarContraseña;
	private JPanel contentPane;
	private ArrayList<Txt> campos;
	private JTextField txtUsuario;
	private JPasswordField txtContrasenia;

	/**
	 * Create the frame.
	 * 
	 * @param controlador
	 */

	public Login(ControladorPadre controlador) {
		this.setControlador(controlador);
		this.setCampos(new ArrayList<Txt>());
		setTitle("Sistema de Cursos");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 392, 207);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);
		
		lblContrseaIncorrecta = new JLabel("Contrase\u00F1a o Usuario incorrecta");
		lblContrseaIncorrecta.setVisible(false);
		lblContrseaIncorrecta.setHorizontalAlignment(SwingConstants.CENTER);
		lblContrseaIncorrecta.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblContrseaIncorrecta.setForeground(Color.RED);
		lblContrseaIncorrecta.setBounds(10, 114, 366, 14);
		contentPane.add(lblContrseaIncorrecta);

		JLabel lblIniciarSesion = new JLabel("Iniciar Sesión");
		lblIniciarSesion.setHorizontalAlignment(SwingConstants.CENTER);
		lblIniciarSesion.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblIniciarSesion.setBounds(10, 11, 346, 30);
		contentPane.add(lblIniciarSesion);

		lblUsuario = new JLabel("Dni:");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblUsuario.setBounds(10, 52, 104, 20);
		contentPane.add(lblUsuario);

		lblContrasenia = new JLabel("Contrase\u00F1a:");
		lblContrasenia.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblContrasenia.setBounds(10, 83, 96, 20);
		contentPane.add(lblContrasenia);
		btnIniciarSesion = new JButton("Iniciar Sesion");
		btnIniciarSesion.addActionListener(getControlador());
		btnIniciarSesion.setToolTipText("");

		btnIniciarSesion.setBounds(10, 139, 182, 30);
		contentPane.add(btnIniciarSesion);

		btnRecuperarContraseña = new JButton("Reestablecer Contrase\u00F1a");
		btnRecuperarContraseña.addActionListener(getControlador());
		btnRecuperarContraseña.setBounds(194, 139, 182, 30);
		contentPane.add(btnRecuperarContraseña);
		
		txtUsuario = new JTextField();
		txtUsuario.setHorizontalAlignment(SwingConstants.LEFT);
		txtUsuario.setBounds(116, 52, 249, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtContrasenia = new JPasswordField();
		txtContrasenia.setBounds(116, 84, 249, 20);
		contentPane.add(txtContrasenia);
		this.getCampos().add(new TxtNumero(lblContrseaIncorrecta, "Ingrese un DNI válido", txtUsuario, lblUsuario, 8, 8));
		this.getCampos().add(new TxtContrasennaSimple(lblContrseaIncorrecta, "Ingrese una contraseña", txtContrasenia, lblContrasenia, true, 1,100));
	}

	public JButton getBtnIniciarSesion() {
		return btnIniciarSesion;
	}

	public void setBtnIniciarSesion(JButton btnIniciarSesion) {
		this.btnIniciarSesion = btnIniciarSesion;
	}

	public ControladorPadre getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public JPasswordField getTxtContrasenia() {
		return txtContrasenia;
	}

	public void setTxtContrasenia(JPasswordField txtContrasenia) {
		this.txtContrasenia = txtContrasenia;
	}

	public JLabel getLblUsuario() {
		return lblUsuario;
	}

	public void setLblUsuario(JLabel lblUsuario) {
		this.lblUsuario = lblUsuario;
	}

	public JLabel getLblContrasenia() {
		return lblContrasenia;
	}

	public void setLblContrasenia(JLabel lblContrasenia) {
		this.lblContrasenia = lblContrasenia;
	}

	public JLabel getLblContrseaIncorrecta() {
		return lblContrseaIncorrecta;
	}

	public void setLblContrseaIncorrecta(JLabel lblContrseaIncorrecta) {
		this.lblContrseaIncorrecta = lblContrseaIncorrecta;
	}

	public JButton getBtnRecuperarContraseña() {
		return btnRecuperarContraseña;
	}

	public void setBtnRecuperarContraseña(JButton btnRecuperarContraseña) {
		this.btnRecuperarContraseña = btnRecuperarContraseña;
	}

	public ArrayList<Txt> getCampos() {
		return campos;
	}

	public void setCampos(ArrayList<Txt> campos) {
		this.campos = campos;
	}
}
