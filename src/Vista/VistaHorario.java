package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Controlador.ControladorPadre;
import Modelo.Txt;
import Modelo.TxtCbx;
import Modelo.TxtHorario;
import Modelo.TxtTexto;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.JLabel;
import javax.swing.JSpinner;

public class VistaHorario extends JFrame {

	private JPanel contentPane;
	private JTable tableHorario;
	private JButton btnGuardar;
	private JButton btnQuitar;
	private ControladorPadre controlador;
	private JButton btnAgregar;
	private JTextField txtLugar;
	private JButton btnCancelar;
	private JComboBox cbxDiaSemana;
	private JSpinner spHoraInicio;
	private JSpinner spHoraFin;
	private JLabel lblDia;
	private JLabel lblHorarioDeInicio;
	private JLabel lblHoraDeFinalizacin;
	private JLabel lblLugarDeLa;
	private JScrollPane scrollPane;
	private ArrayList<Txt> campos;
	private TxtHorario txtHorario; 

	/**
	 * Create the frame.
	 */
	public VistaHorario(ControladorPadre controlador) {
		setResizable(false);
		this.setControlador(controlador);
		this.setCampos(new ArrayList<Txt>());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 541, 409);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 154, 515, 174);
		contentPane.add(scrollPane);

		tableHorario = new JTable();
		tableHorario.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "D\u00EDa", "Hora de Inicio", "Horario de Finalizaci\u00F3n", "Lugar" }));
		tableHorario.getColumnModel().getColumn(1).setPreferredWidth(114);
		tableHorario.getColumnModel().getColumn(2).setPreferredWidth(126);
		scrollPane.setViewportView(tableHorario);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(150, 120, 170, 23);
		btnAgregar.addActionListener(getControlador());
		contentPane.add(btnAgregar);

		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(20, 339, 89, 23);
		btnGuardar.addActionListener(getControlador());
		contentPane.add(btnGuardar);

		btnQuitar = new JButton("Quitar");
		btnQuitar.setBounds(355, 120, 170, 23);
		btnQuitar.addActionListener(getControlador());
		contentPane.add(btnQuitar);

		cbxDiaSemana = new JComboBox();
		cbxDiaSemana.setName("cbxDiaSemana");
		cbxDiaSemana.addFocusListener(getControlador());
		cbxDiaSemana.setModel(new DefaultComboBoxModel(
				new String[] { "Seleccione un d�a","Lunes", "Martes", "M\u00EDercoles", "Jueves", "Viernes", "Sabado", "Domingo" }));
		cbxDiaSemana.setBounds(150, 11, 375, 23);
		contentPane.add(cbxDiaSemana);
		this.getCampos().add(new TxtCbx(null, "Seleccione una fecha", getCbxDiaSemana(), lblDia));
		
		lblDia = new JLabel("D\u00EDa de la semana:");
		lblDia.setBounds(10, 15, 130, 14);
		contentPane.add(lblDia);

		lblHorarioDeInicio = new JLabel("Horario de inicio:");
		lblHorarioDeInicio.setBounds(10, 40, 130, 14);
		contentPane.add(lblHorarioDeInicio);

		lblHoraDeFinalizacin = new JLabel("Hora de finalizaci\u00F3n");
		lblHoraDeFinalizacin.setBounds(10, 65, 130, 14);
		contentPane.add(lblHoraDeFinalizacin);

		lblLugarDeLa = new JLabel("Lugar de la actividad:");
		lblLugarDeLa.setBounds(10, 90, 130, 14);
		contentPane.add(lblLugarDeLa);

		txtLugar = new JTextField();
		txtLugar.setName("txtLugar");
		txtLugar.setBounds(150, 85, 375, 24);
		contentPane.add(txtLugar);
		txtLugar.setColumns(10);
		txtLugar.addFocusListener(getControlador());
		Date date = new Date();
		SpinnerDateModel spinnerModelInicio = new SpinnerDateModel(date, null, null, Calendar.HOUR_OF_DAY);
		spHoraInicio = new JSpinner(spinnerModelInicio);
		spHoraInicio.setName("spHoraInicio");
		JSpinner.DateEditor txtHoraInicio = (JSpinner.DateEditor) spHoraInicio.getEditor();
		txtHoraInicio.getTextField().addFocusListener(getControlador());
		spHoraInicio.setBounds(150, 35, 375, 24);
		JSpinner.DateEditor de_spHoraInicio = new JSpinner.DateEditor(spHoraInicio, "HH:mm");
		
		spHoraInicio.setEditor(de_spHoraInicio);
		contentPane.add(spHoraInicio);
		this.getCampos().add(new TxtTexto(null, "Debe ingresar un lugar", txtLugar, lblLugarDeLa, true, 1, 50,""));

		SpinnerDateModel spinnerModelFin = new SpinnerDateModel(date, null, null, Calendar.HOUR_OF_DAY);
		spHoraFin = new JSpinner(spinnerModelFin);
		spHoraFin.setName("spHoraFin");
		spHoraFin.addFocusListener(getControlador());
		spHoraFin.setBounds(150, 60, 375, 24);
		JSpinner.DateEditor de_spHoraFin = new JSpinner.DateEditor(spHoraFin, "HH:mm");
		spHoraFin.setEditor(de_spHoraFin);
		contentPane.add(spHoraFin);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(422, 339, 89, 23);
		btnCancelar.addActionListener(getControlador());
		contentPane.add(btnCancelar);
		this.setTxtHorario(new TxtHorario(null, "Ingrese un horario correcto", spHoraInicio, spHoraFin, lblHorarioDeInicio, lblHoraDeFinalizacin));
		this.getCampos().add(this.getTxtHorario());
	}

	public JTable getTableHorario() {
		return tableHorario;
	}

	public void setTableHorario(JTable tableHorario) {
		this.tableHorario = tableHorario;
	}

	public JButton getBtnModificar() {
		return btnGuardar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnGuardar = btnModificar;
	}

	public JButton getBtnBorrar() {
		return btnQuitar;
	}

	public void setBtnBorrar(JButton btnBorrar) {
		this.btnQuitar = btnBorrar;
	}

	public ControladorPadre getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnNuevo() {
		return btnAgregar;
	}

	public void setBtnNuevo(JButton btnNuevo) {
		this.btnAgregar = btnNuevo;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnQuitar() {
		return btnQuitar;
	}

	public void setBtnQuitar(JButton btnQuitar) {
		this.btnQuitar = btnQuitar;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JTextField getTxtLugar() {
		return txtLugar;
	}

	public void setTxtLugar(JTextField txtLugar) {
		this.txtLugar = txtLugar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JComboBox getCbxDiaSemana() {
		return cbxDiaSemana;
	}

	public void setCbxDiaSemana(JComboBox cbxDiaSemana) {
		this.cbxDiaSemana = cbxDiaSemana;
	}

	public JSpinner getSpHoraInicio() {
		return spHoraInicio;
	}

	public void setSpHoraInicio(JSpinner spHoraInicio) {
		this.spHoraInicio = spHoraInicio;
	}

	public JSpinner getSpHoraFin() {
		return spHoraFin;
	}

	public void setSpHoraFin(JSpinner spHoraFin) {
		this.spHoraFin = spHoraFin;
	}

	public JLabel getLblDia() {
		return lblDia;
	}

	public void setLblDia(JLabel lblDia) {
		this.lblDia = lblDia;
	}

	public JLabel getLblHorarioDeInicio() {
		return lblHorarioDeInicio;
	}

	public void setLblHorarioDeInicio(JLabel lblHorarioDeInicio) {
		this.lblHorarioDeInicio = lblHorarioDeInicio;
	}

	public JLabel getLblHoraDeFinalizacin() {
		return lblHoraDeFinalizacin;
	}

	public void setLblHoraDeFinalizacin(JLabel lblHoraDeFinalizacin) {
		this.lblHoraDeFinalizacin = lblHoraDeFinalizacin;
	}

	public JLabel getLblLugarDeLa() {
		return lblLugarDeLa;
	}

	public void setLblLugarDeLa(JLabel lblLugarDeLa) {
		this.lblLugarDeLa = lblLugarDeLa;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public ArrayList<Txt> getCampos() {
		return campos;
	}

	public void setCampos(ArrayList<Txt> campos) {
		this.campos = campos;
	}

	public TxtHorario getTxtHorario() {
		return txtHorario;
	}

	public void setTxtHorario(TxtHorario txtHorario) {
		this.txtHorario = txtHorario;
	}
}
