package Vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorPadre;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class VistaCambioContrasenia extends JFrame {

	private JPanel contentPane;
	private JPasswordField txtContrasenia;
	private JPasswordField txtConfirmarContrasenia;
	private ControladorPadre controlador;
	private JButton btnCancelar;
	private JButton btnGuardar;
	private JLabel lblContrasenia;
	private JLabel lblConfirmarContrasenia;
	private JLabel lblError;
	private JLabel lblError2;
	/**
	 * Create the frame.
	 */
	public VistaCambioContrasenia(ControladorPadre controlador) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 407, 264);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblContrasenia = new JLabel("Contrase\u00F1a:*");
		lblContrasenia.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblContrasenia.setBounds(10, 67, 94, 25);
		contentPane.add(lblContrasenia);
		
		lblConfirmarContrasenia = new JLabel("Conf. contrase\u00F1a:*");
		lblConfirmarContrasenia.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblConfirmarContrasenia.setBounds(10, 113, 112, 21);
		contentPane.add(lblConfirmarContrasenia);
		
		txtContrasenia = new JPasswordField();
		txtContrasenia.setName("txtContrasenia");
		txtContrasenia.addFocusListener(getControlador());
		txtContrasenia.addKeyListener(getControlador());
		txtContrasenia.setBounds(114, 70, 197, 22);
		contentPane.add(txtContrasenia);
		
		txtConfirmarContrasenia = new JPasswordField();
		txtConfirmarContrasenia.setName("txtConfirmarContrasenia");
		txtConfirmarContrasenia.addFocusListener(getControlador());
		txtConfirmarContrasenia.addKeyListener(getControlador());
		txtConfirmarContrasenia.setBounds(114, 114, 197, 22);
		contentPane.add(txtConfirmarContrasenia);
		
		JLabel lblNuevaTitulo = new JLabel("Nueva Contrase\u00F1a");
		lblNuevaTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblNuevaTitulo.setFont(new Font("Arial Black", Font.PLAIN, 15));
		lblNuevaTitulo.setBounds(10, 11, 304, 37);
		contentPane.add(lblNuevaTitulo);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(getControlador());
		btnGuardar.setBounds(123, 191, 89, 23);
		contentPane.add(btnGuardar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(getControlador());
		btnCancelar.setBounds(222, 191, 89, 23);
		contentPane.add(btnCancelar);
		
		lblError = new JLabel("Los campos con un * son obligatorios");
		lblError.setForeground(Color.RED);
		lblError.setBounds(114, 145, 271, 14);
		contentPane.add(lblError);
		lblError.setVisible(false);
		
		lblError2 = new JLabel("Error2");
		lblError2.setBounds(114, 160, 175, 14);
		contentPane.add(lblError2);
		lblError2.setVisible(false);
	}
	public JPasswordField getTxtContrasenia() {
		return txtContrasenia;
	}
	public void setTxtContrasenia(JPasswordField txtContrasenia) {
		this.txtContrasenia = txtContrasenia;
	}
	public JPasswordField getTxtConfirmarContrasenia() {
		return txtConfirmarContrasenia;
	}
	public void setTxtConfirmarContrasenia(JPasswordField txtConfirmarContrasenia) {
		this.txtConfirmarContrasenia = txtConfirmarContrasenia;
	}
	public JButton getBtnCancelar() {
		return btnCancelar;
	}
	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}
	public JButton getBtnGuardar() {
		return btnGuardar;
	}
	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}
	public JLabel getLblContrasenia() {
		return lblContrasenia;
	}
	public void setLblContrasenia(JLabel lblContrasenia) {
		this.lblContrasenia = lblContrasenia;
	}
	public JLabel getLblConfirmarContrasenia() {
		return lblConfirmarContrasenia;
	}
	public void setLblConfirmarContrasenia(JLabel lblConfirmarContrasenia) {
		this.lblConfirmarContrasenia = lblConfirmarContrasenia;
	}
	public ControladorPadre getControlador() {
		return controlador;
	}
	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}
	public JLabel getLblError() {
		return lblError;
	}
	public void setLblError(JLabel lblError) {
		this.lblError = lblError;
	}
	public JLabel getLblError2() {
		return lblError2;
	}
	public void setLblError2(JLabel lblError2) {
		this.lblError2 = lblError2;
	}
	
}
