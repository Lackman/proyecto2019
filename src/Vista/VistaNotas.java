package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorPadre;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;

public class VistaNotas extends JFrame {

	private JPanel contentPane;
	private JButton btnCancelar;
	private JButton btnAceptar;
	private JTextField textCurso;
	private JTextField textNombre;
	private JTextField textApellido;
	private JTextField textCalificación;
	private JLabel lblNota;
	private ControladorPadre controlador;

	/**
	 * Create the frame.
	 */
	public VistaNotas() {
		setResizable(false);
		setTitle("NOTAS");
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 311, 247);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(167, 164, 89, 23);
		contentPane.add(btnCancelar);
		btnCancelar.addActionListener(getControlador());
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(49, 164, 89, 23);
		contentPane.add(btnAceptar);
		btnAceptar.addActionListener(getControlador());
		
		JLabel lblCurso = new JLabel("Curso:");
		lblCurso.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCurso.setBounds(10, 11, 48, 25);
		contentPane.add(lblCurso);
		
		textCurso = new JTextField();
		textCurso.setEditable(false);
		textCurso.setEnabled(false);
		textCurso.setBounds(81, 8, 203, 20);
		contentPane.add(textCurso);
		textCurso.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNombre.setBounds(10, 40, 94, 25);
		contentPane.add(lblNombre);
		
		textNombre = new JTextField();
		textNombre.setEditable(false);
		textNombre.setEnabled(false);
		textNombre.setName("txtNombre");
		textNombre.setColumns(10);
		textNombre.setBounds(81, 43, 203, 20);
		contentPane.add(textNombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblApellido.setBounds(10, 78, 65, 25);
		contentPane.add(lblApellido);
		
		textApellido = new JTextField();
		textApellido.setEditable(false);
		textApellido.setEnabled(false);
		textApellido.setName("txtApellido");
		textApellido.setColumns(10);
		textApellido.setBounds(81, 81, 201, 20);
		contentPane.add(textApellido);
		
		lblNota = new JLabel("Calificaci\u00F3n:");
		lblNota.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNota.setBounds(10, 118, 77, 25);
		contentPane.add(lblNota);
		
		textCalificación = new JTextField();
		textCalificación.setColumns(10);
		textCalificación.setBounds(81, 121, 201, 20);
		contentPane.add(textCalificación);
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JTextField getTextCurso() {
		return textCurso;
	}

	public void setTextCurso(JTextField textCurso) {
		this.textCurso = textCurso;
	}

	public JTextField getTextNombre() {
		return textNombre;
	}

	public void setTextNombre(JTextField textNombre) {
		this.textNombre = textNombre;
	}

	public JTextField getTextApellido() {
		return textApellido;
	}

	public void setTextApellido(JTextField textApellido) {
		this.textApellido = textApellido;
	}

	public JTextField getTextCalificación() {
		return textCalificación;
	}

	public void setTextCalificación(JTextField textCalificación) {
		this.textCalificación = textCalificación;
	}

	public JLabel getLblNota() {
		return lblNota;
	}

	public void setLblNota(JLabel lblNota) {
		this.lblNota = lblNota;
	}

	public ControladorPadre getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}
}
