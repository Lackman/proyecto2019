package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorPadre;
public class VistaModificarUsuario extends AgregarUsuario{
	
	public VistaModificarUsuario(ControladorPadre controlador) {
		super(controlador);
		/*super.getTxtRespuesta().setEnabled(false);
		super.getCbxPregunta().setEnabled(false);
		super.getLblPreguntaSecreta().setEnabled(false);
		super.getLblConfirmarContrasenia().setEnabled(false);
		super.getLblContrasenia().setEnabled(false);
		super.getTxtConfirmarContrasenia().setEnabled(false);
		super.getTxtContrasenia().setEnabled(false);*/
		super.getTxtDni().setEnabled(false);
		super.getLblAgregarUsuario().setText("Modificar usuario");
	
	}
}