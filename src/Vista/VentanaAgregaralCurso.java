package Vista;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import Controlador.ControladorPadre;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;

public class VentanaAgregaralCurso extends JFrame {

	private JPanel contentPane;
	private JTable tabladeCurso;
	private JButton btnAgregar;
	private ControladorPadre controlador;
	
	
	public VentanaAgregaralCurso(ControladorPadre controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 522, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 486, 188);
		contentPane.add(scrollPane);
		
		tabladeCurso = new JTable();
		scrollPane.setViewportView(tabladeCurso);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setEnabled(false);
		btnAgregar.setBounds(228, 210, 89, 23);
		contentPane.add(btnAgregar);
	}

	public JTable getTabladeCurso() {
		return tabladeCurso;
	}

	public void setTabladeCurso(JTable tabladeCurso) {
		this.tabladeCurso = tabladeCurso;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public ControladorPadre getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}
	
	
}
