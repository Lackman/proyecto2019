package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.ControladorPadre;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class VistaRecuperarContraseņa extends JFrame {

	private JPanel contentPane;
	private JTextField txtRespuesta;
	private ControladorPadre controlador;
	private JComboBox cbxPregunta;
	private JButton btnAceptar;
	private JButton btnCancelar;

	/**
	 * Create the frame.
	 */
	public VistaRecuperarContraseņa(ControladorPadre controlador) {
		setResizable(false);
		this.setControlador(controlador);
		setTitle("Recuperar Contrase\u00F1a");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 468, 211);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		cbxPregunta = new JComboBox();
		cbxPregunta.addItemListener(getControlador());
		cbxPregunta.setName("cbxPregunta");
		cbxPregunta.addFocusListener(getControlador());
		cbxPregunta.setBounds(72, 46, 304, 22);
		contentPane.add(cbxPregunta);
		
		txtRespuesta = new JTextField();
		txtRespuesta.setName("txtRespuesta");
		txtRespuesta.setEditable(false);
		txtRespuesta.setColumns(10);
		txtRespuesta.setBounds(72, 104, 304, 20);
		contentPane.add(txtRespuesta);
		
		JLabel lblPreguntaSecreta = new JLabel("Pregunta secreta:");
		lblPreguntaSecreta.setHorizontalAlignment(SwingConstants.CENTER);
		lblPreguntaSecreta.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPreguntaSecreta.setBounds(72, 11, 304, 21);
		contentPane.add(lblPreguntaSecreta);
		
		contentPane.add(cbxPregunta);
		contentPane.add(txtRespuesta);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(getControlador());
		btnAceptar.setBounds(72, 135, 89, 23);
		contentPane.add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(getControlador());
		btnCancelar.setBounds(287, 135, 89, 23);
		contentPane.add(btnCancelar);
		
		contentPane.add(btnCancelar);
		
		
		
	}


	public JTextField getTxtRespuesta() {
		return txtRespuesta;
	}


	public void setTxtRespuesta(JTextField txtRespuesta) {
		this.txtRespuesta = txtRespuesta;
	}


	public ControladorPadre getControlador() {
		return controlador;
	}


	public void setControlador(ControladorPadre controlador) {
		this.controlador = controlador;
	}


	public JComboBox getCbxPregunta() {
		return cbxPregunta;
	}


	public void setCbxPregunta(JComboBox cbxPregunta) {
		this.cbxPregunta = cbxPregunta;
	}


	public JButton getBtnAceptar() {
		return btnAceptar;
	}


	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}


	public JButton getBtnCancelar() {
		return btnCancelar;
	}


	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}
	
}
