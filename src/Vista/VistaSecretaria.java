package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import Controlador.ControladorPadre;

import javax.swing.JLabel;
import javax.swing.JMenuItem;

import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

public class VistaSecretaria extends JFrame {

	private JPanel contentPane;
	private JTable tablaCurso;
	private ControladorPadre controladorPadre;
	private JMenuItem mntmUsuarios;
	private JMenuItem mntmCursos;
	private JTextField txtBuscador;
	private JTable tablaAlumno;
	private JLabel lblBuscador;

	private JScrollPane scrollPane;
	private JButton btnAgregar;
	private JButton btnModificar;
	private JMenuItem mntmSalir;
	private JPanel panelUsuario;
	private JPanel panelCursos;
	private JButton btnAgregarACurso;

	/**
	 * Launch the application.
	 */
	/**
	 * Create the frame.
	 */
	public VistaSecretaria(ControladorPadre controlador) {
		setTitle("Secretaria");
		setResizable(false);
		this.setControladorPadre(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 693, 399);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu gestion = new JMenu("Gesti\u00F3n");
		menuBar.add(gestion);
		
		mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(getControladorPadre());
		gestion.add(mntmSalir);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setLocationRelativeTo(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(15, 44, 662, 254);
		
		panelUsuario = new JPanel();
		tabbedPane.addTab("Lista de cursos", null, panelUsuario, null);
		panelUsuario.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 657, 226);
		panelUsuario.add(scrollPane);
		
		tablaCurso = new JTable();
		tablaCurso.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nombre", "Inicio", "Finalizacion", "Lugar", "Profesor", "Horario Desde", "Horario Hasta", "Cupos Disponibles"
			}
		));
		tablaCurso.getColumnModel().getColumn(0).setPreferredWidth(67);
		tablaCurso.getColumnModel().getColumn(5).setPreferredWidth(91);
		tablaCurso.getColumnModel().getColumn(6).setPreferredWidth(88);
		tablaCurso.getColumnModel().getColumn(7).setPreferredWidth(110);
		scrollPane.setViewportView(tablaCurso);
		this.getTablaCurso().getTableHeader().setReorderingAllowed(false);
		panelCursos = new JPanel();
		tabbedPane.addTab("Lista de Alumnos", null, panelCursos, null);
		panelCursos.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(0, 0, 657, 224);
		panelCursos.add(scrollPane_1);
		
		tablaAlumno = new JTable();
		this.getTablaAlumno().getTableHeader().setReorderingAllowed(false);
		tablaAlumno.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nombre", "Apellido", "Dni", "Tipo_usuario", "Telefono"
			}
		));
		
		scrollPane_1.setViewportView(tablaAlumno);
		contentPane.setLayout(null);
		contentPane.add(tabbedPane);
		
		lblBuscador = new JLabel("Buscar:");
		lblBuscador.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblBuscador.setBounds(5, 11, 82, 22);
		contentPane.add(lblBuscador);
		
		txtBuscador = new JTextField();
		txtBuscador.setBounds(58, 11, 576, 25);
		contentPane.add(txtBuscador);
		txtBuscador.setColumns(10);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(15, 299, 105, 38);
		contentPane.add(btnAgregar);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setEnabled(false);
		btnModificar.setBounds(572, 299, 105, 38);
		contentPane.add(btnModificar);
		
		btnAgregarACurso = new JButton("Agregar Curso");
		btnAgregarACurso.setEnabled(false);
		btnAgregarACurso.setBounds(292, 299, 123, 38);
		contentPane.add(btnAgregarACurso);
	}
	public JTextField getTxtBuscador() {
		return txtBuscador;
	}

	public void setTxtBuscador(JTextField txtBuscador) {
		this.txtBuscador = txtBuscador;
	}

	public JTable getTable() {
		return tablaCurso;
	}

	public void setTable(JTable table) {
		this.tablaCurso = table;
	}

	public ControladorPadre getControladorPadre() {
		return controladorPadre;
	}

	public void setControladorPadre(ControladorPadre controladorPadre) {
		this.controladorPadre = controladorPadre;
	}

	public JTable getTablaCurso() {
		return tablaCurso;
	}

	public void setTablaCurso(JTable tablaCurso) {
		this.tablaCurso = tablaCurso;
	}

	public JMenuItem getMntmUsuarios() {
		return mntmUsuarios;
	}

	public void setMntmUsuarios(JMenuItem mntmUsuarios) {
		this.mntmUsuarios = mntmUsuarios;
	}

	public JMenuItem getMntmCursos() {
		return mntmCursos;
	}

	public void setMntmCursos(JMenuItem mntmCursos) {
		this.mntmCursos = mntmCursos;
	}

	public JMenuItem getMntmSalir() {
		return mntmSalir;
	}

	public void setMntmSalir(JMenuItem mntmSalir) {
		this.mntmSalir = mntmSalir;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JLabel getLblBuscador() {
		return lblBuscador;
	}

	public void setLblBuscador(JLabel lblBuscador) {
		this.lblBuscador = lblBuscador;
	}
	public JTable getTablaAlumno() {
		return tablaAlumno;
	}
	public void setTablaAlumno(JTable tablaAlumno) {
		this.tablaAlumno = tablaAlumno;
	}
	public JPanel getPanelUsuario() {
		return panelUsuario;
	}
	public void setPanelUsuario(JPanel panelUsuario) {
		this.panelUsuario = panelUsuario;
	}
	public JPanel getPanelCursos() {
		return panelCursos;
	}
	public void setPanelCursos(JPanel panelCursos) {
		this.panelCursos = panelCursos;
	}
	public JButton getBtnAgregarACurso() {
		return btnAgregarACurso;
	}
	public void setBtnAgregarACurso(JButton btnAgregarACurso) {
		this.btnAgregarACurso = btnAgregarACurso;
	}
	
	
}

