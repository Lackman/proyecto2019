package Modelo;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class TxtNumero extends Txt {
	private JTextField campo;
	private Integer maximoCaracteres;
	private Integer minimoCaracteres;
	private String regex = "[0-9]";
	private Boolean campoClave;
	private String tabla;
	private String columna;
	private String columnaEstado;
	private String mensajeDeReactivacion;
	private Long numeroDefecto;

	public TxtNumero(JLabel etiquetaError, String mesajeError, JTextField txtCampo, JLabel etiqueta) {
		this(etiquetaError, mesajeError, txtCampo, etiqueta, false, 0, 100, false, "", "", "", "", null);
	}

	public TxtNumero(JLabel etiquetaError, String mesajeError, JTextField txtCampo, JLabel etiqueta,
			Integer mininoCaracteres, Integer maximoCaracteres) {
		this(etiquetaError, mesajeError, txtCampo, etiqueta, true, mininoCaracteres, maximoCaracteres, false, "", "",
				"", "", null);
	}

	public TxtNumero(JLabel etiquetaError, String mesajeError, JTextField txtCampo, JLabel etiqueta,
			Integer mininoCaracteres, Integer maximoCaracteres, String tabla, String columna, String columnaEstado,
			String mensajeDeReactivacion) {
		this(etiquetaError, mesajeError, txtCampo, etiqueta, true, mininoCaracteres, maximoCaracteres,
				true, tabla, columna, columnaEstado, mensajeDeReactivacion, null);
	}

	public TxtNumero(JLabel etiquetaError, String mesajeError, JTextField txtCampo, JLabel etiqueta,
			Boolean obligatorio, Integer mininoCaracteres, Integer maximoCaracteres, Boolean campoClave, String tabla,
			String columna, String columnaEstado, String mensajeDeReactivacion, Long numeroDefecto) {

		super(etiquetaError, mesajeError, etiqueta, obligatorio);

		this.setCampo(txtCampo);
		this.setCampoClave(campoClave);
		this.setTabla(tabla);
		this.setColumna(columna);
		this.setColumnaEstado(columnaEstado);
		this.setMinimoCaracteres(mininoCaracteres);
		this.setMaximoCaracteres(maximoCaracteres);
		this.setMensajeDeReactivacion(mensajeDeReactivacion);
		this.setNumeroDefecto(numeroDefecto);
		this.CambiarAColoresDefecto();
		this.CargarListener();
		this.getCampo().addKeyListener(this.getTeclaApretada());
		this.getCampo().addFocusListener(this.getFocoDelCampo());
	}
	
	@Override
	public void ValorPorDefecto() {
		this.getCampo().setText(this.getNumeroDefecto().toString());
	}

	public void SetText(Long numero) {
		this.getCampo().setText(numero.toString());
	}
	
	public Boolean ConsisitirMaximo() {
		return this.getCampo().getText().length() >= this.getMaximoCaracteres();
	}

	public Boolean ConsisitirMinimo() {
		return this.getCampo().getText().length() >= this.getMinimoCaracteres();
	}

	public Boolean ConsisitirMaximoYMinimo() {
		return this.ConsisitirMinimo() && this.getCampo().getText().length() <= this.getMaximoCaracteres();
	}

	public Boolean CaracteresAceptados(String caracteres) {
		Pattern pattern = Pattern.compile(this.getRegex());
		Matcher matcher = pattern.matcher(caracteres);
		return matcher.matches();
	}

	public Boolean ConsistirCampoClave() {
		if (this.getCampoClave()) {
			try {
			
				ResultSet rs = BaseDeDatos.getInstance().consultar( "SELECT " + this.getColumnaEstado() + " FROM "
						+ this.getTabla() + " WHERE " + this.getColumna() + " = " + this.getCampo().getText() + ";");
				if (rs.next()) {
					if (!rs.getBoolean(1)) {
						this.ReactivarCampoClave();
					}
					return false;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.err.println("Puede que el valor minimo del campo sea demaciado chico o 0.");
				e.printStackTrace();
			}

			return true;
		}
		return true;
	}

	public void ReactivarCampoClave() {
		if (JOptionPane.showConfirmDialog(null, this.getMensajeDeReactivacion(), "�ALERTA!",
				JOptionPane.YES_NO_OPTION) == 0) {
			try {
				BaseDeDatos.getInstance()
						.consultar("UPDATE " + this.getTabla() + " SET " + this.getColumnaEstado()
								+ " = true WHERE " + this.getColumna() + " = " + this.getCampo().getText() + ";");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public Boolean ConsisitirCampo() {
		if (this.getCampoActivo()) {
			if (!this.getObligatorio()) {
				if (this.getCampo().getText().isEmpty()) {
					CambiarAColoresCorrectos();
					this.OcultarError();
					return true;
				}
			}

			if (this.ConsisitirMaximoYMinimo()) {
				for (int i = 0; i < this.getCampo().getText().length(); i++) {
					if (!CaracteresAceptados(this.getCampo().getText().substring(i, i + 1))) {
						this.CambiarAColoresDeError();
						this.MostrarError();
						return false;
					}
				}
				if (ConsistirCampoClave()) {
					CambiarAColoresCorrectos();
					this.OcultarError();
					return true;
				}
			}
			this.CambiarAColoresDeError();
			this.MostrarError();
			return false;
		}
		return true;
	}

	@Override
	public void DeshabilitarCampo() {
		this.getCampo().setEnabled(false);
		super.DeshabilitarCampo();
	}

	@Override
	public void HabiliarCampo() {
		this.getCampo().setEnabled(true);
		super.HabiliarCampo();
	}

	public void CambiarAColoresDeError() {
		this.CambiarColorCampo(Color.RED);
		this.CambiarColorEtiqueta(Color.RED);
	}

	@Override
	public void CambiarAColoresCorrectos() {
		this.CambiarColorCampo(Color.GREEN);
		this.CambiarColorEtiqueta(Color.GREEN);
	}

	@Override
	public void CambiarAColoresDefecto() {
		this.CambiarColorCampo(Color.GRAY);
		this.CambiarColorEtiqueta(Color.BLACK);
	}

	@Override
	public void CambiarAColoresDeshabilitado() {
		this.CambiarColorCampo(Color.LIGHT_GRAY);
		this.CambiarColorEtiqueta(Color.LIGHT_GRAY);
	}

	@Override
	public void CambiarColorCampo(Color color) {
		this.getCampo().setBorder(BorderFactory.createLineBorder(color, 1));
	}

	@Override
	public void CambiarColorEtiqueta(Color color) {
		this.getEtiqueta().setForeground(color);
	}

	@Override
	public void CargarListener() {
		this.setTeclaApretada(new KeyListener() {
			Boolean teclaBorrar = false;

			@Override
			public void keyTyped(KeyEvent e) {
				if (ConsisitirMaximo() || !CaracteresAceptados(e.getKeyChar() + "")) {
					e.consume();
					if (!this.teclaBorrar) {
						Toolkit.getDefaultToolkit().beep();
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				ConsisitirCampo();
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if ((e.getKeyCode() == KeyEvent.VK_BACK_SPACE) || (e.getKeyCode() == KeyEvent.VK_ESCAPE)
						|| (e.getKeyCode() == KeyEvent.VK_DELETE) || (e.getKeyCode() == KeyEvent.VK_ENTER)) {
					this.teclaBorrar = true;
				} else {
					this.teclaBorrar = false;
				}
			}
		});
		super.CargarListener();
	}

	public JTextField getCampo() {
		return campo;
	}

	public void setCampo(JTextField campo) {
		this.campo = campo;
	}

	public Integer getMaximoCaracteres() {
		return maximoCaracteres;
	}

	public void setMaximoCaracteres(Integer maximoCaracteres) {
		this.maximoCaracteres = maximoCaracteres;
	}

	public Integer getMinimoCaracteres() {
		return minimoCaracteres;
	}

	public void setMinimoCaracteres(Integer minimoCaracteres) {
		this.minimoCaracteres = minimoCaracteres;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public Boolean getCampoClave() {
		return campoClave;
	}

	public void setCampoClave(Boolean campoClave) {
		this.campoClave = campoClave;
	}

	public String getTabla() {
		return tabla;
	}

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	public String getColumna() {
		return columna;
	}

	public void setColumna(String columna) {
		this.columna = columna;
	}

	public String getColumnaEstado() {
		return columnaEstado;
	}

	public void setColumnaEstado(String columnaEstado) {
		this.columnaEstado = columnaEstado;
	}

	public String getMensajeDeReactivacion() {
		return mensajeDeReactivacion;
	}

	public void setMensajeDeReactivacion(String mensajeDeReactivacion) {
		this.mensajeDeReactivacion = mensajeDeReactivacion;
	}

	public Long getNumeroDefecto() {
		return numeroDefecto;
	}

	public void setNumeroDefecto(Long numeroDefecto) {
		this.numeroDefecto = numeroDefecto;
	}

}
