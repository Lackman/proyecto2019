package Modelo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import Controlador.ControladorLogin;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Reporte {
	public static void reportesClientes() throws SQLException
	{
		HashMap<String, Object> parametros = new HashMap<String,Object>();
			
		String consulta = "select dni, nombre, apellido, telefono from usuario where estado = true";
	    ResultSet rs = BaseDeDatos.getInstance().buscarEntidad(consulta);
	    
	    while(rs.next()) {
	    	parametros.put("dni", rs.getString(1));
	    	parametros.put("nombre", rs.getString(2));
	    	parametros.put("apellido", rs.getString(3));
	    	parametros.put("telefono", rs.getString(4));
	    }
			
		String nombreEmpresa = "MyReports";
		AbstractJasperReports.createReport( rs, "C:\\Users\\nico_\\JaspersoftWorkspace\\MyReports\\Invoice_1.jasper",nombreEmpresa );
	
		//AbstractJasperReports.showViewer();
		//AbstractJasperReports.showViewer();
		//AbstractJasperReports.exportToPDF( "reporteUsuarios.pdf" );
	}
	
	public static void ReporteAlumnos(String curso) throws SQLException {

		
		HashMap<String, Object> parametros = new HashMap<>();
		String dni = ControladorLogin.getInstance().getLogin().getTxtUsuario().getText();
		String consulta = "select distinct nombre, apellido, dni, nota from curso_profesor inner join curso_alumno on curso_alumno.id_curso= curso_profesor.id_curso inner join usuario on usuario.dni = curso_alumno.id_alumno inner join curso on curso.id_curso = curso_alumno.id_curso where (curso_profesor.id_profesor ="+ dni +") and"
				+ " (curso.nombre_curso = '"+curso+"')";
		//System.out.println(consulta);
		
		ResultSet rs = BaseDeDatos.getInstance().buscarEntidad(consulta);
		parametros.put("idProfesor", new Integer(dni));
		parametros.put("nombreCurso", curso);
		parametros.put("logo1", "C:\\Users\\nico_\\JaspersoftWorkspace\\MyReports\\leaf_banner_red.png");
		
		JasperReport jr;
		
		String ruta = "C:\\Users\\nico_\\JaspersoftWorkspace\\MyReports\\ReporteLindo.jasper";
		
		
		try {
			jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
			JasperPrint jp = JasperFillManager.fillReport(jr, parametros, BaseDeDatos.getInstance().getConnection());
			JasperViewer jv = new JasperViewer(jp);
			jv.setVisible(true);
			jv.setTitle("prueba");
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//HashMap<String, Object> parametros2 = new HashMap<String, Object>();
		/*while (rs.next()) {
			//nombre,apellido,dni,notas
			parametros.put("nombre", rs.getString(1));
			parametros.put("apellido", rs.getString(2));
			parametros.put("dni", rs.getString(3));
			parametros.put("nota", rs.getString(4));
			//System.out.println(rs.getString(4));
			
		}
		parametros.put("idProfesor", new Integer(dni));
		parametros.put("nombreCurso", curso);*/
		
		//AbstractJasperReports.createReport(rs, "C:\\Users\\nico_\\JaspersoftWorkspace\\MyReports\\Blank_A4.jasper",nombreEmpresa);
		/*AbstractJasperReports.createReport(rs, ruta, nombreEmpresa);
		AbstractJasperReports.exportToPDF("reportealumnos.pdf");*/
	}
}
