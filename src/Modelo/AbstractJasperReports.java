package Modelo;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class AbstractJasperReports {
	private static JasperReport	report;
	private static JasperPrint	reportFilled;
	private static JasperViewer	viewer;
	

	public static void createReport( ResultSet info, String path,String parametro )
	{
		//borrar
		HashMap<String, Object> parametros = new HashMap<String, Object>();

		parametros.put("Lista de usuario", parametro);
		
		// descarga dentro del mismo proyecto
				
				try {
					JasperPrint jasperPrint;
					jasperPrint = JasperFillManager.fillReport(
							path, parametros,
							BaseDeDatos.getInstance().getConnection());
					JRPdfExporter exp = new JRPdfExporter();
				exp.setExporterInput(new SimpleExporterInput(jasperPrint));
				exp.setExporterOutput(new SimpleOutputStreamExporterOutput("ReporteAlumnos.pdf"));
				SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
				exp.setConfiguration(conf);
				exp.exportReport();
				// se muestra en una ventana aparte para su descarga
			JasperPrint jasperPrintWindow;
				jasperPrintWindow = JasperFillManager.fillReport(
						path, parametros,
						BaseDeDatos.getInstance().getConnection());
				JasperViewer jasperViewer = new JasperViewer(jasperPrintWindow);
			jasperViewer.setVisible(true);
				} catch (JRException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	}
	/*public static void createReport2(HashMap<String, Object> parametros) {
		JasperReport jr;
		
		String ruta = "C:\\Users\\nico_\\JaspersoftWorkspace\\MyReports\\Blank_A4.jasper";
		
		
		try {
			jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
			JasperPrint jp = JasperFillManager.fillReport(jr, parametros);
			JasperViewer jv = new JasperViewer(jp);
			jv.setVisible(true);
			jv.setTitle("prueba");
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	
	public static void showViewer()
	{
		//JDialog jd = new JDialog(new JFrame(),false);
		viewer = new JasperViewer( reportFilled );
		
		//jd.getContentPane().add(viewer);
		//jd.setVisible(true);
	   viewer.setVisible( true );
	}

	public static void exportToPDF( String destination )
	{
		try { 
			JasperExportManager.exportReportToPdfFile( reportFilled, destination );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
		try {
		     File path = new File (destination);
		     Desktop.getDesktop().open(path);
		}catch (IOException ex) {
		     ex.printStackTrace();
		}
	}
	
}
