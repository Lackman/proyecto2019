package Modelo;

import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

import Controlador.ControladorLogin;

public class Usuario {
	private static Usuario instance;
	private String nombre, apellido, tipo, localidad, genero, contrasenia, respuesta, pregunta, provincia, telefono;
	private Integer DNI;

	public static Usuario deleteinstance() {
		instance = null;
		return getInstance();
	}

	public static Usuario getInstance() {
		if (instance == null) {
			instance = new Usuario();
		}
		return instance;
	}

	public String comprobar(String usuario) {
		ResultSet rs = null;
		String x = "";
		try {
			rs = BaseDeDatos.getInstance()
					.consultar("Select id_tipo_usuario from usuario where dni = '" + usuario + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (rs.next()) {
				x = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return String.valueOf(x);

	}

	public Boolean chequearRespuesta(String usuario, String respuesta) throws SQLException {
		if (BaseDeDatos.getInstance()
				.consultar("Select dni from usuario where dni = '" + usuario + "'and respuesta = '" + respuesta + "'")
				.next()) {
			return true;
		} else {
			return false;
		}
		// throw new SQLException();
	}

	public Boolean iniciarSesion(String usuario, String contrasenia) throws SQLException {
		if (BaseDeDatos.getInstance().consultar("SELECT dni, contrasenia, estado FROM public.\"usuario\" where dni = '"
				+ usuario + "' and contrasenia = '" + contrasenia + "' and estado = true").next()) {
			
			return true;
		}
		throw new SQLException();

	}

	public String[] consultaLocalidad(String provincia) {
		ResultSet rs = null;
		try {
			rs = BaseDeDatos.getInstance().buscarEntidad(
					"select nombre_localidad from localidad where localidad.id_provincia = (select id_provincia from provincia where nombre_provincia = '"
							+ provincia + "')");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ArrayList<String> str = new ArrayList<String>();
			Integer i = 0;
			while (rs.next()) {
				str.add(rs.getString(1));
				i++;
			}
			String[] str2 = new String[i + 1];
			str2[0] = "Selecciona una localidad";
			for (int j = 1; j < str2.length; j++) {
				str2[j] = str.remove(0);
			}
			return str2;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public DefaultTableModel consultaTablaUsuario() {
		String consulta = "select usuario.nombre, usuario.apellido, usuario.dni, tipo_usuario.nombre_usuario, usuario.telefono, provincia.nombre_provincia, localidad.nombre_localidad, genero.nombre_genero from usuario join tipo_usuario on usuario.id_tipo_usuario = tipo_usuario.id_tipo join localidad on usuario.id_localidad = localidad.id_localidad join provincia on localidad.id_provincia = provincia.id_provincia join genero on usuario.id_genero = genero.id_genero where usuario.estado = true";
		ResultSet rs = null;
		DefaultTableModel modeloTabla = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}
		};
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);

			String[] titulos = new String[] { "Nombre", "Apellido", "D.N.I", "Tipo Usuario", "Telefono", "Provincia",
					"Localidad" };
			modeloTabla.setColumnIdentifiers(titulos);
			ResultSetMetaData rsmd = null;
			try {
				rsmd = rs.getMetaData();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer nroCol = 0;

			try {
				nroCol = rsmd.getColumnCount();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				while (rs.next()) {
					Object[] objects = new Object[nroCol];
					for (int i = 0; i < nroCol; i++) {
						objects[i] = rs.getObject(i + 1);
					}
					modeloTabla.addRow(objects);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return modeloTabla;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	public DefaultTableModel ConsultarTablaAlumno(String busq) {
		String consulta = "select distinct usuario.nombre, usuario.apellido, usuario.dni, tipo_usuario.nombre_usuario, "
				+ "usuario.telefono, provincia.nombre_provincia, localidad.nombre_localidad, genero.nombre_genero "
				+ "from usuario inner join tipo_usuario on usuario.id_tipo_usuario = tipo_usuario.id_tipo "
				+ "inner join localidad on usuario.id_localidad = localidad.id_localidad "
				+ "inner join provincia on localidad.id_provincia = provincia.id_provincia "
				+ "inner join genero on usuario.id_genero = genero.id_genero where usuario.estado = true and id_tipo_usuario = 3"
				+ "and (usuario.nombre like '%" + busq + "%' or usuario.apellido like '%" + busq
				+ "%' or cast(usuario.dni as varchar) like '%" + busq + "%' or tipo_usuario.nombre_usuario like '%"
				+ busq + "%' or cast(usuario.telefono as varchar) like '%" + busq
				+ "%' or localidad.nombre_localidad like '%" + busq + "%' or provincia.nombre_provincia like '%" + busq
				+ "%' )";
		ResultSet rs = null;
		DefaultTableModel modeloTabla = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}
		};
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);

			String[] titulos = new String[] { "Nombre", "Apellido", "D.N.I", "Tipo Usuario", "Telefono", "Provincia",
					"Localidad" };
			modeloTabla.setColumnIdentifiers(titulos);
			ResultSetMetaData rsmd = null;
			try {
				rsmd = rs.getMetaData();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer nroCol = 0;

			try {
				nroCol = rsmd.getColumnCount();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				while (rs.next()) {
					Object[] objects = new Object[nroCol];
					for (int i = 0; i < nroCol; i++) {
						objects[i] = rs.getObject(i + 1);
					}
					modeloTabla.addRow(objects);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return modeloTabla;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	public DefaultTableModel consultarTablaProfesor() {
		String consulta = "select nombre, apellido, dni from usuario where id_tipo_usuario = 4 and estado = true";
		ResultSet rs = null;
		DefaultTableModel modeloTabla = new DefaultTableModel() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}
		};
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);

			String[] titulos = new String[] { "Nombre", "Apellido", "DNI" };
			modeloTabla.setColumnIdentifiers(titulos);
			ResultSetMetaData rsmd = null;
			try {
				rsmd = rs.getMetaData();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer nroCol = 0;

			try {
				nroCol = rsmd.getColumnCount();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				while (rs.next()) {
					Object[] objects = new Object[nroCol];
					for (int i = 0; i < nroCol; i++) {
						objects[i] = rs.getObject(i + 1);
					}
					modeloTabla.addRow(objects);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return modeloTabla;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	public DefaultTableModel buscarUsuario(String st) {
		String consulta = "select usuario.nombre, usuario.apellido, usuario.dni, tipo_usuario.nombre_usuario, usuario.telefono, provincia.nombre_provincia, localidad.nombre_localidad, genero.nombre_genero from usuario join tipo_usuario on usuario.id_tipo_usuario = tipo_usuario.id_tipo join localidad on usuario.id_localidad = localidad.id_localidad join provincia on localidad.id_provincia = provincia.id_provincia join genero on usuario.id_genero = genero.id_genero and usuario.estado = true where usuario.nombre like '%"
				+ st + "%' or usuario.apellido like '%" + st + "%' or cast(usuario.dni as varchar) like '%" + st
				+ "%' or tipo_usuario.nombre_usuario like '%" + st + "%' or cast(usuario.telefono as varchar) like '%"
				+ st + "%' or localidad.nombre_localidad like '%" + st + "%' or provincia.nombre_provincia like '%" + st
				+ "%'";
		// System.out.println(consulta);
		ResultSet rs = null;
		DefaultTableModel modeloTabla = new DefaultTableModel() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}
		};
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);
			String[] titulos = new String[] { "Nombre", "Apellido", "D.N.I", "Tipo Usuario", "Telefono", "Provincia",
					"Localidad" };
			modeloTabla.setColumnIdentifiers(titulos);
			ResultSetMetaData rsmd = null;
			// System.out.println(st);
			try {
				rsmd = rs.getMetaData();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer nroCol = 0;

			try {
				nroCol = rsmd.getColumnCount();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				while (rs.next()) {
					Object[] objects = new Object[nroCol];
					for (int i = 0; i < nroCol; i++) {
						objects[i] = rs.getObject(i + 1);
					}
					modeloTabla.addRow(objects);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return modeloTabla;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	public String[] consulColumna(String columna, String tabla) {
		String consultas = "select " + columna + " from " + tabla;
		ResultSet rs = null;
		try {
			rs = BaseDeDatos.getInstance().buscarEntidad(consultas);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ArrayList<String> str = new ArrayList<String>();
			Integer i = 0;
			while (rs.next()) {
				str.add(rs.getString(1));
				i++;
			}
			String[] str2 = new String[i + 1];
			for (int j = 1; j < str2.length; j++) {
				str2[j] = str.remove(0);
			}
			return str2;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String[] consulColumna2() {
		Integer dni = Integer.valueOf(ControladorLogin.getInstance().getLogin().getTxtUsuario().getText());
		String consulta = "select distinct nombre_curso from curso inner join curso_profesor on curso_profesor.id_curso = curso.id_curso where curso_profesor.id_profesor ="
				+ dni + ";";
		// String consultas = "select " + columna + " from " + tabla;
		ResultSet rs = null;
		try {
			rs = BaseDeDatos.getInstance().buscarEntidad(consulta);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ArrayList<String> str = new ArrayList<String>();
			Integer i = 0;
			while (rs.next()) {
				str.add(rs.getString(1));
				i++;
			}
			String[] str2 = new String[i + 1];
			for (int j = 1; j < str2.length; j++) {
				str2[j] = str.remove(0);
			}
			return str2;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Boolean consultarEstado(String dni) {
		String consultarEstado = "select dni from usuario where dni = " + dni + " and estado = false";
		ResultSet rs;
		Boolean exis = true;
		String estado = "";
		try {
			rs = BaseDeDatos.getInstance().consultar(consultarEstado);
			if (rs.next()) {
			} else {
				exis = false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			exis = false;
		}
		return exis;
	}

	public void cambiarEstadoUsuario(String dni) {
		String consultaEstado = "select estado from usuario where dni = " + dni;
		String estadoCad = "";
		ResultSet rs;
		try {
			rs = BaseDeDatos.getInstance().consultar(consultaEstado);
			while (rs.next()) {
				estadoCad = rs.getString(1);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Boolean estado;
		if (estadoCad.equals("f")) {
			estado = true;
		} else {
			estado = false;
		}

		String consulta = "UPDATE public.usuario SET estado= " + estado + " where usuario.dni = " + dni + ";";

		try {
			BaseDeDatos.getInstance().insertar(consulta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Usuario ObtenerDatosUsuario(Integer IdUsuario) {
		Usuario usuario = new Usuario();
		String consulta = "select * from usuario where dni = " + IdUsuario;
		try {
			ResultSet rs = BaseDeDatos.getInstance().consultar(consulta);
			while (rs.next()) {
				usuario.setDNI(Integer.parseInt(rs.getObject(1).toString()));
				usuario.setNombre(rs.getObject(2).toString());
				usuario.setContrasenia(rs.getObject(5).toString());
				usuario.setPregunta(rs.getObject(10).toString());
				usuario.setApellido(rs.getObject(3).toString());
				usuario.setTipo(rs.getObject(4).toString());
				usuario.setTelefono(rs.getObject(7).toString());
				usuario.setLocalidad(rs.getObject(8).toString());
				usuario.setGenero(rs.getObject(9).toString());
				usuario.setPregunta(rs.getObject(11).toString());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return usuario;
	}

	public Integer ObtenerProvincia(String IdLocalidad) {
		String consulta = "select id_provincia from localidad where id_localidad = " + IdLocalidad;
		Integer idProvincia = null;
		try {
			ResultSet rs = BaseDeDatos.getInstance().consultar(consulta);
			while (rs.next()) {
				idProvincia = Integer.parseInt(rs.getObject(1).toString());

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return idProvincia;
	}

	public void ModificarUsuario(Usuario usuario, Boolean modificarContrasenna, Boolean modificarRespuesta) {
		String consulta = null;
		if (modificarContrasenna && modificarRespuesta) {
			consulta = "UPDATE public.usuario SET nombre='" + usuario.getNombre() + "', apellido='"
					+ usuario.getApellido()
					+ "', id_tipo_usuario=(select id_tipo from tipo_usuario where nombre_usuario = '"
					+ usuario.getTipo() + "'), estado=true, telefono='" + usuario.getTelefono()
					+ "', id_localidad=(select id_localidad from localidad where nombre_localidad = '"
					+ usuario.getLocalidad() + "'), id_genero=(select id_genero from genero where nombre_genero = '"
					+ usuario.getGenero() + "'), respuesta='" + usuario.getRespuesta()
					+ "', id_pregunta=(select id_pregunta from pregunta where pregunta ='" + usuario.getPregunta()
					+ "'), contrasenia='" + usuario.contrasenia + "' WHERE dni = " + usuario.getDNI() + ";";
		} else if (modificarRespuesta) {
			consulta = "UPDATE public.usuario SET nombre='" + usuario.getNombre() + "', apellido='"
					+ usuario.getApellido()
					+ "', id_tipo_usuario=(select id_tipo_usuario from tipo_usuario where nombre_usuario = '"
					+ usuario.getTipo() + "'), estado=true, telefono='" + usuario.getTelefono()
					+ "', id_localidad=(select id_localidad from localidad where nombre_localidad = '"
					+ usuario.getLocalidad() + "'), id_genero=(select id_genero from genero where nombre_genero = '"
					+ usuario.getGenero() + "'), respuesta='" + usuario.getRespuesta()
					+ "', id_pregunta=(select id_pregunta from pregunta where pregunta ='" + usuario.getPregunta()
					+ "') WHERE dni = " + usuario.getDNI() + ";";
		} else if (modificarContrasenna) {
			consulta = "UPDATE public.usuario SET nombre='" + usuario.getNombre() + "', apellido='"
					+ usuario.getApellido()
					+ "', id_tipo_usuario=(select id_tipo_usuario from tipo_usuario where nombre_usuario = '"
					+ usuario.getTipo() + "'), estado=true, telefono='" + usuario.getTelefono()
					+ "', id_localidad=(select id_localidad from localidad where nombre_localidad = '"
					+ usuario.getLocalidad() + "'), id_genero=(select id_genero from genero where nombre_genero = '"
					+ usuario.getGenero() + "'), contrasenia='" + usuario.contrasenia + "' WHERE dni = "
					+ usuario.getDNI() + ";";
		} else {
			consulta = "UPDATE public.usuario SET nombre='" + usuario.getNombre() + "', apellido='"
					+ usuario.getApellido()
					+ "', id_tipo_usuario=(select id_tipo_usuario from tipo_usuario where nombre_usuario = '"
					+ usuario.getTipo() + "'), estado=true, telefono='" + usuario.getTelefono()
					+ "', id_localidad=(select id_localidad from localidad where nombre_localidad = '"
					+ usuario.getLocalidad() + "'), id_genero=(select id_genero from genero where nombre_genero = '"
					+ usuario.getGenero() + "') WHERE dni = " + usuario.getDNI() + ";";
		}
		try {
			BaseDeDatos.getInstance().consultar(consulta);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void guardarUsuario(Usuario user) {
		String consulta = "INSERT INTO public.usuario(dni, nombre, apellido, id_tipo_usuario, contrasenia, estado, telefono, id_localidad, id_genero, respuesta, id_pregunta)"
				+ " values('" + user.DNI + "','" + user.nombre.toLowerCase() + "','" + user.apellido.toLowerCase()
				+ "'," + " (select id_tipo from tipo_usuario where nombre_usuario = '" + user.tipo + "'),'"
				+ user.contrasenia + "'," + "true" + ",'" + user.telefono
				+ "', (select id_localidad from localidad where nombre_localidad = '" + user.localidad
				+ "'), (select id_genero from genero where nombre_genero = '" + user.genero + "'), '" + user.respuesta
				+ "'," + "(select id_pregunta from pregunta where pregunta = '" + user.pregunta + "'))";
		try {
			BaseDeDatos.getInstance().insertar(consulta);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void ActualizarContrasenna(String dni, String text) {
		String consulta = "update usuario set contrasenia = '" + text + "' where usuario.dni = " + dni;
		try {
			BaseDeDatos.getInstance().consultar(consulta);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void NotaUsuario(String nota, String id, String curso) {
		String consulta = "Update curso_alumno SET nota = '" + nota + "' where (id_alumno = '" + id
				+ "') and id_curso = (select id_curso from curso where nombre_curso = '" + curso + "');";
		try {
			BaseDeDatos.getInstance().consultar(consulta);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getDNI() {
		return DNI;
	}

	public void setDNI(Integer dNI) {
		DNI = dNI;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
