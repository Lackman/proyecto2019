package Modelo;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class TxtTexto extends Txt {
	private JTextField campo;
	private Integer maximoCaracteres;
	private Integer minimoCaracteres;
	private String textoDefecto;

	public TxtTexto(JLabel etiquetaError, String mesajeError, JTextField txtCampo, JLabel etiqueta) {
		this(etiquetaError, mesajeError, txtCampo, etiqueta, false, 0, 100, "");
	}
	
	public TxtTexto(JLabel etiquetaError, String mesajeError, JTextField txtCampo, JLabel etiqueta, String textoDefecto) {
		this(etiquetaError, mesajeError, txtCampo, etiqueta, true, 1, 100, textoDefecto);
	}

	public TxtTexto(JLabel etiquetaError, String mesajeError, JTextField txtCampo, JLabel etiqueta, Boolean obligatorio,
			Integer mininoCaracteres, Integer maximoCaracteres, String textoDefecto) {
		super(etiquetaError, mesajeError, etiqueta, obligatorio);
		this.setCampo(txtCampo);
		this.CambiarAColoresDefecto();
		this.setMinimoCaracteres(mininoCaracteres);
		this.setMaximoCaracteres(maximoCaracteres);
		this.setTextoDefecto(textoDefecto);
		this.ValorPorDefecto();
		this.CargarListener();
		this.getCampo().addKeyListener(this.getTeclaApretada());
		this.getCampo().addFocusListener(this.getFocoDelCampo());
	}

	@Override
	public void ValorPorDefecto() {
		this.getCampo().setText(this.getTextoDefecto());
	}
	
	public void SetText(String texto) {
		this.getCampo().setText(texto);
	}
	
	public Boolean ConsisitirMaximo() {
		return this.getCampo().getText().length() >= this.getMaximoCaracteres();
	}

	public Boolean ConsisitirMinimo() {
		return this.getCampo().getText().length() >= this.getMinimoCaracteres();
	}

	public Boolean ConsisitirMaximoYMinimo() {
		return this.ConsisitirMinimo() && this.getCampo().getText().length() <= this.getMaximoCaracteres();
	}

	@Override
	public Boolean ConsisitirCampo() {
		if (this.getCampoActivo()) {
			if (!this.getObligatorio()) {
				if (this.getCampo().getText().isEmpty()) {
					CambiarAColoresCorrectos();
					this.OcultarError();
					return true;
				}
			}
			if (this.ConsisitirMaximoYMinimo()) {
				CambiarAColoresCorrectos();
				this.OcultarError();
				return true;
			}
			this.CambiarAColoresDeError();
			this.MostrarError();
			return false;
		}
		return true;
	}

	@Override
	public void DeshabilitarCampo() {
		this.getCampo().setEnabled(false);
		this.CambiarAColoresDeshabilitado();
		super.DeshabilitarCampo();
	}

	@Override
	public void HabiliarCampo() {
		this.getCampo().setEnabled(true);
		super.HabiliarCampo();
	}

	@Override
	public void CambiarAColoresDeError() {
		this.CambiarColorCampo(Color.RED);
		try {
			this.CambiarColorEtiqueta(Color.RED);	
		} catch (NullPointerException e) {
		}
	}

	@Override
	public void CambiarAColoresCorrectos() {
		this.CambiarColorCampo(Color.GREEN);
		try {
			this.CambiarColorEtiqueta(Color.GREEN);	
		} catch (NullPointerException e) {
		}
	}

	@Override
	public void CambiarAColoresDefecto() {
		this.CambiarColorCampo(Color.GRAY);
		try {
			this.CambiarColorEtiqueta(Color.BLACK);
		} catch (NullPointerException e) {
		}
	}

	@Override
	public void CambiarAColoresDeshabilitado() {
		this.CambiarColorCampo(Color.LIGHT_GRAY);
		try {
			this.CambiarColorEtiqueta(Color.LIGHT_GRAY);
		} catch (NullPointerException e) {
		}
	}

	@Override
	public void CambiarColorCampo(Color color) {
		this.getCampo().setBorder(BorderFactory.createLineBorder(color, 1));
	}

	@Override
	public void CambiarColorEtiqueta(Color color) {
		this.getEtiqueta().setForeground(color);
	}

	@Override
	public void CargarListener() {
		this.setTeclaApretada(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				if (ConsisitirMaximo()) {
					e.consume();
					Toolkit.getDefaultToolkit().beep();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				ConsisitirCampo();
			}

			@Override
			public void keyPressed(KeyEvent e) {

			}
		});
		super.CargarListener();
	}

	public JTextField getCampo() {
		return campo;
	}

	public void setCampo(JTextField campo) {
		this.campo = campo;
	}

	public Integer getMaximoCaracteres() {
		return maximoCaracteres;
	}

	public void setMaximoCaracteres(Integer maximoCaracteres) {
		this.maximoCaracteres = maximoCaracteres;
	}

	public Integer getMinimoCaracteres() {
		return minimoCaracteres;
	}

	public void setMinimoCaracteres(Integer minimoCaracteres) {
		this.minimoCaracteres = minimoCaracteres;
	}

	public String getTextoDefecto() {
		return textoDefecto;
	}

	public void setTextoDefecto(String textoDefecto) {
		this.textoDefecto = textoDefecto;
	}

}
