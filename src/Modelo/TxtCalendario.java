package Modelo;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import com.toedter.calendar.JDateChooser;

public class TxtCalendario extends Txt {
	private JDateChooser primerCampo;
	private JDateChooser segundoCampo;
	private String formatoMostrar = "dd-MM-yyyy";
	private JLabel segundaEtiqueta;
	private Date fechaDefectoPrimero;
	private Date fechaDefectoSegundo;

	public TxtCalendario(JLabel etiquetaError, String mesajeError, JDateChooser txtPrimerCampo,
			JDateChooser txtSegundoCampo, JLabel primeraEtiqueta, JLabel segundaEtiqueta) {
		this(etiquetaError, mesajeError, txtPrimerCampo, txtSegundoCampo, primeraEtiqueta, segundaEtiqueta, true,
				new Date(), new Date(), new Date(new Date().getTime() + 86400000));// 86400000 es 1 dia
	}
	
	public TxtCalendario(JLabel etiquetaError, String mesajeError, JDateChooser txtPrimerCampo,
			JDateChooser txtSegundoCampo, JLabel primeraEtiqueta, JLabel segundaEtiqueta, Date fechaDefectoPrimero, Date fechaDefectoSegundo, Boolean obligatorio) {
		this(etiquetaError, mesajeError, txtPrimerCampo, txtSegundoCampo, primeraEtiqueta, segundaEtiqueta, obligatorio,
				new Date(), fechaDefectoPrimero, fechaDefectoSegundo);
	}
	
	public TxtCalendario(JLabel etiquetaError, String mesajeError, JDateChooser txtPrimerCampo,
			JDateChooser txtSegundoCampo, JLabel primeraEtiqueta, JLabel segundaEtiqueta, Date fechaDefectoPrimero, Date fechaDefectoSegundo) {
		this(etiquetaError, mesajeError, txtPrimerCampo, txtSegundoCampo, primeraEtiqueta, segundaEtiqueta, true,
				new Date(), fechaDefectoPrimero, fechaDefectoSegundo);
	}

	public TxtCalendario(JLabel etiquetaError, String mesajeError, JDateChooser txtPrimerCampo,
			JDateChooser txtSegundoCampo, JLabel primeraEtiqueta, JLabel segundaEtiqueta, Boolean obligatorio,
			Date fechaMinima, Date fechaDefectoPrimero, Date fechaDefectoSegundo) {
		super(etiquetaError, mesajeError, primeraEtiqueta, obligatorio);
		this.setSegundaEtiqueta(segundaEtiqueta);
		this.setPrimerCampo(txtPrimerCampo);
		this.setSegundoCampo(txtSegundoCampo);
		this.CambiarAColoresDefecto();
		this.getPrimerCampo().setMinSelectableDate(fechaMinima);
		this.getSegundoCampo().setMinSelectableDate(fechaMinima);
		this.setFechaDefectoPrimero(fechaDefectoPrimero);
		this.setFechaDefectoSegundo(fechaDefectoSegundo);
		this.ValorPorDefecto();
		this.getPrimerCampo().setDateFormatString(this.getFormatoMostrar());
		this.getSegundoCampo().setDateFormatString(this.getFormatoMostrar());
	}

	public Boolean ConsisitirFechaMayor(Date fechaInicial, Date fechaFinal) {
		return fechaInicial.getTime() <= fechaFinal.getTime(); // !Igual o no?
	}
	
	@Override
	public void ValorPorDefecto() {
		this.getPrimerCampo().setDate(new Date(this.getFechaDefectoPrimero().getTime()));
		this.getSegundoCampo().setDate(new Date(this.getFechaDefectoSegundo().getTime()));
	}
	
	public void SetText(Date fechaPrimerCampo, Date fechaSegundoCampo) {
		this.getPrimerCampo().setDate(new Date(fechaPrimerCampo.getTime()));
		this.getSegundoCampo().setDate(new Date(fechaSegundoCampo.getTime()));
	}
	
	@Override
	public Boolean ConsisitirCampo() {
		if (this.getCampoActivo()) {
			if (!this.getObligatorio()) {
				if ((this.getPrimerCampo().getDate().getTime() == new Date().getTime())
						&& (this.getSegundoCampo().getDate().getTime() == new Date().getTime() + 86400000)) {
					CambiarAColoresCorrectos();
					this.OcultarError();
					return true;
				}
			}
			if (ConsisitirFechaMayor(this.getPrimerCampo().getDate(), this.getSegundoCampo().getDate())) {
				CambiarAColoresCorrectos();
				this.OcultarError();
				return true;
			}
			this.CambiarAColoresDeError();
			this.MostrarError();
			return false;
		}
		CambiarAColoresCorrectos();
		this.OcultarError();
		return true;
	}

	@Override
	public void CambiarColorCampo(Color color) {
		this.getPrimerCampo().setBorder(BorderFactory.createLineBorder(color, 1));
		this.getSegundoCampo().setBorder(BorderFactory.createLineBorder(color, 1));
	}

	@Override
	public void CambiarColorEtiqueta(Color color) {
		this.getEtiqueta().setForeground(color);
		this.getSegundaEtiqueta().setForeground(color);
	}

	@Override
	public void CambiarAColoresDeError() {
		this.CambiarColorCampo(Color.RED);
		this.CambiarColorEtiqueta(Color.RED);
	}

	@Override
	public void CambiarAColoresCorrectos() {
		this.CambiarColorCampo(Color.GREEN);
		this.CambiarColorEtiqueta(Color.GREEN);
	}

	@Override
	public void CambiarAColoresDefecto() {
		//this.CambiarColorCampo(Color.GRAY);
		this.CambiarColorEtiqueta(Color.BLACK);
	}

	@Override
	public void CambiarAColoresDeshabilitado() {
		this.CambiarColorCampo(Color.LIGHT_GRAY);
		this.CambiarColorEtiqueta(Color.LIGHT_GRAY);
	}

	@Override
	public void CargarListener() {
	}

	public JDateChooser getPrimerCampo() {
		return primerCampo;
	}

	public void setPrimerCampo(JDateChooser primerCampo) {
		this.primerCampo = primerCampo;
	}

	public JDateChooser getSegundoCampo() {
		return segundoCampo;
	}

	public void setSegundoCampo(JDateChooser segundoCampo) {
		this.segundoCampo = segundoCampo;
	}

	public String getFormatoMostrar() {
		return formatoMostrar;
	}

	public void setFormatoMostrar(String formatoMostrar) {
		this.formatoMostrar = formatoMostrar;
	}

	public JLabel getSegundaEtiqueta() {
		return segundaEtiqueta;
	}

	public void setSegundaEtiqueta(JLabel segundaEtiqueta) {
		this.segundaEtiqueta = segundaEtiqueta;
	}

	public Date getFechaDefectoPrimero() {
		return fechaDefectoPrimero;
	}

	public void setFechaDefectoPrimero(Date fechaDefectoPrimero) {
		this.fechaDefectoPrimero = fechaDefectoPrimero;
	}

	public Date getFechaDefectoSegundo() {
		return fechaDefectoSegundo;
	}

	public void setFechaDefectoSegundo(Date fechaDefectoSegundo) {
		this.fechaDefectoSegundo = fechaDefectoSegundo;
	}
}
