package Modelo;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class TxtCbx extends Txt {
	private JComboBox<Object> campo;
	private JComboBox<Object> campoPadre;
	private TxtTexto campoTexto;
	private ItemListener cambioDeCampo;

	public TxtCbx(JLabel etiquetaError, String mesajeError, JComboBox<Object> campo, JLabel etiqueta) {
		this(etiquetaError, mesajeError, campo, null, etiqueta, null, true);
	}

	public TxtCbx(JLabel etiquetaError, String mesajeError, JComboBox<Object> campo, JComboBox<Object> campoPadre,
			JLabel etiqueta, Boolean obligatorio) {
		this(etiquetaError, mesajeError, campo, campoPadre, etiqueta, null, true);
	}

	public TxtCbx(JLabel etiquetaError, String mesajeError, JComboBox<Object> campo, TxtTexto campoTexto,
			JLabel etiqueta, Boolean obligatorio) {
		this(etiquetaError, mesajeError, campo, null, etiqueta, campoTexto, true);
	}

	public TxtCbx(JLabel etiquetaError, String mesajeError, JComboBox<Object> campo, JComboBox<Object> campoPadre,
			JLabel etiqueta, TxtTexto campoTexto, Boolean obligatorio) {
		super(etiquetaError, mesajeError, etiqueta, obligatorio);
		this.setCampo(campo);
		this.setCampoPadre(campoPadre);
		this.setCampoTexto(campoTexto);

		this.CambiarAColoresDefecto();
		this.CargarListener();
		try {
			getCampoTexto().DeshabilitarCampo();
			getCampoTexto().ValorPorDefecto();
		} catch (Exception e) {
		}

		this.getCampo().addItemListener(this.getCambioDeCampo());
		try {
			this.getCampoPadre().addItemListener(this.getCambioDeCampo());
			this.DeshabilitarCampo();
		} catch (NullPointerException e) {
		}
		this.getCampo().addFocusListener(this.getFocoDelCampo());
	}

	public void SetModelo(Object[] modelo) {
		this.getCampo().setModel(new DefaultComboBoxModel(modelo));
		ValorPorDefecto();
	}

	@Override
	public Boolean ConsisitirCampo() {
		if (!this.getObligatorio()) {
			if (getCampo().getSelectedIndex() == 0) {
				CambiarAColoresCorrectos();
				this.OcultarError();
				return true;
			}
		}
		if (this.getCampoActivo()) {
			if (getCampo().getSelectedIndex() != 0) {
				CambiarAColoresCorrectos();
				this.OcultarError();
				return true;
			}
			this.CambiarAColoresDeError();
			this.MostrarError();
			return false;
		}
		return false;
	}

	@Override
	public void CambiarAColoresDeError() {
		this.CambiarColorCampo(Color.RED);
		this.CambiarColorEtiqueta(Color.RED);
	}

	@Override
	public void CambiarAColoresCorrectos() {
		this.CambiarColorCampo(Color.GREEN);
		this.CambiarColorEtiqueta(Color.GREEN);
	}

	@Override
	public void CambiarAColoresDefecto() {
		this.CambiarColorCampo(Color.GRAY);
		this.CambiarColorEtiqueta(Color.BLACK);
	}

	@Override
	public void CambiarAColoresDeshabilitado() {
		this.CambiarColorCampo(Color.LIGHT_GRAY);
		this.CambiarColorEtiqueta(Color.LIGHT_GRAY);
	}

	@Override
	public void CambiarColorCampo(Color color) {
		this.getCampo().setBorder(BorderFactory.createLineBorder(color, 1));
	}

	@Override
	public void CambiarColorEtiqueta(Color color) {
		try {
			this.getEtiqueta().setForeground(color);
		} catch (NullPointerException e) {
		}
	}

	@Override
	public void ValorPorDefecto() {
		try {
			this.getCampo().setSelectedIndex(0);
		} catch (IllegalArgumentException e) {
		}
	}

	@Override
	public void HabiliarCampo() {
		this.getCampo().setEnabled(true);
		super.HabiliarCampo();
		this.setObligatorio(this.getAuxObligatorio());
	}

	@Override
	public void DeshabilitarCampo() {
		this.getCampo().setEnabled(false);
		super.DeshabilitarCampo();
		this.ValorPorDefecto();
		this.CambiarAColoresDefecto();
		this.setObligatorio(this.getAuxObligatorio());
	}

	@Override
	public void CargarListener() {
		this.setCambioDeCampo(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (getCampo().getSelectedIndex() != 0) {
					try {
						getCampoTexto().HabiliarCampo();
					} catch (NullPointerException e2) {
					}
				} else {
					try {
						getCampoTexto().DeshabilitarCampo();
						getCampoTexto().ValorPorDefecto();
					} catch (NullPointerException e2) {
					}
				}
				if (e.getSource() == getCampoPadre()) {
					if (((JComboBox<?>) e.getSource()).getSelectedIndex() == 0) {
						DeshabilitarCampo();
					} else {
						HabiliarCampo();
						ConsisitirCampo();
					}
				}
			}
		});
		super.CargarListener();
	}

	public ItemListener getCambioDeCampo() {
		return cambioDeCampo;
	}

	public void setCambioDeCampo(ItemListener cambioDeCampo) {
		this.cambioDeCampo = cambioDeCampo;
	}

	public JComboBox<?> getCampo() {
		return campo;
	}

	public void setCampo(JComboBox<Object> campo) {
		this.campo = campo;
	}

	public JComboBox<?> getCampoPadre() {
		return campoPadre;
	}

	public void setCampoPadre(JComboBox<Object> campoPadre) {
		this.campoPadre = campoPadre;
	}

	public TxtTexto getCampoTexto() {
		return campoTexto;
	}

	public void setCampoTexto(TxtTexto campoTexto) {
		this.campoTexto = campoTexto;
	}
}
