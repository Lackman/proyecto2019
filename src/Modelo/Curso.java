package Modelo;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

import Vista.AgregarCurso;

public class Curso {
	private String nombre;
	private Integer limiteDeCupos;
	private Date inicio, finalizacion;
	private Horario horario;
	private ArrayList<String[]> profesor;
	private Integer id;

	private static Curso instance;

	public static Curso deleteinstance() {
		instance = null;
		return getInstance();
	}

	public static Curso getInstance() {
		if (instance == null) {
			instance = new Curso();
		}
		return instance;
	}

	// Este metodo funciona correctamente
	public DefaultTableModel consultarTablaCursos(String busq) {
		String consulta = "SELECT curso.nombre_curso, curso.limite_cupos, curso.inicio_curso, curso.finalizacion_curso FROM public.curso where (curso.nombre_curso like '%"
				+ busq + "%' " + "or cast(curso.limite_cupos as varchar) like '%" + busq
				+ "%' or cast(curso.inicio_curso as varchar) like '%" + busq + "%' "
				+ "or cast(curso.finalizacion_curso as varchar) like '%" + busq + "%') and estado_curso = true;";
		ResultSet rs = null;
		DefaultTableModel modeloTabla = new DefaultTableModel() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}
		};
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);

			String[] titulos = new String[] { "Nombre del curso", "Limite de cupos", "Fecha de Inicio",
					"Fecha de Finalizacion" };
			modeloTabla.setColumnIdentifiers(titulos);
			ResultSetMetaData rsmd = null;
			try {
				rsmd = rs.getMetaData();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer nroCol = 0;

			try {
				nroCol = rsmd.getColumnCount();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				while (rs.next()) {
					Object[] objects = new Object[nroCol];
					for (int i = 0; i < 2; i++) {
						objects[i] = rs.getObject(i + 1);
					}
					for (int i = 2; i < 4; i++) {
						SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
						objects[i] = f.format(rs.getDate(i + 1));
					}
					modeloTabla.addRow(objects);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return modeloTabla;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	// se corrigio la consulta de buscar entidad
	public String[] consulColumna() {
		ResultSet rs = null;
		try {
			rs = BaseDeDatos.getInstance()
					.buscarEntidad("SELECT nombre, apellido FROM usuario where id_tipo_usuario = '3'");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ArrayList<String> str = new ArrayList<String>();
			Integer i = 0;
			while (rs.next()) {
				str.add(rs.getString(1) + " " + rs.getString(2));
				i++;
			}
			String[] str2 = new String[i + 1];
			for (int j = 1; j < str2.length; j++) {
				str2[j] = str.remove(0);
			}
			return str2;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Integer crearCurso(Curso curso) {
		Integer idCurso = null;
		try {
			String consulta = "INSERT INTO public.curso(nombre_curso, limite_cupos, inicio_curso, finalizacion_curso, estado_curso)VALUES ('"
					+ curso.nombre + "', " + curso.limiteDeCupos + ", '" + curso.inicio + "', '" + curso.finalizacion
					+ "', true);";
			BaseDeDatos.getInstance().insertar(consulta);
			System.out.println(consulta);

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			ResultSet rs = BaseDeDatos.getInstance()
					.consultar("Select id_curso from curso order by id_curso desc limit 1");
			Integer i = 1;
			while (rs.next()) {
				System.out.println(rs.getObject(i));
				idCurso = Integer.parseInt(rs.getObject(i).toString());
				i++;
			}
		} catch (SQLException e) {
			// TODO: handle exception
		}
		return idCurso;
	}

	public void AgregarCursoProfesor(Integer idCurso, String idProfesor) {
		try {
			BaseDeDatos.getInstance().consultar("INSERT INTO public.curso_profesor(id_profesor, id_curso) VALUES ("
					+ idProfesor + ", " + idCurso + ");");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void AgregarCursoHorario(Integer idCurso, Curso curso) {
		for (int i = 0; i < curso.horario.getDiaDeSemana().size(); i++) {
			String c = "INSERT INTO public.horario(dia_horario, horario_inicio, horario_fin, lugar, curso_horario) VALUES ('"
					+ curso.horario.getDiaDeSemana().get(i) + "', '" + curso.horario.getHoraInicio().get(i) + "', '"
					+ curso.horario.getHoraFinal().get(i) + "', '" + curso.horario.getLugarDeLaActividad().get(i)
					+ "', " + idCurso + ");";
			System.out.println(c);
			try {
				BaseDeDatos.getInstance().insertar(c);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Curso obtenerCurso(String nombreCurso) {
		String consulta = "select * from curso where curso.nombre_curso = '" + nombreCurso
				+ "' and curso.estado_curso = true";
		ResultSet rs = null;
		Curso c = new Curso();
		try {
			rs = BaseDeDatos.getInstance().buscarEntidad(consulta);
			while (rs.next()) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
				try {
					c.setNombre(rs.getObject(1).toString());
					c.setLimiteDeCupos(Integer.parseInt(rs.getObject(2).toString()));
					c.setFinalizacion(formatter.parse(rs.getObject(4).toString()));
					c.setId(Integer.parseInt(rs.getObject(5).toString()));
					c.setInicio(formatter.parse(rs.getObject(3).toString()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return c;
	}

	public Horario obtenerHorario(Integer id) {
		String consulta = "select * from horario where horario.curso_horario =" + id + ";";
		System.out.println(consulta);
		ResultSet rs = null;
		Horario h = new Horario();
		try {
			rs = BaseDeDatos.getInstance().buscarEntidad(consulta);
			while (rs.next()) {
				h.getHoraInicio().add(LocalTime.parse(rs.getObject(2).toString()));
				h.getHoraFinal().add(LocalTime.parse(rs.getObject(3).toString()));
				h.getLugarDeLaActividad().add(rs.getObject(4).toString());
				h.getDiaDeSemana().add(rs.getObject(6).toString());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return h;
	}

	public ArrayList<String[]> obtenerProfesor(Integer id) {
		System.out.println("ID>"+id);
		String consulta = "select * from curso_profesor where curso_profesor.id_curso =" + id + ";";
		ResultSet rs = null;
		ArrayList<String[]> p = new ArrayList<String[]>();
		try {
			rs = BaseDeDatos.getInstance().buscarEntidad(consulta);
			while (rs.next()) {
				ResultSet rs2 = BaseDeDatos.getInstance()
						.consultar("select * from usuario where dni=" + rs.getObject(2).toString());
				while (rs2.next()) {
					String[] s = { rs2.getObject(2).toString(), rs2.getObject(3).toString() };
					p.add(s);
				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return p;
	}

	public void cambiarEstadoCurso(String nombreCurso) {
		String consultaEstado = "select estado_curso from curso where nombre_curso = '" + nombreCurso + "'";
		String estadoCad = "";
		ResultSet rs;
		try {
			rs = BaseDeDatos.getInstance().consultar(consultaEstado);
			while (rs.next()) {
				estadoCad = rs.getString(1);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Boolean estado;
		if (estadoCad.equals("f")) {
			estado = true;
		} else {
			estado = false;
		}

		String consulta = "UPDATE public.curso SET estado_curso= " + estado + " where curso.nombre_curso = '"
				+ nombreCurso + "';";

		try {
			BaseDeDatos.getInstance().insertar(consulta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void actualizarHorario(Integer id, Curso curso) {
		String consulta = "delete from horario where curso_horario =" + id + ";";
		System.out.println(consulta);
		ResultSet rs = null;
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.AgregarCursoHorario(id, curso);
	}

	public void actualizarProfesor(Integer id, Curso curso) {
		String consulta = "delete from curso_profesor where curso_profesor.id_curso =" + id + ";";
		ResultSet rs = null;
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);
			
		} catch (SQLException e) {

			e.printStackTrace();
		}
		System.out.println(curso.getProfesor().size());
		for (int i = 0; i < curso.getProfesor().size(); i++) {
			String c = "select dni from usuario where nombre = '" + curso.getProfesor().get(i)[0]
					+ "' and apellido = '" + curso.getProfesor().get(i)[1]
					+ "' and estado=true and id_tipo_usuario = 4";
			System.out.println(c);
			try {
				rs = BaseDeDatos.getInstance().buscarEntidad(c);
				if (rs.next()) {
					AgregarCursoProfesor(id, rs.getObject(1).toString());
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	public void actualizarCurso(Curso curso) {
		String consulta = "UPDATE curso SET nombre_curso= '" + curso.getNombre() + "', inicio_curso= '"
				+ curso.getInicio() + "', finalizacion_curso= '" + curso.getFinalizacion() + "', limite_cupos = "
				+ curso.getLimiteDeCupos() + " where id_curso = " + curso.getId() + ";";
		System.out.println(consulta);
		try {
			BaseDeDatos.getInstance().consultar(consulta);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.actualizarHorario(curso.id, curso);
		this.actualizarProfesor(curso.id, curso);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getLimiteDeCupos() {
		return limiteDeCupos;
	}

	public void setLimiteDeCupos(Integer limiteDeCupos) {
		this.limiteDeCupos = limiteDeCupos;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFinalizacion() {
		return finalizacion;
	}

	public void setFinalizacion(Date finalizacion) {
		this.finalizacion = finalizacion;
	}

	public ArrayList<String[]> getProfesor() {
		return profesor;
	}

	public void setProfesor(ArrayList<String[]> profesor) {
		this.profesor = profesor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
