package Modelo;

import java.awt.Color;
import java.util.Calendar;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class TxtHorario extends Txt {
	private JSpinner primerCampo;
	private JSpinner segundoCampo;
	private JLabel segundaEtiqueta;
	private String formatoHora = "HH:mm";
	private ChangeListener cambiarHora;

	public TxtHorario(JLabel etiquetaError, String mesajeError, JSpinner primerCampo, JSpinner segundoCampo,
			JLabel etiqueta, JLabel segundaEtiqueta) {
		this(etiquetaError, mesajeError, primerCampo, segundoCampo, etiqueta, segundaEtiqueta, false);
	}

	public TxtHorario(JLabel etiquetaError, String mesajeError, JSpinner primerCampo, JSpinner segundoCampo,
			JLabel etiqueta, JLabel segundaEtiqueta, Boolean obligatorio) {
		super(etiquetaError, mesajeError, etiqueta, obligatorio);
		this.setPrimerCampo(primerCampo);
		this.setSegundoCampo(segundoCampo);
		this.setSegundaEtiqueta(segundaEtiqueta);

		this.CambiarAColoresDefecto();
		this.ValorPorDefecto();
		this.CargarListener();

		this.getPrimerCampo().addChangeListener(this.getCambiarHora());
		this.getSegundoCampo().addChangeListener(this.getCambiarHora());
	}

	public Boolean ConsisitirMaximo() {
		return (((Date) this.getPrimerCampo().getValue()).getTime() < ((Date) this.getSegundoCampo().getValue())
				.getTime());
	}

	@Override
	public Boolean ConsisitirCampo() {
		if (this.getCampoActivo()) {
			if (this.ConsisitirMaximo()) {
				CambiarAColoresCorrectos();
				this.OcultarError();
				return true;
			}
			this.MostrarError();
			this.CambiarAColoresDeError();
			return false;
		}
		return true;
	}

	public void CambiarAColoresDeError() {
		this.CambiarColorCampo(Color.RED);
		this.CambiarColorEtiqueta(Color.RED);
	}

	@Override
	public void CambiarAColoresCorrectos() {
		this.CambiarColorCampo(Color.GREEN);
		this.CambiarColorEtiqueta(Color.GREEN);
	}

	@Override
	public void CambiarAColoresDefecto() {
		this.CambiarColorCampo(Color.GRAY);
		this.CambiarColorEtiqueta(Color.BLACK);
	}

	@Override
	public void CambiarAColoresDeshabilitado() {
		this.CambiarColorCampo(Color.LIGHT_GRAY);
		this.CambiarColorEtiqueta(Color.LIGHT_GRAY);
	}

	@Override
	public void CambiarColorCampo(Color color) {
		this.getPrimerCampo().setBorder(BorderFactory.createLineBorder(color, 1));
		this.getSegundoCampo().setBorder(BorderFactory.createLineBorder(color, 1));
	}

	@Override
	public void CambiarColorEtiqueta(Color color) {
		this.getEtiqueta().setForeground(color);
		this.getSegundaEtiqueta().setForeground(color);
	}

	@Override
	public void ValorPorDefecto() {
		this.getPrimerCampo()
				.setModel(new SpinnerDateModel(new Date(new Date().getTime()), null, null, Calendar.HOUR_OF_DAY));
		this.getSegundoCampo()
				.setModel(new SpinnerDateModel(new Date(new Date().getTime()), null, null, Calendar.HOUR_OF_DAY));
		this.getPrimerCampo().setEditor(new JSpinner.DateEditor(this.getPrimerCampo(), this.getFormatoHora()));
		this.getSegundoCampo().setEditor(new JSpinner.DateEditor(this.getSegundoCampo(), this.getFormatoHora()));
		this.getSegundoCampo().setValue(this.getSegundoCampo().getNextValue());
	}

	@Override
	public void CargarListener() {
		this.setCambiarHora(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				ConsisitirCampo();
			}
		});
		super.CargarListener();
	}

	public JSpinner getPrimerCampo() {
		return primerCampo;
	}

	public void setPrimerCampo(JSpinner primerCampo) {
		this.primerCampo = primerCampo;
	}

	public JSpinner getSegundoCampo() {
		return segundoCampo;
	}

	public void setSegundoCampo(JSpinner segundoCampo) {
		this.segundoCampo = segundoCampo;
	}

	public String getFormatoHora() {
		return formatoHora;
	}

	public void setFormatoHora(String formatoHora) {
		this.formatoHora = formatoHora;
	}

	public JLabel getSegundaEtiqueta() {
		return segundaEtiqueta;
	}

	public void setSegundaEtiqueta(JLabel segundaEtiqueta) {
		this.segundaEtiqueta = segundaEtiqueta;
	}

	public ChangeListener getCambiarHora() {
		return cambiarHora;
	}

	public void setCambiarHora(ChangeListener cambiarHora) {
		this.cambiarHora = cambiarHora;
	}
}
