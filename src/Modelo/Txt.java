package Modelo;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JLabel;

public abstract class Txt {
	// Atributos
	private KeyListener teclaApretada;
	private FocusListener focoDelCampo;
	private JLabel etiquetaError;
	private String mesajeError;
	private JLabel etiqueta;
	private Boolean obligatorio;
	private Boolean auxObligatorio;
	private Boolean campoActivo;

	public Txt(JLabel etiquetaError, String mesajeError, JLabel etiqueta, Boolean obligatorio) {
		this.setEtiquetaError(etiquetaError);
		this.setMesajeError(mesajeError);
		this.setEtiqueta(etiqueta);
		this.setObligatorio(obligatorio);
		this.setAuxObligatorio(obligatorio);
		this.setCampoActivo(true);
	}

	// Devuelve si existe un error
	public abstract Boolean ConsisitirCampo();

	public abstract void CambiarColorCampo(Color color);

	public abstract void CambiarColorEtiqueta(Color color);

	public abstract void CambiarAColoresDeError();

	public abstract void CambiarAColoresCorrectos();

	public abstract void CambiarAColoresDefecto();
	
	public abstract void CambiarAColoresDeshabilitado();
	
	public abstract void ValorPorDefecto();

	public void CargarListener() {
		this.setFocoDelCampo(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				ConsisitirCampo();
			}

			@Override
			public void focusGained(FocusEvent e) {
				CambiarAColoresDefecto();
			}
		});
	}
	
	public void DeshabilitarCampo() {
		this.setCampoActivo(false);
		this.setObligatorio(false);
	}
	
	public void HabiliarCampo() {
		this.setCampoActivo(true);
		this.setObligatorio(this.getAuxObligatorio());
	}

	public void MostrarError() {
		try {
			this.getEtiquetaError().setText(this.getMesajeError());
		this.getEtiquetaError().setVisible(true);
		} catch (NullPointerException e) {
		}
		
	}

	public void OcultarError() {
		try {
			this.getEtiquetaError().setVisible(false);
		} catch (NullPointerException e) {
		}
		
	}

	public static Boolean ConsistirCampos(ArrayList<Txt> campos) {
		for (Txt txt : campos) {
			if (!txt.ConsisitirCampo()) {
				return false;
			}
		}
		return true;
	}
	public KeyListener getTeclaApretada() {
		return teclaApretada;
	}
	public void setTeclaApretada(KeyListener teclaApretada) {
		this.teclaApretada = teclaApretada;
	}
	public FocusListener getFocoDelCampo() {
		return focoDelCampo;
	}
	public void setFocoDelCampo(FocusListener focoDelCampo) {
		this.focoDelCampo = focoDelCampo;
	}

	public JLabel getEtiquetaError() {
		return etiquetaError;
	}

	public void setEtiquetaError(JLabel etiquetaError) {
		this.etiquetaError = etiquetaError;
	}

	public String getMesajeError() {
		return mesajeError;
	}

	public void setMesajeError(String mesajeError) {
		this.mesajeError = mesajeError;
	}

	public Boolean getObligatorio() {
		return obligatorio;
	}
	public void setObligatorio(Boolean obligatorio) {
		this.obligatorio = obligatorio;
	}
	public JLabel getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(JLabel etiqueta) {
		this.etiqueta = etiqueta;
	}

	public Boolean getAuxObligatorio() {
		return auxObligatorio;
	}

	public void setAuxObligatorio(Boolean auxObligatorio) {
		this.auxObligatorio = auxObligatorio;
	}

	public Boolean getCampoActivo() {
		return campoActivo;
	}

	public void setCampoActivo(Boolean campoActivo) {
		this.campoActivo = campoActivo;
	}

}
