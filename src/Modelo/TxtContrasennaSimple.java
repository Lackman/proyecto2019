package Modelo;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

public class TxtContrasennaSimple extends Txt {
	private JPasswordField Campo;
	private Integer maximoCaracteres;
	private Integer minimoCaracteres;
	private char[] contrasennaDefecto;

	public TxtContrasennaSimple(JLabel etiquetaError, String mesajeError, JPasswordField txtCampo,
			JLabel etiqueta) {
		this(etiquetaError, mesajeError, txtCampo, etiqueta, false, 0, 100, "".toCharArray());
	}
	
	public TxtContrasennaSimple(JLabel etiquetaError, String mesajeError, JPasswordField txtCampo,
			JLabel etiqueta, Boolean obligatorio, Integer mininoCaracteres, Integer maximoCaracteres) {
		this(etiquetaError, mesajeError, txtCampo, etiqueta, obligatorio, mininoCaracteres, maximoCaracteres, "".toCharArray());
	}

	public TxtContrasennaSimple(JLabel etiquetaError, String mesajeError, JPasswordField txtCampo,
			JLabel etiqueta, Boolean obligatorio, Integer mininoCaracteres, Integer maximoCaracteres, char[] contrasennaDefecto) {
		super(etiquetaError, mesajeError, etiqueta, obligatorio);
		this.setCampo(txtCampo);
		this.CambiarAColoresDefecto();
		this.setMinimoCaracteres(mininoCaracteres);
		this.setMaximoCaracteres(maximoCaracteres);
		this.setContrasennaDefecto(contrasennaDefecto);
		this.ValorPorDefecto();
		this.CargarListener();
		this.getCampo().addKeyListener(this.getTeclaApretada());
		this.getCampo().addFocusListener(this.getFocoDelCampo());
	}

	@Override
	public void ValorPorDefecto() {
		this.getCampo().setText(String.copyValueOf(this.getContrasennaDefecto()));
	}
	
	public void SetText(char[] contrasenna) {
		this.getCampo().setText(String.copyValueOf(contrasenna));
	}
	
	public Boolean ConsisitirMaximo() {
		return this.getCampo().getPassword().length >= this.getMaximoCaracteres();
	}

	public Boolean ConsisitirMinimo() {
		return getCampo().getPassword().length >= this.getMinimoCaracteres();
	}

	public Boolean ConsisitirMaximoYMinimo() {
		return this.ConsisitirMinimo() && this.getCampo().getPassword().length <= this.getMaximoCaracteres();
	}

	@Override
	public Boolean ConsisitirCampo() {
		if (this.getCampoActivo()) {
			if (!this.getObligatorio()) {
				if (this.getCampo().getPassword().length == 0) {
					CambiarAColoresCorrectos();
					this.OcultarError();
					return true;
				}
			}

			if (this.ConsisitirMaximoYMinimo()) {
				CambiarAColoresCorrectos();
				this.OcultarError();
				return true;
			}
			this.CambiarAColoresDeError();
			this.MostrarError();
			return false;
		}
		return true;
	}

	public void DeshabilitarCampo() {
		this.getCampo().setEnabled(false);
		super.DeshabilitarCampo();
	}

	@Override
	public void HabiliarCampo() {
		this.getCampo().setEnabled(true);
		super.HabiliarCampo();
	}

	public void CambiarAColoresDeError() {
		this.CambiarColorCampo(Color.RED);
		this.CambiarColorEtiqueta(Color.RED);
	}

	@Override
	public void CambiarAColoresCorrectos() {
		this.CambiarColorCampo(Color.GREEN);
		this.CambiarColorEtiqueta(Color.GREEN);
	}

	@Override
	public void CambiarAColoresDefecto() {
		this.CambiarColorCampo(Color.GRAY);
		this.CambiarColorEtiqueta(Color.BLACK);
	}

	@Override
	public void CambiarAColoresDeshabilitado() {
		this.CambiarColorCampo(Color.LIGHT_GRAY);
		this.CambiarColorEtiqueta(Color.LIGHT_GRAY);
	}

	@Override
	public void CambiarColorCampo(Color color) {
		this.getCampo().setBorder(BorderFactory.createLineBorder(color, 1));
	}

	@Override
	public void CambiarColorEtiqueta(Color color) {
		this.getEtiqueta().setForeground(color);
	}

	@Override
	public void CargarListener() {
		this.setTeclaApretada(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (ConsisitirMaximo()) {
					e.consume();
					Toolkit.getDefaultToolkit().beep();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				ConsisitirCampo();
			}

			@Override
			public void keyPressed(KeyEvent e) {

			}
		});
		super.CargarListener();
	}

	public JPasswordField getCampo() {
		return Campo;
	}

	public void setCampo(JPasswordField campo) {
		Campo = campo;
	}

	public Integer getMaximoCaracteres() {
		return maximoCaracteres;
	}

	public void setMaximoCaracteres(Integer maximoCaracteres) {
		this.maximoCaracteres = maximoCaracteres;
	}

	public Integer getMinimoCaracteres() {
		return minimoCaracteres;
	}

	public void setMinimoCaracteres(Integer minimoCaracteres) {
		this.minimoCaracteres = minimoCaracteres;
	}

	public char[] getContrasennaDefecto() {
		return contrasennaDefecto;
	}

	public void setContrasennaDefecto(char[] contrasennaDefecto) {
		this.contrasennaDefecto = contrasennaDefecto;
	}

}
