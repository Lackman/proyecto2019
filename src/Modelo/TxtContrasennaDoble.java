package Modelo;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

public class TxtContrasennaDoble extends Txt {
	private JPasswordField primerCampo;
	private JPasswordField segundoCampo;
	private Integer maximoCaracteres;
	private Integer minimoCaracteres;
	private JLabel segundaEtiqueta;
	private char[] contrasennaDefectoPrimero;
	private char[] contrasennaDefectoSegundo;

	public TxtContrasennaDoble(JLabel etiquetaError, String mesajeError, JPasswordField txtPrimerCampo,
			JPasswordField txtSegundoCampo, JLabel primeraEtiqueta, JLabel segundaEtiqueta) {
		this(etiquetaError, mesajeError, txtPrimerCampo, txtSegundoCampo, primeraEtiqueta, segundaEtiqueta, false, 0,
				100, "".toCharArray(), "".toCharArray());
	}

	public TxtContrasennaDoble(JLabel etiquetaError, String mesajeError, JPasswordField txtPrimerCampo,
			JPasswordField txtSegundoCampo, JLabel primeraEtiqueta, JLabel segundaEtiqueta, char[] contrasennaDefectoPrimero, char[] contrasennaDefectoSegundo) {
		this(etiquetaError, mesajeError, txtPrimerCampo, txtSegundoCampo, primeraEtiqueta, segundaEtiqueta, true, 1, 100, contrasennaDefectoPrimero, contrasennaDefectoSegundo);
	}
	
	public TxtContrasennaDoble(JLabel etiquetaError, String mesajeError, JPasswordField txtPrimerCampo,
			JPasswordField txtSegundoCampo, JLabel primeraEtiqueta, JLabel segundaEtiqueta, Boolean obligatorio,
			Integer mininoCaracteres, Integer maximoCaracteres) {
		this(etiquetaError, mesajeError, txtPrimerCampo, txtSegundoCampo, primeraEtiqueta, segundaEtiqueta, obligatorio, mininoCaracteres, maximoCaracteres, "".toCharArray(), "".toCharArray());
	}
	
	public TxtContrasennaDoble(JLabel etiquetaError, String mesajeError, JPasswordField txtPrimerCampo,
			JPasswordField txtSegundoCampo, JLabel primeraEtiqueta, JLabel segundaEtiqueta, Boolean obligatorio,
			Integer mininoCaracteres, Integer maximoCaracteres, char[] contrasennaDefectoPrimero, char[] contrasennaDefectoSegundo) {
		super(etiquetaError, mesajeError, primeraEtiqueta, obligatorio);
		this.setSegundaEtiqueta(segundaEtiqueta);
		this.setPrimerCampo(txtPrimerCampo);
		this.setSegundoCampo(txtSegundoCampo);
		this.CambiarAColoresDefecto();
		this.setMinimoCaracteres(mininoCaracteres);
		this.setMaximoCaracteres(maximoCaracteres);
		this.setContrasennaDefectoPrimero(contrasennaDefectoPrimero);
		this.setContrasennaDefectoSegundo(contrasennaDefectoSegundo);
		this.ValorPorDefecto();
		this.CargarListener();
		this.getPrimerCampo().addKeyListener(this.getTeclaApretada());
		this.getPrimerCampo().addFocusListener(this.getFocoDelCampo());
		this.getSegundoCampo().addKeyListener(this.getTeclaApretada());
		this.getSegundoCampo().addFocusListener(this.getFocoDelCampo());
	}

	@Override
	public void ValorPorDefecto() {
		this.getPrimerCampo().setText(String.copyValueOf(this.getContrasennaDefectoPrimero()));
		this.getSegundoCampo().setText(String.copyValueOf(this.getContrasennaDefectoSegundo()));
	}
	
	public void SetText(char[] contrasennaPrimerCampo, char[] contrasennaSegundoCampo) {
		this.getPrimerCampo().setText(String.copyValueOf(contrasennaPrimerCampo));
		this.getSegundoCampo().setText(String.copyValueOf(contrasennaSegundoCampo));
	}
	
	public Boolean ConsisitirMaximo(JPasswordField campo) {
		return campo.getPassword().length >= this.getMaximoCaracteres();
	}

	public Boolean ConsisitirMinimo(JPasswordField campo) {
		return campo.getPassword().length >= this.getMinimoCaracteres();
	}

	public Boolean ConsisitirMaximoYMinimo(JPasswordField campo) {
		return this.ConsisitirMinimo(campo) && campo.getPassword().length <= this.getMaximoCaracteres();
	}

	private Boolean CompararContrasennas(char[] campo1, char[] campo2) {
		Integer puntero = 0;
		if (campo1.length != campo2.length) {
			return false;
		} else {
			while (puntero < campo1.length) {
				if (campo1[puntero] != campo2[puntero]) {
					return false;
				}
				puntero++;
			}
		}
		return true;
	}

	public Boolean ConsisitirCampo() {
		if (this.getCampoActivo()) {
			if (!this.getObligatorio()) {
				if (this.getPrimerCampo().getPassword().length == 0
						&& this.getSegundoCampo().getPassword().length == 0) {
					CambiarAColoresCorrectos();
					this.OcultarError();
					return true;
				}
			}

			if (this.ConsisitirMaximoYMinimo(this.getPrimerCampo())) {
				if (CompararContrasennas(this.getPrimerCampo().getPassword(), this.getSegundoCampo().getPassword())) {
					CambiarAColoresCorrectos();
					this.OcultarError();
					return true;
				}
			}
			this.CambiarAColoresDeError();
			this.MostrarError();
			return false;
		}
		return true;
	}

	public void DeshabilitarCampo() {
		this.getPrimerCampo().setEnabled(false);
		this.getSegundoCampo().setEnabled(false);
		super.DeshabilitarCampo();
	}

	@Override
	public void HabiliarCampo() {
		this.getPrimerCampo().setEnabled(true);
		this.getSegundoCampo().setEnabled(true);
		super.HabiliarCampo();
	}

	public void CambiarAColoresDeError() {
		this.CambiarColorCampo(Color.RED);
		this.CambiarColorEtiqueta(Color.RED);
	}

	@Override
	public void CambiarAColoresCorrectos() {
		this.CambiarColorCampo(Color.GREEN);
		this.CambiarColorEtiqueta(Color.GREEN);
	}

	@Override
	public void CambiarAColoresDefecto() {
		this.CambiarColorCampo(Color.GRAY);
		this.CambiarColorEtiqueta(Color.BLACK);
	}

	@Override
	public void CambiarAColoresDeshabilitado() {
		this.CambiarColorCampo(Color.LIGHT_GRAY);
		this.CambiarColorEtiqueta(Color.LIGHT_GRAY);
	}

	@Override
	public void CambiarColorCampo(Color color) {
		this.getPrimerCampo().setBorder(BorderFactory.createLineBorder(color, 1));
		this.getSegundoCampo().setBorder(BorderFactory.createLineBorder(color, 1));
	}

	@Override
	public void CambiarColorEtiqueta(Color color) {
		this.getEtiqueta().setForeground(color);
		this.getSegundaEtiqueta().setForeground(color);
	}

	@Override
	public void CargarListener() {
		this.setTeclaApretada(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (ConsisitirMaximo((JPasswordField) e.getSource())) {
					e.consume();
					Toolkit.getDefaultToolkit().beep();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				ConsisitirCampo();
			}

			@Override
			public void keyPressed(KeyEvent e) {

			}
		});
		super.CargarListener();
	}

	public JPasswordField getPrimerCampo() {
		return primerCampo;
	}

	public void setPrimerCampo(JPasswordField primerCampo) {
		this.primerCampo = primerCampo;
	}

	public JPasswordField getSegundoCampo() {
		return segundoCampo;
	}

	public void setSegundoCampo(JPasswordField segundoCampo) {
		this.segundoCampo = segundoCampo;
	}

	public Integer getMaximoCaracteres() {
		return maximoCaracteres;
	}

	public void setMaximoCaracteres(Integer maximoCaracteres) {
		this.maximoCaracteres = maximoCaracteres;
	}

	public Integer getMinimoCaracteres() {
		return minimoCaracteres;
	}

	public void setMinimoCaracteres(Integer minimoCaracteres) {
		this.minimoCaracteres = minimoCaracteres;
	}

	public JLabel getSegundaEtiqueta() {
		return segundaEtiqueta;
	}

	public void setSegundaEtiqueta(JLabel segundaEtiqueta) {
		this.segundaEtiqueta = segundaEtiqueta;
	}

	public char[] getContrasennaDefectoPrimero() {
		return contrasennaDefectoPrimero;
	}

	public void setContrasennaDefectoPrimero(char[] contrasennaDefectoPrimero) {
		this.contrasennaDefectoPrimero = contrasennaDefectoPrimero;
	}

	public char[] getContrasennaDefectoSegundo() {
		return contrasennaDefectoSegundo;
	}

	public void setContrasennaDefectoSegundo(char[] contrasennaDefectoSegundo) {
		this.contrasennaDefectoSegundo = contrasennaDefectoSegundo;
	}

}
