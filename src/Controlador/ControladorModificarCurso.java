package Controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import Modelo.Curso;
import Modelo.Horario;
import Modelo.Usuario;
import Vista.AgregarCurso;
import Vista.AgregarProfesor;

public class ControladorModificarCurso extends ControladorPadre {
	private Curso curso;
	private ArrayList<String[]> profesores;
	private Boolean[] bandera;
	// Orden de las banderas: Nombre = 0, Inicio = 1, Finalizacion = 2, Profesores =
	// 3, Limite de cupo = 4, Horarios = 5
	private Horario horario;
	private static ControladorModificarCurso instance;
	private AgregarCurso vistaModificarCurso;
	private ActionListener listenerModificar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorHorario.deleteInstance(ControladorModificarCurso.getInstance().getCurso()).getVistaHorario().setVisible(true);
		}
	};
	private ActionListener listenerCancelar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			String tipo = Usuario.getInstance().getTipo();
			getVistaModificarCurso().setVisible(false);
			if (tipo.equals("1")) {
				ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
			}
			if (tipo.equals("2")) {
				ControladorVistaSecretaria.deleteinstance().getVistaSecretaria().setVisible(true);
			}
		}
	};

	private ActionListener listenerGuardar = new ActionListener() {
		
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorModificarCurso.getInstance().getVistaModificarCurso().dispose();
			ControladorVistaAdmin.getInstance().actualizarTabla2("");
			ControladorVistaAdmin.deleteInstance().getVistaAdmin().setVisible(true);
			ControladorModificarCurso.getInstance().getCurso().setNombre(ControladorModificarCurso.getInstance().getVistaModificarCurso().getTxtNombre().getText());
			ControladorModificarCurso.getInstance().getCurso().setInicio(ControladorModificarCurso.getInstance().getVistaModificarCurso().getDateInicio().getDate());
			ControladorModificarCurso.getInstance().getCurso().setFinalizacion(ControladorModificarCurso.getInstance().getVistaModificarCurso().getDateFinalizacion().getDate());
			ControladorModificarCurso.getInstance().getCurso().setLimiteDeCupos(Integer.parseInt(ControladorModificarCurso.getInstance().getVistaModificarCurso().getSpLimiteDeCupos().getText()));
			Curso.getInstance().actualizarCurso(ControladorModificarCurso.getInstance().getCurso());
		}
	};
	private ActionListener lisenerProfesores = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorProfesores.deleteInstance().getVistaAgregarProfesor().setVisible(true);
		}
	};
	public static ControladorModificarCurso getInstance() {
		if (instance == null) {
			instance = new ControladorModificarCurso();
		}
		return instance;
	}
	public ControladorModificarCurso() {
		this.setVistaModificarCurso(new AgregarCurso(this));
		this.getVistaModificarCurso().getBtnHorarios().addActionListener(this.getListenerModificar());
		this.getVistaModificarCurso().getBtnCancelar().addActionListener(this.getListenerCancelar());
		this.getVistaModificarCurso().getBtnProfesores().addActionListener(this.getLisenerProfesores());
		this.getVistaModificarCurso().getBtnGuardar().addActionListener(this.getListenerGuardar());
	}
	
	public void MostrarDatos() {
		this.getVistaModificarCurso().getTxtNombre().setText(this.getCurso().getNombre());
		this.getVistaModificarCurso().getDateInicio().setDate(this.getCurso().getInicio());
		this.getVistaModificarCurso().getDateFinalizacion().setDate(this.getCurso().getFinalizacion());
		this.getVistaModificarCurso().getSpLimiteDeCupos().setText(this.getCurso().getLimiteDeCupos().toString());
		ControladorHorario.getInstance(this.getCurso());
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Boolean[] getBandera() {
		return bandera;
	}

	public void setBandera(Boolean[] bandera) {
		this.bandera = bandera;
	}

	public ArrayList<String[]> getProfesores() {
		return profesores;
	}

	public void setProfesores(ArrayList<String[]> profesores) {
		this.profesores = profesores;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}
	public AgregarCurso getVistaModificarCurso() {
		return vistaModificarCurso;
	}
	public void setVistaModificarCurso(AgregarCurso vistaModificarCurso) {
		this.vistaModificarCurso = vistaModificarCurso;
	}
	public ActionListener getListenerModificar() {
		return listenerModificar;
	}
	public void setListenerModificar(ActionListener listenerModificar) {
		this.listenerModificar = listenerModificar;
	}
	public ActionListener getListenerCancelar() {
		return listenerCancelar;
	}
	public void setListenerCancelar(ActionListener listenerCancelar) {
		this.listenerCancelar = listenerCancelar;
	}
	public ActionListener getLisenerProfesores() {
		return lisenerProfesores;
	}
	public void setLisenerProfesores(ActionListener lisenerBtnProfesores) {
		this.lisenerProfesores = lisenerBtnProfesores;
	}
	public ActionListener getListenerGuardar() {
		return listenerGuardar;
	}
	public void setListenerGuardar(ActionListener listenerGuardar) {
		this.listenerGuardar = listenerGuardar;
	}

}