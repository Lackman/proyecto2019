package Controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import Modelo.Hash;
import Modelo.Usuario;
import Vista.VistaCambioContrasenia;

public class ControladorCambioContrasenia extends ControladorPadre {
	private static ControladorCambioContrasenia instance;
	private Usuario user;
	private VistaCambioContrasenia vistaCambioContrasenia;
	private Boolean[] bandera;
	
	public static ControladorCambioContrasenia deleteInstance() {
		instance = null;
		return getInstance();
	}
	
	public static ControladorCambioContrasenia getInstance() {
		if (instance == null) {
			instance = new ControladorCambioContrasenia();
		}
		return instance;
	}


	public ControladorCambioContrasenia() {
		this.setVistaCambioContrasenia(new VistaCambioContrasenia(this));
		this.getVistaCambioContrasenia().setVisible(true);
		this.setBandera(new Boolean[2]);
		super.inicializadorDeBandera(this.getBandera());
		this.getVistaCambioContrasenia().getTxtContrasenia().addFocusListener(this.getListenerFocusTxtContrasennaCambio());
		this.getVistaCambioContrasenia().getTxtConfirmarContrasenia().addFocusListener(this.getListenerFocusTxtCambioContrasennaCambio());
		this.getVistaCambioContrasenia().getTxtContrasenia().addKeyListener(this.getKeyTxtContrasenna());
		this.getVistaCambioContrasenia().getTxtConfirmarContrasenia().addKeyListener(this.getKeyTxtConfirmatContrasenna());
		this.getVistaCambioContrasenia().getBtnGuardar().addActionListener(this.getActionBtnGuardar());
		this.getVistaCambioContrasenia().getBtnCancelar().addActionListener(this.getListenerBtnCancelar());
			
	}
	private ActionListener actionBtnGuardar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (ControladorCambioContrasenia.getInstance().revisarbanderas(ControladorCambioContrasenia.getInstance().getBandera()) == ControladorCambioContrasenia.getInstance().getBandera().length) {
				if (e.getSource().equals(ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getBtnGuardar())) {
					Usuario.getInstance().ActualizarContrasenna(ControladorLogin.getInstance().getLogin().getTxtUsuario().getText(), Hash.md5(ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getTxtContrasenia().getText()));;
					JOptionPane.showMessageDialog(getAgregarUsuario(), "Guardado correctamente");
					ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().setVisible(false);
					ControladorLogin.deleteInstance().getLogin().setVisible(true);
				}
								
			}
		}
	};
	private FocusListener listenerFocusTxtContrasennaCambio = new FocusListener() {
		
		@Override
		public void focusLost(FocusEvent e) {
			if (ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getTxtContrasenia().getText().isEmpty()) {
				ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblContrasenia().setForeground(Color.RED);
				ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError().setVisible(true);
				ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError2().setText("Se debe ingresar una contraseņa");
				ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError2().setVisible(true);
				ControladorCambioContrasenia.getInstance().getBandera()[0] = false;
			}else {
				ControladorCambioContrasenia.getInstance().getBandera()[0] = true;
			}
		}

		@Override
		public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub
			
		}
	};
		private FocusListener listenerFocusTxtCambioContrasennaCambio = new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if (ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getTxtConfirmarContrasenia().getText().isEmpty()) {
					ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblConfirmarContrasenia().setForeground(Color.RED);
					ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError().setVisible(true);
					ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError2().setText("Las contraseņas deben ser iguales");
					ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError2().setVisible(true);
				}else {
					ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError2().setVisible(false);
					
				}
			}
		@Override
		public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub
			
		}
	};
	private KeyListener keyTxtContrasenna = new KeyListener() {
		
		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void keyReleased(KeyEvent e) {
			if (ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getTxtContrasenia().getText()
					.equals(getVistaCambioContrasenia().getTxtConfirmarContrasenia().getText())) {
				ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblConfirmarContrasenia().setForeground(Color.BLACK);
				ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError().setVisible(false);
				ControladorCambioContrasenia.getInstance().getBandera()[0] = true;
			}else {
				ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblContrasenia().setForeground(Color.BLACK);
				ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError().setVisible(false);
				ControladorCambioContrasenia.getInstance().getBandera()[0] = true;
			}
		}
		
		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
	};
private KeyListener keyTxtConfirmatContrasenna = new KeyListener() {
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		if (ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getTxtContrasenia().getText()
				.equals(ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getTxtConfirmarContrasenia().getText())) {
			ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblConfirmarContrasenia().setForeground(Color.BLACK);
			ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError().setVisible(false);
			ControladorCambioContrasenia.getInstance().getBandera()[1] = true;

		} else {
			ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblConfirmarContrasenia().setForeground(Color.RED);
			ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError().setVisible(true);
			ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError2().setText("Las contraseņa deben ser iguales");
			ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().getLblError2().setVisible(true);
			ControladorCambioContrasenia.getInstance().getBandera()[1] = false;
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
};
	private void ponerEnRojo() {
		
		if (!this.getBandera()[0]) {
			super.getVistaCambioContrasenia().getLblContrasenia().setForeground(Color.RED);
		}
		if (!this.getBandera()[1]) {
			super.getVistaCambioContrasenia().getLblConfirmarContrasenia().setForeground(Color.RED);
		}
		super.getVistaCambioContrasenia().getLblError().setVisible(true);
		super.getVistaCambioContrasenia().getLblError2().setText("Faltan completar campos");
		super.getVistaCambioContrasenia().getLblError2().setVisible(true);
	}
	private ActionListener listenerBtnCancelar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorCambioContrasenia.getInstance().getVistaCambioContrasenia().dispose();
			ControladorRecuperarContraseņa.getInstance().getVistaRecuperarContraseņa().setVisible(true);
		}
	};
	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVistaCambioContrasenia().getBtnGuardar())) {
			super.getVistaCambioContrasenia().getLblError().setVisible(false);
			super.getVistaCambioContrasenia().getLblError2().setVisible(false);
		}
		if (super.revisarbanderas(this.getBandera()) == 1) {
			
			this.getUser().setContrasenia(Hash.md5(super.getAgregarUsuario().getTxtContrasenia().getText()));
			
		/*	user.guardarUsuario(user);
			JOptionPane.showMessageDialog(getAgregarUsuario(), "Guardado correctamente");
			ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
			super.getAgregarUsuario().setVisible(false);*/
		} else {
			// System.out.println(i);
			ponerEnRojo();
		}
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	public VistaCambioContrasenia getVistaCambioContrasenia() {
		return vistaCambioContrasenia;
	}

	public void setVistaCambioContrasenia(VistaCambioContrasenia vistaCambioContrasenia) {
		this.vistaCambioContrasenia = vistaCambioContrasenia;
	}

	public Boolean[] getBandera() {
		return bandera;
	}

	public void setBandera(Boolean[] bandera) {
		this.bandera = bandera;
	}

	public FocusListener getListenerFocusTxtContrasennaCambio() {
		return listenerFocusTxtContrasennaCambio;
	}

	public void setListenerFocusTxtContrasennaCambio(FocusListener listenerFocusTxtContrasennaCambio) {
		this.listenerFocusTxtContrasennaCambio = listenerFocusTxtContrasennaCambio;
	}

	public FocusListener getListenerFocusTxtCambioContrasennaCambio() {
		return listenerFocusTxtCambioContrasennaCambio;
	}

	public void setListenerFocusTxtCambioContrasennaCambio(FocusListener listenerFocusTxtCambioContrasennaCambio) {
		this.listenerFocusTxtCambioContrasennaCambio = listenerFocusTxtCambioContrasennaCambio;
	}

	public KeyListener getKeyTxtContrasenna() {
		return keyTxtContrasenna;
	}

	public void setKeyTxtContrasenna(KeyListener keyTxtContrasenna) {
		this.keyTxtContrasenna = keyTxtContrasenna;
	}

	public KeyListener getKeyTxtConfirmatContrasenna() {
		return keyTxtConfirmatContrasenna;
	}

	public void setKeyTxtConfirmatContrasenna(KeyListener keyTxtConfirmatContrasenna) {
		this.keyTxtConfirmatContrasenna = keyTxtConfirmatContrasenna;
	}

	public ActionListener getActionBtnGuardar() {
		return actionBtnGuardar;
	}

	public void setActionBtnGuardar(ActionListener actionBtnGuardar) {
		this.actionBtnGuardar = actionBtnGuardar;
	}

	public ActionListener getListenerBtnCancelar() {
		return listenerBtnCancelar;
	}

	public void setListenerBtnCancelar(ActionListener listenerBtnCancelar) {
		this.listenerBtnCancelar = listenerBtnCancelar;
	}

	}