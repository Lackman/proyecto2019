package Controlador;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import Modelo.Hash;
import Modelo.Usuario;
import Vista.VistaModificarUsuario;

public class ControladorModificarUsuario extends ControladorPadre {
	private Usuario user;
	private Boolean[] bandera;
	private static ControladorModificarUsuario instance;
	private String contrasenia;
	private String respuesta;
	private Boolean modificarContrasenia, modificarRespuesta;

	public static ControladorModificarUsuario deleteInstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorModificarUsuario getInstance() {
		if (instance == null) {
			instance = new ControladorModificarUsuario();
		}
		return instance;
	}

	public ControladorModificarUsuario() {
		super.setVmu(new VistaModificarUsuario(this));
		super.getVmu().setVisible(true);
		user = new Usuario();
		creadorModelos();
		this.setBandera(new Boolean[12]);
		super.inicializadorDeBandera(this.getBandera());
		this.setModificarContrasenia(false);
		this.setModificarRespuesta(false);
		super.getVmu().getBtnCancelar().addActionListener(this.getListenerCancelar());

	}

	private ActionListener listenerCancelar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			String tipo = Usuario.getInstance().getTipo();
			getVmu().setVisible(false);
			if (tipo.equals("1")) {
				ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
			}
			if (tipo.equals("2")) {
				ControladorVistaSecretaria.deleteinstance().getVistaSecretaria().setVisible(true);
			}
		}
	};

	@SuppressWarnings("unchecked")
	private void creadorModelos() {
		String[] str = user.consulColumna("nombre_provincia", "provincia");
		str[0] = "Selecciona una provincia";
		super.getVmu().getCbxProvincia().setModel(new DefaultComboBoxModel<Object>(str));
		str = user.consulColumna("nombre_genero", "genero");
		str[0] = "Selecciona un genero";
		super.getVmu().getCbxGenero().setModel(new DefaultComboBoxModel<Object>(str));
		str = user.consulColumna("pregunta", "pregunta");
		str[0] = "Selecciona una pregunta";
		super.getVmu().getCbxPregunta().setModel(new DefaultComboBoxModel<Object>(str));
		str = user.consulColumna("nombre_localidad", "localidad");
		str[0] = "Selecciona una localidad";
		super.getVmu().getCbxLocalidad().setModel(new DefaultComboBoxModel<Object>(str));
		str = user.consulColumna("nombre_usuario", "tipo_usuario");
		str[0] = "Seleccione un tipo";
		super.getVmu().getCbxTipoUsuario().setModel(new DefaultComboBoxModel<Object>(str));

	}

	public void ConsisitirDatos() {
		if (super.getVmu().getTxtNombre().getText().isEmpty()) {
			super.getVmu().getLblNombre().setForeground(Color.RED);
			super.getVmu().getLblError().setVisible(true);
			this.getBandera()[0] = false;
		} else {
			this.getBandera()[0] = true;
		}
		if (super.getVmu().getTxtApellido().getText().isEmpty()) {
			super.getVmu().getLblApellido().setForeground(Color.RED);
			super.getVmu().getLblError().setVisible(true);
			this.getBandera()[1] = false;

		}
		{
			this.getBandera()[1] = true;
		}
		this.getBandera()[2] = true;
		if (super.getVmu().getTxtTelefono().getText().isEmpty()
				|| super.getVmu().getTxtTelefono().getText().length() < 7) {
			super.getVmu().getLblTelefono().setForeground(Color.RED);
			super.getVmu().getLblError().setVisible(true);
			this.getBandera()[3] = false;

		}
		{
			this.getBandera()[3] = true;
		}
		if (super.getVmu().getCbxProvincia().getSelectedIndex() == 0) {
			super.getVmu().getLblProvincia().setForeground(Color.RED);
			super.getVmu().getLblError().setVisible(true);
			super.getVmu().getCbxLocalidad().setEnabled(false);
			this.getBandera()[4] = false;
		}
		{
			this.getBandera()[4] = true;
		}
		if (super.getVmu().getTxtRespuesta().getText().isEmpty()) {
			this.setModificarRespuesta(false);
		} else {
			this.setModificarRespuesta(true);
		}
		this.getBandera()[11] = true;
		if (super.getVmu().getTxtContrasenia().getText().isEmpty()) {
			this.getBandera()[8] = true;
			this.getBandera()[9] = true;
			this.setModificarContrasenia(false);
		} else {
			this.setModificarContrasenia(true);
		}
		if (super.getVmu().getCbxTipoUsuario().getSelectedIndex() == 0) {
			super.getVmu().getLblTipoUsuario().setForeground(Color.RED);
			super.getVmu().getLblError().setVisible(true);
			this.getBandera()[5] = false;
		} else {
			super.getVmu().getLblTipoUsuario().setForeground(Color.BLACK);
			super.getVmu().getLblError().setVisible(false);
			this.getBandera()[5] = true;
		}
		if (super.getVmu().getCbxLocalidad().getSelectedIndex() == 0) {
			super.getVmu().getLblLocalidad().setForeground(Color.RED);
			super.getVmu().getLblError().setVisible(true);
			this.getBandera()[6] = false;
		} else {
			super.getVmu().getLblLocalidad().setForeground(Color.BLACK);
			super.getVmu().getLblError().setVisible(false);
			this.getBandera()[6] = true;
		}
		if (super.getVmu().getCbxGenero().getSelectedIndex() == 0) {
			super.getVmu().getLblGenero().setForeground(Color.RED);
			super.getVmu().getLblError().setVisible(true);
			this.getBandera()[7] = false;
		} else {
			super.getVmu().getLblGenero().setForeground(Color.BLACK);
			super.getVmu().getLblError().setVisible(false);
			this.getBandera()[7] = true;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void focusLost(FocusEvent arg0) {
		switch (arg0.getComponent().getName()) {
		case "txtNombre":
			if (super.getVmu().getTxtNombre().getText().isEmpty()) {
				super.getVmu().getLblNombre().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[0] = false;
			}
			break;
		case "txtApellido":
			if (super.getVmu().getTxtApellido().getText().isEmpty()) {
				super.getVmu().getLblApellido().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[1] = false;

			}
			break;
		case "txtDni":
			if (super.getVmu().getTxtDni().getText().isEmpty() || super.getVmu().getTxtDni().getText().length() < 8) {
				super.getVmu().getLblDni().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[2] = false;
			}
			break;
		case "txtTelefono":
			if (super.getVmu().getTxtTelefono().getText().isEmpty()
					|| super.getVmu().getTxtTelefono().getText().length() < 7) {
				super.getVmu().getLblTelefono().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[3] = false;

			}
			break;
		case "cbxProvincia":
			if (super.getVmu().getCbxProvincia().getSelectedIndex() == 0) {
				super.getVmu().getLblProvincia().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				super.getVmu().getCbxLocalidad().setEnabled(false);
				this.getBandera()[4] = false;
			}
			break;
		case "txtRespuesta":
			if (super.getVmu().getTxtRespuesta().getText().isEmpty()) {
				super.getVmu().getLblPreguntaSecreta().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[11] = false;
			}
			break;
		case "txtContrasenia":
			if (super.getVmu().getTxtContrasenia().getText().isEmpty()) {
				super.getVmu().getLblContrasenia().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[8] = false;
			}
			break;
		case "txtConfirmarContrasenia":
			if (super.getVmu().getTxtContrasenia().getText()
					.equals(super.getVmu().getTxtConfirmarContrasenia().getText())) {
				super.getVmu().getLblConfirmarContrasenia().setForeground(Color.BLACK);
				super.getVmu().getLblError().setVisible(false);
				this.getBandera()[9] = true;
			}
			break;
		case "cbxTipoUsuario":
			if (super.getVmu().getCbxTipoUsuario().getSelectedIndex() == 0) {
				super.getVmu().getLblTipoUsuario().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[5] = false;
			} else {
				super.getVmu().getLblTipoUsuario().setForeground(Color.BLACK);
				super.getVmu().getLblError().setVisible(false);
				this.getBandera()[5] = true;
			}
			break;
		case "cbxLocalidad":
			if (super.getVmu().getCbxLocalidad().getSelectedIndex() == 0) {
				super.getVmu().getLblLocalidad().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[6] = false;
			} else {
				super.getVmu().getLblLocalidad().setForeground(Color.BLACK);
				super.getVmu().getLblError().setVisible(false);
				this.getBandera()[6] = true;
			}
			break;
		case "cbxGenero":
			if (super.getVmu().getCbxGenero().getSelectedIndex() == 0) {
				super.getVmu().getLblGenero().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[7] = false;
			} else {
				super.getVmu().getLblGenero().setForeground(Color.BLACK);
				super.getVmu().getLblError().setVisible(false);
				this.getBandera()[7] = true;
			}
			break;
		default:
			break;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void itemStateChanged(ItemEvent arg0) {

		if (super.getVmu().getCbxProvincia().getSelectedIndex() == 0) {
			super.getVmu().getLblProvincia().setForeground(Color.RED);
			super.getVmu().getLblError().setVisible(true);
			super.getVmu().getCbxLocalidad().setEnabled(false);
		} else {
			if (super.getVmu().getCbxLocalidad().getSelectedIndex() == 0) {
				super.getVmu().getCbxLocalidad().setModel(new DefaultComboBoxModel(this.getUser()
						.consultaLocalidad(super.getVmu().getCbxProvincia().getSelectedItem().toString())));
				super.getVmu().getCbxLocalidad().setEnabled(true);
				super.getVmu().getLblProvincia().setForeground(Color.BLACK);
				super.getVmu().getLblError().setVisible(false);
				this.getBandera()[5] = true;
			}
			if (super.getVmu().getCbxPregunta().getSelectedIndex() == 0) {
				super.getVmu().getTxtRespuesta().setText("");
				super.getVmu().getTxtRespuesta().setEditable(false);
				super.getVmu().getLblPreguntaSecreta().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[10] = false;
			} else {

				super.getVmu().getTxtRespuesta().setEditable(true);
				super.getVmu().getLblPreguntaSecreta().setForeground(Color.BLACK);
				super.getVmu().getLblError().setVisible(false);
				this.getBandera()[10] = true;
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		switch (e.getComponent().getName()) {
		case "txtNombre":
			super.getVmu().getLblNombre().setForeground(Color.BLACK);
			super.getVmu().getLblError().setVisible(false);
			this.getBandera()[0] = true;
			break;
		case "txtApellido":
			super.getVmu().getLblApellido().setForeground(Color.BLACK);
			super.getVmu().getLblError().setVisible(false);
			this.getBandera()[1] = true;
			break;
		case "txtDni":
			if (!Character.isDigit(e.getKeyChar()) || (super.getVmu().getTxtDni().getText().length()) >= 8) {
				e.consume();
				Toolkit.getDefaultToolkit().beep();
			}
			if (super.getVmu().getTxtDni().getText().length() < 8) {
				Boolean marca = true;
				String[] dni = user.consulColumna("dni", "usuario");
				for (int i = 1; i < dni.length; i++) {
					// System.out.println(dni[i]);
					if (dni[i].equals(super.getVmu().getTxtDni().getText() + e.getKeyChar())) {
						marca = false;
						break;
					}
				}
				if (marca) {
					super.getVmu().getLblDni().setForeground(Color.BLACK);
					super.getVmu().getLblError().setVisible(false);
					this.getBandera()[2] = true;
				} else {
					super.getVmu().getLblDni().setForeground(Color.RED);
					super.getVmu().getLblError().setVisible(true);
					this.getBandera()[2] = false;
				}
			}
			break;
		case "txtTelefono":
			if (!Character.isDigit(e.getKeyChar()) || (super.getVmu().getTxtTelefono().getText().length()) >= 7) {
				e.consume();
				Toolkit.getDefaultToolkit().beep();
			}
			super.getVmu().getLblTelefono().setForeground(Color.BLACK);
			super.getVmu().getLblError().setVisible(false);
			this.getBandera()[4] = true;
			break;
		case "txtRespuesta":
			super.getVmu().getLblPreguntaSecreta().setForeground(Color.BLACK);
			super.getVmu().getLblError().setVisible(false);
			this.getBandera()[11] = true;
			break;
		case "txtContrasenia":
			super.getVmu().getLblContrasenia().setForeground(Color.BLACK);
			super.getVmu().getLblError().setVisible(false);
			this.getBandera()[8] = true;
			break;
		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getComponent().getName()) {
		case "txtContrasenia":
			if (super.getVmu().getTxtContrasenia().getText()
					.equals(super.getVmu().getTxtConfirmarContrasenia().getText())) {
				super.getVmu().getLblConfirmarContrasenia().setForeground(Color.BLACK);
				super.getVmu().getLblError().setVisible(false);
				this.getBandera()[8] = true;
			}
		case "txtConfirmarContrasenia":
			if (super.getVmu().getTxtContrasenia().getText()
					.equals(super.getVmu().getTxtConfirmarContrasenia().getText())) {
				super.getVmu().getLblConfirmarContrasenia().setForeground(Color.BLACK);
				super.getVmu().getLblError().setVisible(false);
				this.getBandera()[9] = true;

			} else {
				super.getVmu().getLblConfirmarContrasenia().setForeground(Color.RED);
				super.getVmu().getLblError().setVisible(true);
				this.getBandera()[9] = false;
			}
			break;
		default:
			break;
		}
	}

	private void ponerEnRojo() {
		if (!this.getBandera()[0]) {
			super.getVmu().getLblNombre().setForeground(Color.RED);
		}
		if (!this.getBandera()[1]) {
			super.getVmu().getLblApellido().setForeground(Color.RED);
		}
		if (!this.getBandera()[2]) {
			super.getVmu().getLblDni().setForeground(Color.RED);
		}
		if (!this.getBandera()[3]) {
			super.getVmu().getLblTipoUsuario().setForeground(Color.RED);
		}
		if (!this.getBandera()[4]) {
			super.getVmu().getLblTelefono().setForeground(Color.RED);
		}
		if (!this.getBandera()[5]) {
			super.getVmu().getLblProvincia().setForeground(Color.RED);
		}
		if (!this.getBandera()[6]) {
			super.getVmu().getLblLocalidad().setForeground(Color.RED);
		}
		if (!this.getBandera()[7]) {
			super.getVmu().getLblGenero().setForeground(Color.RED);
		}
		if (!this.getBandera()[8]) {
			super.getVmu().getLblContrasenia().setForeground(Color.RED);
		}
		if (!this.getBandera()[9]) {
			super.getVmu().getLblConfirmarContrasenia().setForeground(Color.RED);
		}
		if (!this.getBandera()[10]) {
			super.getVmu().getLblPreguntaSecreta().setForeground(Color.RED);
		}
		if (!this.getBandera()[11]) {
			super.getVmu().getLblPreguntaSecreta().setForeground(Color.RED);
		}
		super.getVmu().getLblError().setVisible(true);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVmu().getBtnGuardar())) {
			this.ConsisitirDatos();
			super.getVmu().getLblError().setVisible(false);

			if (super.revisarbanderas(this.getBandera()) == 12) {
				this.getUser().setNombre(super.getVmu().getTxtNombre().getText());

				this.getUser().setApellido(super.getVmu().getTxtApellido().getText());

				this.getUser().setDNI(Integer.parseInt((super.getVmu().getTxtDni().getText())));

				this.getUser().setTipo(super.getVmu().getCbxTipoUsuario().getSelectedItem().toString());

				this.getUser().setTelefono(super.getVmu().getTxtTelefono().getText());

				this.getUser().setLocalidad(super.getVmu().getCbxLocalidad().getSelectedItem().toString());

				this.getUser().setGenero(super.getVmu().getCbxGenero().getSelectedItem().toString());

				this.getUser().setContrasenia(Hash.md5(super.getVmu().getTxtContrasenia().getText()));

				this.getUser().setRespuesta(Hash.md5(super.getVmu().getTxtRespuesta().getText()));

				this.getUser().setPregunta(super.getVmu().getCbxPregunta().getSelectedItem().toString());
				user.ModificarUsuario(user, this.getModificarContrasenia(), this.getModificarRespuesta());
				JOptionPane.showMessageDialog(getVmu(), "Guardado correctamente");
				super.getVmu().setVisible(true);
				super.getVmu().dispose();
				String tipo = Usuario.getInstance().getTipo();
				if (tipo.equals("1")) {
					ControladorVistaAdmin.getInstance().actualizarTabla();
					ControladorVistaAdmin.getInstance().getVistaAdmin().setVisible(true);

				}
				if (tipo.equals("2")) {
					ControladorVistaSecretaria.getInstance().getVistaSecretaria().setVisible(true);
				}
			} else {
				// System.out.println(i);
				ponerEnRojo();
			}
		} else {
			super.getVmu().dispose();
			String tipo = Usuario.getInstance().getTipo();
			if (tipo.equals("1")) {
				ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
			}
			if (tipo.equals("2")) {
				ControladorVistaSecretaria.deleteinstance().getVistaSecretaria().setVisible(true);
			}

		}
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	public Boolean[] getBandera() {
		return bandera;
	}

	public void setBandera(Boolean[] bandera) {
		this.bandera = bandera;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public Boolean getModificarContrasenia() {
		return modificarContrasenia;
	}

	public void setModificarContrasenia(Boolean modificarContrasenia) {
		this.modificarContrasenia = modificarContrasenia;
	}

	public Boolean getModificarRespuesta() {
		return modificarRespuesta;
	}

	public void setModificarRespuesta(Boolean modificarRespuesta) {
		this.modificarRespuesta = modificarRespuesta;
	}

	public ActionListener getListenerCancelar() {
		return listenerCancelar;
	}

	public void setListenerCancelar(ActionListener listenerCancelar) {
		this.listenerCancelar = listenerCancelar;
	}

}
