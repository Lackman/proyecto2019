package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Modelo.Usuario;
import Vista.VentanaProfesor;
import Vista.VistaNotas;

public class ControladorNotas extends ControladorPadre {
	private String nota;
	private Integer dni;
	private static ControladorNotas instance;

	public static ControladorNotas deleteInstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorNotas getInstance() {
		if (instance == null) {
			instance = new ControladorNotas();
		}
		return instance;
	}

	public ControladorNotas() {
		this.setVistaNotas(new VistaNotas());
		this.getVistaNotas().getBtnAceptar().addActionListener(this.getBtnBotonAceptar());
		this.getVistaNotas().getBtnCancelar().addActionListener(this.getBtnBotonCancelar());
	}

	private ActionListener btnBotonAceptar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			Usuario.getInstance().NotaUsuario(getVistaNotas().getTextCalificación().getText(), getDni().toString(), getVistaNotas().getTextCurso().getText());
			//ControladorNotas.getInstance().getVistaNotas().getTextCalificación().getText().toString();
			getVistaNotas().setVisible(false);
			ControladorVentanaProfesor.getInstance().getVentanaProfesor().setVisible(true);
			ControladorVentanaProfesor.getInstance().getVentanaProfesor().getComboBox().setSelectedIndex(0);
		}
	};
	
	private ActionListener btnBotonCancelar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			getVistaNotas().setVisible(false);
			ControladorVentanaProfesor.getInstance().getVentanaProfesor().setVisible(true);	
		}
	};

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public ActionListener getBtnBotonAceptar() {
		return btnBotonAceptar;
	}

	public void setBtnBotonAceptar(ActionListener btnBotonAceptar) {
		this.btnBotonAceptar = btnBotonAceptar;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public ActionListener getBtnBotonCancelar() {
		return btnBotonCancelar;
	}

	public void setBtnBotonCancelar(ActionListener btnBotonCancelar) {
		this.btnBotonCancelar = btnBotonCancelar;
	}

}
