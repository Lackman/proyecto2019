package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Vista.AgregarCurso;
import Vista.AgregarProfesor;
import Vista.AgregarUsuario;
import Vista.Login;
import Vista.VentanaProfesor;
import Vista.VistaAdmin;
import Vista.VistaCambioContrasenia;
import Vista.VistaHorario;
import Vista.VistaModificarUsuario;
import Vista.VistaNotas;
import Vista.VistaRecuperarContraseņa;
import Vista.VistaSecretaria;

public class ControladorPadre implements FocusListener, KeyListener, ItemListener, ActionListener {
	private static ControladorPadre instance;
	private VistaHorario vistaHorario;
	private AgregarCurso agregarCurso;
	private AgregarUsuario agregarUsuario;
	private AgregarProfesor vistaAgregarProfesor;
	private VistaModificarUsuario vmu;
	private Login login;
	private VistaAdmin vistaAdmin;
	private VistaSecretaria vistaSecretaria;
	private VentanaProfesor ventanaProfesor;
	private DefaultTableModel tablaModelo;
	private VistaRecuperarContraseņa vistaRecuperarContrasenia;
	private VistaCambioContrasenia vistaCambioContrasenia;
	private VistaNotas vistaNotas;
	public void mostrarCartel(String mensaje) {
		JOptionPane.showMessageDialog(null, mensaje + ".");
	}

	public ControladorPadre() {
		this.setTablaModelo(null);
	}

	public static ControladorPadre deleteInstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorPadre getInstance() {
		if (instance == null) {
			instance = new ControladorPadre();
		}
		return instance;
	}

	public Integer revisarbanderas(Boolean[] bandera) {
		Integer i = 0;
		for (Boolean boolean1 : bandera) {
			if (!boolean1) {
				break;
			}
			i++;
		}
		return i;
	}

	public void inicializadorDeBandera(Boolean[] bandera) {
		for (int i = 0; i < bandera.length; i++) {
			bandera[i] = false;
		}
	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

	public AgregarCurso getAgregarCurso() {
		return agregarCurso;
	}

	public void setAgregarCurso(AgregarCurso agregarCurso) {
		this.agregarCurso = agregarCurso;
	}

	public AgregarUsuario getAgregarUsuario() {
		return agregarUsuario;
	}

	public void setAgregarUsuario(AgregarUsuario agregarUsuario) {
		this.agregarUsuario = agregarUsuario;
	}

	public VistaHorario getVistaHorario() {
		return vistaHorario;
	}

	public void setVistaHorario(VistaHorario vistaHorario) {
		this.vistaHorario = vistaHorario;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public VistaAdmin getVistaAdmin() {
		return vistaAdmin;
	}

	public void setVistaAdmin(VistaAdmin vistaAdmin) {
		this.vistaAdmin = vistaAdmin;
	}

	public DefaultTableModel getTablaModelo() {
		return tablaModelo;
	}

	public void setTablaModelo(DefaultTableModel tablaModelo) {
		this.tablaModelo = tablaModelo;
	}

	public AgregarProfesor getVistaAgregarProfesor() {
		return vistaAgregarProfesor;
	}

	public void setVistaAgregarProfesor(AgregarProfesor vistaAgregarProfesor) {
		this.vistaAgregarProfesor = vistaAgregarProfesor;
	}
	public VistaSecretaria getVistaSecretaria() {
		return vistaSecretaria;
	}

	public void setVistaSecretaria(VistaSecretaria vistaSecretaria) {
		this.vistaSecretaria = vistaSecretaria;
	}

	public VistaModificarUsuario getVmu() {
		return vmu;
	}

	public void setVmu(VistaModificarUsuario vmu) {
		this.vmu = vmu;
	}

	public VentanaProfesor getVentanaProfesor() {
		return ventanaProfesor;
	}

	public void setVentanaProfesor(VentanaProfesor ventanaProfesor) {
		this.ventanaProfesor = ventanaProfesor;
	}

	public VistaRecuperarContraseņa getVistaRecuperarContrasenia() {
		return vistaRecuperarContrasenia;
	}

	public void setVistaRecuperarContrasenia(VistaRecuperarContraseņa vistaRecuperarContrasenia) {
		this.vistaRecuperarContrasenia = vistaRecuperarContrasenia;
	}

	public VistaCambioContrasenia getVistaCambioContrasenia() {
		return vistaCambioContrasenia;
	}

	public void setVistaCambioContrasenia(VistaCambioContrasenia vistaCambioContrasenia) {
		this.vistaCambioContrasenia = vistaCambioContrasenia;
	}

	public VistaNotas getVistaNotas() {
		return vistaNotas;
	}

	public void setVistaNotas(VistaNotas vistaNotas) {
		this.vistaNotas = vistaNotas;
	}
	
	
}
