package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import Modelo.BaseDeDatos;
import Modelo.Hash;
import Modelo.Txt;
import Modelo.Usuario;
import Vista.Login;

public class ControladorLogin extends ControladorPadre {
	private static ControladorLogin instance;

	public ControladorLogin() {
		super.setLogin(new Login(this));
		super.getLogin().setVisible(true);
		super.getLogin().getTxtUsuario().setText("38299867");
		super.getLogin().getTxtContrasenia().setText("1234");
	}

	public static ControladorLogin getInstance() {
		if (instance == null) {
			instance = new ControladorLogin();
		}
		return instance;
	}

	public static ControladorLogin deleteInstance() {
		instance = null;
		return getInstance();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		switch (e.getComponent().getName()) {
		case "txtUsuario":
			if (!Character.isDigit(e.getKeyChar()) || (super.getLogin().getTxtUsuario().getText().length()) >= 8) {
				e.consume();
			}
			break;

		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {
		String tipo = "";
		if (e.getSource().equals(super.getLogin().getBtnRecuperarContraseña())) {
			super.getLogin().dispose();
			ControladorRecuperarContraseña.deleteInstance().getVistaRecuperarContrasenia().setVisible(true);

			// System.out.println("Te toco " +
			// e.getSource().equals(ControladorRecuperarContraseña.getInstance().getVistaRecuperarContraseña().
			// getBtnCancelar()));
		} else {
			try {
				Txt.ConsistirCampos(this.getLogin().getCampos());
				if (Usuario.getInstance().iniciarSesion(super.getLogin().getTxtUsuario().getText(),
						Hash.md5(super.getLogin().getTxtContrasenia().getText()))) {
					tipo = Usuario.getInstance().comprobar(super.getLogin().getTxtUsuario().getText());
					// System.out.println("El tipo de usuario es " + tipo);
					if (tipo.equals("1")) {
						Usuario.getInstance().setTipo(tipo);
						ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
						this.getLogin().setVisible(false);
					}
					if (tipo.equals("2")) {
						Usuario.getInstance().setTipo(tipo);
						ControladorVistaSecretaria.deleteInstance().getVistaSecretaria().setVisible(true);
						this.getLogin().setVisible(false);
					}
					if (tipo.equals("4")) {
						Usuario.getInstance().setTipo(tipo);
						ControladorVentanaProfesor.deleteinstance().getVentanaProfesor().setVisible(true);

					}
					super.getLogin().dispose();
					/* Hacer los caminos para los diferentes usuarios */

				} else {
					super.getLogin().getLblContrseaIncorrecta().setVisible(true);
				}
			} catch (SQLException e1) {
				super.mostrarCartel("Error al iniciar sesión");
			}

		}
	}

}
