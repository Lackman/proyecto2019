package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.time.LocalTime;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import Modelo.Curso;
import Modelo.Horario;
import Modelo.Txt;
import Modelo.Usuario;
import Vista.VistaHorario;

public class ControladorHorario extends ControladorPadre {
	private Curso curso;
	private static ControladorHorario instance;
	private Integer indexTabla = null;
	
	
	private ListSelectionListener listenerSeleccionarTabla = new ListSelectionListener() {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			try {
				setIndexTabla(getVistaHorario().getTableHorario().getSelectedRow());
			} catch (ArrayIndexOutOfBoundsException e2) {
			}
		}
	};
	private ActionListener listenerBtnAgregar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (Txt.ConsistirCampos(getVistaHorario().getCampos())) {
				String diaSemana = getVistaHorario().getCbxDiaSemana().getSelectedItem().toString();
				String lugar = getVistaHorario().getTxtLugar().getText();
				LocalTime horaInicio = LocalTime.of(
						Integer.parseInt(
								getVistaHorario().getSpHoraInicio().getValue().toString().split(" ")[3].split(":")[0]),
						Integer.parseInt(
								getVistaHorario().getSpHoraInicio().getValue().toString().split(" ")[3].split(":")[1]));
				LocalTime horaFinal = LocalTime.of(
						Integer.parseInt(
								getVistaHorario().getSpHoraFin().getValue().toString().split(" ")[3].split(":")[0]),
						Integer.parseInt(
								getVistaHorario().getSpHoraFin().getValue().toString().split(" ")[3].split(":")[1]));
				DefaultTableModel modelo = (DefaultTableModel) getVistaHorario().getTableHorario().getModel();
				Object[] row = { diaSemana, horaInicio, horaFinal, lugar };
				modelo.addRow(row);
				getVistaHorario().getTableHorario().setModel(modelo);
				getCurso().getHorario().getLugarDeLaActividad().add(lugar);
				getCurso().getHorario().getDiaDeSemana().add(diaSemana);
				getCurso().getHorario().getHoraInicio().add(horaInicio);
				getCurso().getHorario().getHoraFinal().add(horaFinal);
			} else {
				mostrarCartel("Faltan datos");
			}
		}
	};

	private ActionListener listenerBtnQuitar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (getIndexTabla() != null) {
				getCurso().getHorario().getLugarDeLaActividad().remove(Integer.parseInt(getIndexTabla().toString()));
				getCurso().getHorario().getDiaDeSemana().remove(Integer.parseInt(getIndexTabla().toString()));
				getCurso().getHorario().getHoraInicio().remove(Integer.parseInt(getIndexTabla().toString()));
				getCurso().getHorario().getHoraFinal().remove(Integer.parseInt(getIndexTabla().toString()));
				getVistaHorario().getTableHorario().setModel(GenerarModelo());
				setIndexTabla(null);
			}
		}
	};
private ActionListener listenerBtnGuardar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (getVistaHorario().getTableHorario().getRowCount() == 0) {
				JOptionPane.showMessageDialog(null, "Ingrese al menos un horario");
			}else {
				getVistaHorario().dispose();
				ControladorAgregarCurso.getInstance().getAgregarCurso().setVisible(true);
				
			}
		}
	};
private ActionListener listenerBtnCancelar = new ActionListener() {
	
	@Override
	public void actionPerformed(ActionEvent e) {
		getVistaHorario().setVisible(false);
		ControladorAgregarCurso.getInstance().getAgregarCurso().setVisible(true);
		
	}
};
	public ControladorHorario(Curso curso) {
		this.setVistaHorario(new VistaHorario(this));
		this.getVistaHorario().getTableHorario().getSelectionModel()
				.addListSelectionListener(this.getListenerSeleccionarTabla());
		this.getVistaHorario().getBtnQuitar().addActionListener(this.getListenerBtnQuitar());
		this.getVistaHorario().getBtnGuardar().addActionListener(getListenerBtnGuardar());
		this.getVistaHorario().getBtnCancelar().addActionListener(getListenerBtnCancelar());
		this.setCurso(curso);
		this.CargarHorarios();
		this.getVistaHorario().getBtnAgregar().addActionListener(this.getListenerBtnAgregar());
		if (ControladorPadre.getInstance().getTablaModelo() != null) {
			this.getVistaHorario().getTableHorario().setModel(ControladorPadre.getInstance().getTablaModelo());
		} else {
			this.setTablaModelo(new DefaultTableModel(0, 0));
		}
	}

	public static ControladorHorario deleteInstance(Curso curso) {
		instance = null;
		return getInstance(curso);
	}

	public static ControladorHorario getInstance(Curso curso) {
		if (instance == null) {
			instance = new ControladorHorario(curso);
		}
		return instance;
	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		itemStateChanged(arg0);
	}

	public DefaultTableModel GenerarModelo() {
		String[] titulos = new String[] { "D\u00EDa", "Hora de Inicio", "Horario de Finalizaci\u00F3n", "Lugar" };
		DefaultTableModel modelo = new DefaultTableModel() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}
		};
		modelo.setColumnIdentifiers(titulos);
		if (this.getCurso().getHorario().getHoraFinal().size() == 0) {
			this.setTablaModelo(new DefaultTableModel(0, 0));
		} else {

			for (int i = 0; i < this.getCurso().getHorario().getHoraFinal().size(); i++) {
				Object[] row = { this.getCurso().getHorario().getDiaDeSemana().get(i),
						this.getCurso().getHorario().getHoraInicio().get(i),
						this.getCurso().getHorario().getHoraFinal().get(i),
						this.getCurso().getHorario().getLugarDeLaActividad().get(i) };
				modelo.addRow(row);
			}
		}
		return modelo;
	}

	public void CargarHorarios() {
		if (this.getCurso().getHorario() != null) {
			this.getVistaHorario().getTableHorario().setModel(GenerarModelo());
		} else {
			this.getCurso().setHorario(new Horario());
		}
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public ActionListener getListenerBtnAgregar() {
		return listenerBtnAgregar;
	}

	public void setListenerBtnAgregar(ActionListener listenerBtnAgregar) {
		this.listenerBtnAgregar = listenerBtnAgregar;
	}

	public ListSelectionListener getListenerSeleccionarTabla() {
		return listenerSeleccionarTabla;
	}

	public void setListenerSeleccionarTabla(ListSelectionListener listenerSeleccionarTabla) {
		this.listenerSeleccionarTabla = listenerSeleccionarTabla;
	}

	public ActionListener getListenerBtnQuitar() {
		return listenerBtnQuitar;
	}

	public Integer getIndexTabla() {
		return indexTabla;
	}

	public void setIndexTabla(Integer indexTabla) {
		this.indexTabla = indexTabla;
	}

	public void setListenerBtnQuitar(ActionListener listenerBtnQuitar) {
		this.listenerBtnQuitar = listenerBtnQuitar;
	}

	public ActionListener getListenerBtnGuardar() {
		return listenerBtnGuardar;
	}

	public void setListenerBtnGuardar(ActionListener listenerBtnGuardar) {
		this.listenerBtnGuardar = listenerBtnGuardar;
	}

	public ActionListener getListenerBtnCancelar() {
		return listenerBtnCancelar;
	}

	public void setListenerBtnCancelar(ActionListener listenerBtnCancelar) {
		this.listenerBtnCancelar = listenerBtnCancelar;
	}
}
