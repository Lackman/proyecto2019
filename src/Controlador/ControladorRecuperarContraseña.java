package Controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import Modelo.BaseDeDatos;
import Modelo.Hash;
import Modelo.Usuario;
import Vista.VistaCambioContrasenia;
import Vista.VistaRecuperarContraseņa;
import Controlador.ControladorLogin;

public class ControladorRecuperarContraseņa extends ControladorPadre{
	
	private static ControladorRecuperarContraseņa instance;
	private Usuario user;
	private VistaRecuperarContraseņa VistaRecuperarContraseņa;
	private VistaCambioContrasenia vistaCambioContrasenia;
	
	public static ControladorRecuperarContraseņa deleteInstance() {
		instance = null;
		return getInstance();
	}
	
	public static ControladorRecuperarContraseņa getInstance() {
		if (instance == null) {
			instance = new ControladorRecuperarContraseņa();
		}
		return instance;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}
	public ControladorRecuperarContraseņa() {
		this.setVistaRecuperarContraseņa(new VistaRecuperarContraseņa(this));
		this.getVistaRecuperarContraseņa().setVisible(true);
		user = new Usuario();
		this.getVistaRecuperarContraseņa().getCbxPregunta().setModel(creadorModelos());
		 //explota aca
		//System.out.println("Hola " + ControladorLogin.getInstance().getLogin().getTxtUsuario().getText());
		Integer x = Integer.valueOf(ControladorLogin.getInstance().getLogin().getTxtUsuario().getText());
		ObtenerDatosUsuario(x);
	}
	@SuppressWarnings("unchecked")
	public DefaultComboBoxModel<Object> creadorModelos() {
		String[] str = user.consulColumna("pregunta", "pregunta");
		str[0] = "Selecciona una pregunta";
		return new DefaultComboBoxModel<Object>(str);
		
	}
	public Usuario ObtenerDatosUsuario(Integer IdUsuario) {
		Usuario usuario = new Usuario();
		String consulta = "select dni, pregunta from usuario inner join pregunta on pregunta.id_pregunta = usuario.id_pregunta where dni = " + IdUsuario;
		try {
			ResultSet rs = BaseDeDatos.getInstance().consultar(consulta);
			while (rs.next()) {
				usuario.setPregunta(rs.getObject(2).toString());
				//String preg = rs.getString(2).toString();
				//ControladorRecuperarContraseņa.getInstance().getVistaRecuperarContraseņa().setCbxPregunta(preg);;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuario;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void itemStateChanged(ItemEvent arg0) {
		if (this.getVistaRecuperarContraseņa().getCbxPregunta().getSelectedIndex() == 0) {
			this.getVistaRecuperarContraseņa().getTxtRespuesta().setText("");
			this.getVistaRecuperarContraseņa().getTxtRespuesta().setEditable(false);
		} else {

			this.getVistaRecuperarContraseņa().getTxtRespuesta().setEditable(true);
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(getVistaRecuperarContraseņa().getBtnCancelar())) {
			getVistaRecuperarContraseņa().dispose();
			ControladorLogin.getInstance().getLogin().setVisible(true);
		}
		if (e.getSource().equals(getVistaRecuperarContraseņa().getBtnAceptar())) {
			try {
				if (Usuario.getInstance().chequearRespuesta(ControladorLogin.getInstance().getLogin().getTxtUsuario().getText(), Hash.md5(getVistaRecuperarContraseņa().getTxtRespuesta().getText()))) {
					ControladorCambioContrasenia.deleteInstance().getVistaCambioContrasenia().setVisible(true);
					getVistaRecuperarContraseņa().dispose();
					//aca
				}else {
					JOptionPane.showMessageDialog(getVistaRecuperarContraseņa(), "Respuesta incorrecta!");
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
	}
	public void keyTyped(KeyEvent e) {
		switch (e.getComponent().getName()) {
		case "txtUsuario":
			if (!Character.isDigit(e.getKeyChar()) || (super.getLogin().getTxtUsuario().getText().length()) >= 8) {
				e.consume();
			}
			break;

		default:
			break;
		}
	}

	public VistaRecuperarContraseņa getVistaRecuperarContraseņa() {
		return VistaRecuperarContraseņa;
	}

	public void setVistaRecuperarContraseņa(VistaRecuperarContraseņa vistaRecuperarContraseņa) {
		VistaRecuperarContraseņa = vistaRecuperarContraseņa;
	}

	public VistaCambioContrasenia getVistaCambioContrasenia() {
		return vistaCambioContrasenia;
	}

	public void setVistaCambioContrasenia(VistaCambioContrasenia vistaCambioContrasenia) {
		this.vistaCambioContrasenia = vistaCambioContrasenia;
	}
	
	
}
