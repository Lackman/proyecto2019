package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import Modelo.Hash;
import Modelo.Txt;
import Modelo.Usuario;
import Vista.AgregarUsuario;

public class ControladorAgregarUsuario extends ControladorPadre {
	private static ControladorAgregarUsuario instance;

	private ActionListener listenerGuardar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Boolean m = true;
			for (Txt txt : getAgregarUsuario().getCampos()) {
				if (!txt.ConsisitirCampo()) {
					m = false;
					break;
				}
			}

			if (m) {
				Usuario u = new Usuario();
				u.setDNI(Integer.parseInt(getAgregarUsuario().getTxtDni().getText()));
				u.setNombre(getAgregarUsuario().getTxtNombre().getText());
				u.setApellido(getAgregarUsuario().getTxtApellido().getText());
				u.setTipo(getAgregarUsuario().getCbxTipoUsuario().getSelectedItem().toString());
				u.setTelefono(getAgregarUsuario().getTxtTelefono().getText());
				u.setProvincia(getAgregarUsuario().getCbxProvincia().getSelectedItem().toString());
				u.setLocalidad(getAgregarUsuario().getCbxLocalidad().getSelectedItem().toString());
				u.setGenero(getAgregarUsuario().getCbxGenero().getSelectedItem().toString());
				u.setContrasenia(Hash.md5(getAgregarUsuario().getTxtContrasenia().getText()));
				u.setPregunta(getAgregarUsuario().getCbxPregunta().getSelectedItem().toString());
				u.setRespuesta(getAgregarUsuario().getTxtRespuesta().getText());
				Usuario.getInstance().guardarUsuario(u);
				String tipo = Usuario.getInstance().getTipo();
				getAgregarUsuario().setVisible(false);
				ControladorAgregarUsuario.getInstance().getAgregarUsuario().setVisible(false);
				ControladorVistaAdmin.getInstance().actualizarTabla();
				//ControladorVistaSecretaria.getInstance().actualizarTabla();
				if (tipo.equals("1")) {
					ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
				}
				if (tipo.equals("2")) {
					ControladorVistaSecretaria.deleteinstance().getVistaSecretaria().setVisible(true);
				}
				//ControladorVistaAdmin.getInstance().getVistaAdmin().setVisible(true);
			}
		}
	};
	private ActionListener listenerCancelar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			String tipo = Usuario.getInstance().getTipo();
			getAgregarUsuario().setVisible(false);
			if (tipo.equals("1")) {
				ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
			}
			if (tipo.equals("2")) {
				ControladorVistaSecretaria.deleteinstance().getVistaSecretaria().setVisible(true);
			}
			
		}
	};
	public static ControladorAgregarUsuario deleteInstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorAgregarUsuario getInstance() {
		if (instance == null) {
			instance = new ControladorAgregarUsuario();
		}
		return instance;
	}

	public ControladorAgregarUsuario() {
		this.setAgregarUsuario(new AgregarUsuario(this));
		this.getAgregarUsuario().setVisible(true);
		this.getAgregarUsuario().getBtnGuardar().addActionListener(this.getListenerGuardar());
		this.getAgregarUsuario().getBtnCancelar().addActionListener(this.getListenerCancelar());
		creadorModelos();
	}

	@SuppressWarnings("unchecked")
	private void creadorModelos() {
		String[] str = Usuario.getInstance().consulColumna("nombre_provincia", "provincia");
		str[0] = "Selecciona una provincia";
		this.getAgregarUsuario().getTxtProvincia().SetModelo(str);
		str = Usuario.getInstance().consulColumna("nombre_genero", "genero");
		str[0] = "Selecciona un genero";
		this.getAgregarUsuario().getTbxGenero().SetModelo(str);
		str = Usuario.getInstance().consulColumna("pregunta", "pregunta");
		str[0] = "Selecciona una pregunta";
		this.getAgregarUsuario().getTbxPregunta().SetModelo(str);
		str = Usuario.getInstance().consulColumna("nombre_localidad", "localidad");
		str[0] = "Selecciona una localidad";
		this.getAgregarUsuario().getTbxLocalidad().SetModelo(str);
		str = Usuario.getInstance().consulColumna("nombre_usuario", "tipo_usuario");
		str[0] = "Seleccione un tipo";
		this.getAgregarUsuario().getTxtTipoUsuario().SetModelo(str);
	}

	public ActionListener getListenerGuardar() {
		return listenerGuardar;
	}

	public void setListenerGuardar(ActionListener listenerGuardar) {
		this.listenerGuardar = listenerGuardar;
	}
	public ActionListener getListenerCancelar() {
		return listenerCancelar;
	}

	public void setListenerCancelar(ActionListener listenerCancelar) {
		this.listenerCancelar = listenerCancelar;
	}

}
