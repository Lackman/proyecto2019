package Controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import Modelo.Curso;
import Modelo.Hash;
import Modelo.Horario;
import Modelo.Txt;
import Modelo.Usuario;
import Vista.AgregarCurso;
import Vista.AgregarProfesor;

public class ControladorAgregarCurso extends ControladorPadre {
	private Curso curso;
	private ArrayList<String[]> profesores;
	private Boolean[] bandera; // Orden de las banderas: Nombre = 0, Inicio = 1, Finalizacion = 2, Profesores =
								// 3, Limite de cupo = 4, Horarios = 5
	private Horario horario;

	private ActionListener lisenerBtnGuardar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (Txt.ConsistirCampos(getAgregarCurso().getCampos())) {
				Curso c = new Curso();
				c.setNombre(getAgregarCurso().getTxtNombre().getText());
				c.setLimiteDeCupos(Integer.valueOf(getAgregarCurso().getSpLimiteDeCupos().getText()));
				c.setInicio(getAgregarCurso().getDateInicio().getDate());
				c.setFinalizacion(getAgregarCurso().getDateFinalizacion().getDate());
				c.crearCurso(c);
				for (String boolean1 : getHorario().getLugarDeLaActividad()) {
					System.out.println("asd" + boolean1);
				}
				Integer idCurso = curso.crearCurso(curso);
				for (String[] dni : getProfesores()) {
					curso.AgregarCursoProfesor(idCurso, dni[2]);
				}
				curso.AgregarCursoHorario(idCurso, curso);
				JOptionPane.showMessageDialog(getAgregarCurso(), "Curso agregado correctamente");
				//ControladorAgregarCurso.getInstance().getAgregarUsuario().setVisible(false);
				ControladorVistaAdmin.getInstance().actualizarTabla();
				String tipo = Usuario.getInstance().getTipo();
				if (tipo.equals("1")) {
					ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
					ControladorAgregarCurso.getInstance().getAgregarCurso().setVisible(false);
				}
				if (tipo.equals("2")) {
					ControladorVistaAdmin.getInstance().getVistaAdmin().setVisible(false);
					ControladorVistaSecretaria.deleteinstance().getVistaSecretaria().setVisible(true);
				}
				}
		}
	};
	private ActionListener lisenerBtnProfesores = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorProfesores.deleteInstance().getVistaAgregarProfesor().setVisible(true);
		}
	};
	private ActionListener lisenerBtnCacnelar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			String tipo = Usuario.getInstance().getTipo();
			getAgregarCurso().setVisible(false);
			if (tipo.equals("1")) {
				ControladorVistaAdmin.deleteinstance().getVistaAdmin().setVisible(true);
			}
			if (tipo.equals("2")) {
				ControladorVistaSecretaria.deleteinstance().getVistaSecretaria().setVisible(true);
			}
		}
	};
	private ActionListener lisenerBtnHorario = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorHorario.deleteInstance(getCurso()).getVistaHorario().setVisible(true);
		}
	};
	private static ControladorAgregarCurso instance;

	public static ControladorAgregarCurso deleteInstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorAgregarCurso getInstance() {
		if (instance == null) {
			instance = new ControladorAgregarCurso();
		}
		return instance;
	}

	public ControladorAgregarCurso() {
		super.setAgregarCurso(new AgregarCurso(this));
		this.setCurso(new Curso());
		this.setProfesores(null);
		this.getCurso().setHorario(null);	
		this.getAgregarCurso().getBtnCancelar().addActionListener(this.getLisenerBtnCacnelar());
		this.getAgregarCurso().getBtnGuardar().addActionListener(this.getLisenerBtnGuardar());
		this.getAgregarCurso().getBtnHorarios().addActionListener(this.getLisenerBtnHorario());
		this.getAgregarCurso().getBtnProfesores().addActionListener(this.getLisenerBtnProfesores());
		this.setHorario(new Horario());
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(super.getAgregarCurso().getBtnHorarios())) {
			ControladorHorario.deleteInstance(this.getCurso()).getVistaHorario().setVisible(true);
		}
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Boolean[] getBandera() {
		return bandera;
	}

	public void setBandera(Boolean[] bandera) {
		this.bandera = bandera;
	}

	public ActionListener getLisenerBtnGuardar() {
		return lisenerBtnGuardar;
	}

	public void setLisenerBtnGuardar(ActionListener lisenerBtnGuardar) {
		this.lisenerBtnGuardar = lisenerBtnGuardar;
	}

	public ActionListener getLisenerBtnProfesores() {
		return lisenerBtnProfesores;
	}

	public void setLisenerBtnProfesores(ActionListener lisenerBtnProfesores) {
		this.lisenerBtnProfesores = lisenerBtnProfesores;
	}

	public ArrayList<String[]> getProfesores() {
		return profesores;
	}

	public void setProfesores(ArrayList<String[]> profesores) {
		this.profesores = profesores;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public ActionListener getLisenerBtnCacnelar() {
		return lisenerBtnCacnelar;
	}

	public ActionListener getLisenerBtnHorario() {
		return lisenerBtnHorario;
	}

	public void setLisenerBtnCacnelar(ActionListener lisenerBtnCacnelar) {
		this.lisenerBtnCacnelar = lisenerBtnCacnelar;
	}

	public void setLisenerBtnHorario(ActionListener lisenerBtnHorario) {
		this.lisenerBtnHorario = lisenerBtnHorario;
	}

}
