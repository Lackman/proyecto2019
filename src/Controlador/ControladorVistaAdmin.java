
package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Modelo.Curso;
import Modelo.Reporte;
import Modelo.Usuario;
import Vista.VistaAdmin;

public class ControladorVistaAdmin extends ControladorPadre {
	private ActionListener usuarioAgregar;
	private ActionListener cursoAgregar;
	private ActionListener usuarioDesactivar;
	private ActionListener cursoDesactivar;
	private ActionListener usuarioModificar;
	private ActionListener cursoModificar;
	private static ControladorVistaAdmin instance;
	private String idUsuario = null;
	private String nombreCurso;

	public static ControladorVistaAdmin deleteInstance() {
		instance = null;
		return getInstance();
	}

	// para activar los botones deshabilitados
	private ListSelectionListener listener = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			try {
				ControladorVistaAdmin.getInstance().setIdUsuario(ControladorVistaAdmin.getInstance().getVistaAdmin()
						.getTableAdmin()
						.getValueAt(
								ControladorVistaAdmin.getInstance().getVistaAdmin().getTableAdmin().getSelectedRow(), 2)
						.toString());
			} catch (ArrayIndexOutOfBoundsException e2) {
				// TODO: handle exception
			}
			ControladorVistaAdmin.getInstance().getVistaAdmin().getBtnDesactivar().setEnabled(true);// activamos el
																									// boton desactivar
			ControladorVistaAdmin.getInstance().getVistaAdmin().getBtnModificar().setEnabled(true);// activamos el boton
																									// modificar
			ControladorVistaAdmin.getInstance().getVistaAdmin().getBtnReporte().setEnabled(true);
	
		}
	};
	// para activar los botones deshabilitados
	private ListSelectionListener listenerCurso = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			try {
				ControladorVistaAdmin.getInstance().setNombreCurso(ControladorVistaAdmin.getInstance().getVistaAdmin()
						.getTableAdmin()
						.getValueAt(
								ControladorVistaAdmin.getInstance().getVistaAdmin().getTableAdmin().getSelectedRow(), 0)
						.toString());
			} catch (ArrayIndexOutOfBoundsException e2) {
				// TODO: handle exception
			}
			System.out.println(ControladorVistaAdmin.getInstance().getNombreCurso());
			ControladorVistaAdmin.getInstance().getVistaAdmin().getBtnDesactivar().setEnabled(true);// activamos el
																									// boton desactivar
			ControladorVistaAdmin.getInstance().getVistaAdmin().getBtnModificar().setEnabled(true);// activamos el boton
																									// modificar
			ControladorVistaAdmin.getInstance().getVistaAdmin().getBtnReporte().setEnabled(true);
		}
	};
	// para la barra de busqueda
	private KeyListener keyListenerbuscarUsuario = new KeyListener() {

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// aca va la llamada al actualizar tabla
			ControladorVistaAdmin.getInstance().actualizarBusqueda();
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
		}
	};

	private KeyListener keyListenerBuscarCurso = new KeyListener() {

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void keyReleased(KeyEvent e) {
			ControladorVistaAdmin.getInstance()
					.actualizarTabla2(ControladorVistaAdmin.getInstance().getVistaAdmin().getTxtBuscador().getText());
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub

		}
	};

	public ControladorVistaAdmin() {
		super.setVistaAdmin(new VistaAdmin(this));
		this.getVistaAdmin().getTxtBuscador().addKeyListener(this.getKeyListenerbuscarUsuario());// barra de busqueda
		this.getVistaAdmin().getTableAdmin().getSelectionModel().addListSelectionListener(this.getListener());
		this.getVistaAdmin().getBtnReporte().addActionListener(this.getListenerBtnGenerarReporte());
		this.actualizarTabla();
		super.getVistaAdmin().getBtnModificar().removeAll();
		super.getVistaAdmin().getBtnModificar().addActionListener(this.getBtnModificarUsuario());
		this.setUsuarioAgregar(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ControladorVistaAdmin.getInstance().getVistaAdmin().dispose();
				ControladorAgregarUsuario.deleteInstance().getAgregarUsuario().setVisible(true);
			}
		});
		
		this.setCursoAgregar(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ControladorVistaAdmin.getInstance().getVistaAdmin().dispose();
				ControladorAgregarCurso.deleteInstance().getAgregarCurso().setVisible(true);
			}
		});

		this.setUsuarioDesactivar(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object[] options = { "Si", "No" };
				Integer respuesta = JOptionPane.showOptionDialog(getVistaAdmin(), "Desea desactivar el usuario?",
						"Aviso", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options,
						options[0]);
				if (respuesta == 0) {
					String dni = "";
					dni = ControladorVistaAdmin.getInstance().getVistaAdmin().getTable()
							.getValueAt(ControladorVistaAdmin.getInstance().getVistaAdmin().getTable().getSelectedRow(),
									2)
							.toString();
					Usuario.getInstance().cambiarEstadoUsuario(dni);
					mostrarCartel("Se ha desactivado correctamente");
					actualizarTabla();
				}

			}
		});

		this.setCursoDesactivar(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object[] options = { "Si", "No" };
				Integer respuesta = JOptionPane.showOptionDialog(getVistaAdmin(), "Desea desactivar el curso?", "Aviso",
						JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
				if (respuesta == 0) {
					String nombreCurso = "";
					nombreCurso = ControladorVistaAdmin.getInstance().getVistaAdmin().getTable()
							.getValueAt(ControladorVistaAdmin.getInstance().getVistaAdmin().getTable().getSelectedRow(),
									0)
							.toString();
					Curso.getInstance().cambiarEstadoCurso(nombreCurso);
					mostrarCartel("Se ha desactivado correctamente");
					actualizarTabla2("");
				}

			}
		});

		super.getVistaAdmin().setTitle("Usuarios");
		super.getVistaAdmin().getBtnAgregar().addActionListener(getUsuarioAgregar());
		super.getVistaAdmin().getBtnDesactivar().addActionListener(getUsuarioDesactivar());
		super.getVistaAdmin().getBtnModificar().addActionListener(getUsuarioModificar());

	}

	public void actualizarTabla() {
		super.getVistaAdmin().getTable().setModel(Usuario.getInstance().consultaTablaUsuario());
	}

	public void actualizarTabla2(String busq) {
		super.getVistaAdmin().getTable().setModel(Curso.getInstance().consultarTablaCursos(busq));
	}

	public void actualizarBusqueda() {
		System.out.println(super.getVistaAdmin().getTxtBuscador().getText());
		super.getVistaAdmin().getTable()
				.setModel(Usuario.getInstance().buscarUsuario(super.getVistaAdmin().getTxtBuscador().getText()));

	}

	public static ControladorVistaAdmin deleteinstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorVistaAdmin getInstance() {
		if (instance == null) {
			instance = new ControladorVistaAdmin();
		}
		return instance;
	}

	private ActionListener btnModificarUsuario = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorVistaAdmin.getInstance().getVistaAdmin().dispose();
			Usuario u = Usuario.getInstance()
					.ObtenerDatosUsuario(Integer.parseInt(ControladorVistaAdmin.getInstance().idUsuario));
			ControladorModificarUsuario.deleteInstance();
			ControladorModificarUsuario.getInstance().getVmu().getTxtNombre().setText(u.getNombre());
			ControladorModificarUsuario.getInstance().setContrasenia(u.getContrasenia());
			ControladorModificarUsuario.getInstance().setRespuesta(u.getRespuesta());
			ControladorModificarUsuario.getInstance().getVmu().getTxtApellido().setText(u.getApellido());
			ControladorModificarUsuario.getInstance().getVmu().getTxtDni().setText(u.getDNI().toString());
			ControladorModificarUsuario.getInstance().getVmu().getCbxTipoUsuario()
					.setSelectedIndex(Integer.parseInt(u.getTipo()));
			ControladorModificarUsuario.getInstance().getVmu().getTxtTelefono().setText(u.getTelefono());
			ControladorModificarUsuario.getInstance().getVmu().getCbxGenero()
					.setSelectedIndex(Integer.parseInt(u.getGenero()));
			ControladorModificarUsuario.getInstance().getVmu().getCbxLocalidad().setEnabled(true);
			ControladorModificarUsuario.getInstance().getVmu().getCbxLocalidad()
					.setSelectedIndex(Integer.parseInt(u.getLocalidad()));
			ControladorModificarUsuario.getInstance().getVmu().getCbxProvincia()
					.setSelectedIndex(Usuario.getInstance().ObtenerProvincia(u.getLocalidad()));
			ControladorModificarUsuario.getInstance().getVmu().getCbxPregunta()
					.setSelectedIndex(Integer.parseInt(u.getPregunta()));
			ControladorModificarUsuario.getInstance().getVmu().setVisible(true);

		}
	};
	private ActionListener btnModificarCurso = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			Curso c = Curso.getInstance().obtenerCurso(ControladorVistaAdmin.getInstance().getNombreCurso());
			c.setHorario(Curso.getInstance().obtenerHorario(c.getId()));
			c.setProfesor(Curso.getInstance().obtenerProfesor(c.getId()));
			ControladorModificarCurso.getInstance().setCurso(c);
			ControladorModificarCurso.getInstance().setProfesores(c.getProfesor());
			ControladorModificarCurso.getInstance().setHorario(Curso.getInstance().obtenerHorario(c.getId()));
			ControladorModificarCurso.getInstance().MostrarDatos();
			ControladorModificarCurso.getInstance().getVistaModificarCurso().setVisible(true);
			ControladorVistaAdmin.getInstance().getVistaAdmin().setVisible(false);
		}
	};

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(super.getVistaAdmin().getMntmUsuarios())) {
			super.getVistaAdmin().setTitle("Usuarios");
			super.getVistaAdmin().setTitle("Cursos");
			this.getVistaAdmin().getTableAdmin().getSelectionModel().addListSelectionListener(this.getListener());
			this.getVistaAdmin().getTableAdmin().getSelectionModel()
					.removeListSelectionListener(this.getListenerCurso());
			super.getVistaAdmin().getTxtBuscador().removeAll();
			super.getVistaAdmin().getTxtBuscador().addKeyListener(getKeyListenerbuscarUsuario());
			super.getVistaAdmin().getTxtBuscador().setText("");
			super.getVistaAdmin().getBtnAgregar().removeActionListener(getUsuarioAgregar());
			super.getVistaAdmin().getBtnAgregar().addActionListener(getUsuarioAgregar());
			super.getVistaAdmin().getBtnAgregar().removeActionListener(getCursoAgregar());
			super.getVistaAdmin().getBtnDesactivar().removeAll();
			super.getVistaAdmin().getBtnDesactivar().addActionListener(this.getUsuarioDesactivar());
			super.getVistaAdmin().getBtnReporte().setVisible(true);
			for (ActionListener iterable_element : super.getVistaAdmin().getBtnModificar().getActionListeners()) {
				super.getVistaAdmin().getBtnModificar().removeActionListener(iterable_element);
			}
			super.getVistaAdmin().getBtnModificar().addActionListener(this.getBtnModificarUsuario());

		}
		if (e.getSource().equals(super.getVistaAdmin().getMntmCursos())) {
			super.getVistaAdmin().setTitle("Cursos");
			this.getVistaAdmin().getTableAdmin().getSelectionModel().addListSelectionListener(this.getListenerCurso());
			this.getVistaAdmin().getTableAdmin().getSelectionModel().removeListSelectionListener(this.getListener());
			super.getVistaAdmin().getTxtBuscador().removeAll();
			super.getVistaAdmin().getTxtBuscador().addKeyListener(getKeyListenerBuscarCurso());
			super.getVistaAdmin().getTxtBuscador().setText("");
			super.getVistaAdmin().getBtnAgregar().removeActionListener(getCursoAgregar());
			super.getVistaAdmin().getBtnAgregar().addActionListener(getCursoAgregar());
			super.getVistaAdmin().getBtnAgregar().removeActionListener(getUsuarioAgregar());
			super.getVistaAdmin().getBtnDesactivar().removeActionListener(getCursoDesactivar());
			super.getVistaAdmin().getBtnDesactivar().addActionListener(getCursoDesactivar());
			super.getVistaAdmin().getBtnDesactivar().removeActionListener(getUsuarioDesactivar());
			super.getVistaAdmin().getBtnReporte().setVisible(false);
			for (ActionListener iterable_element : super.getVistaAdmin().getBtnModificar().getActionListeners()) {
				super.getVistaAdmin().getBtnModificar().removeActionListener(iterable_element);
			}
			super.getVistaAdmin().getBtnModificar().addActionListener(this.getBtnModificarCurso());
		}
		if (e.getSource().equals(super.getVistaAdmin().getMntmCursos())) {
			this.actualizarTabla2("");
		} else {
			this.actualizarTabla();
		}
		if (e.getSource().equals(super.getVistaAdmin().getMntmSalir())) {
			super.getVistaAdmin().dispose();
			ControladorLogin.deleteInstance().getLogin().setVisible(true);
		}
	}
private ActionListener listenerBtnGenerarReporte = new ActionListener() {
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			Reporte.reportesClientes();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
};
	public ActionListener getUsuarioAgregar() {
		return usuarioAgregar;
	}

	public void setUsuarioAgregar(ActionListener usuarioAgregar) {
		this.usuarioAgregar = usuarioAgregar;
	}

	public ActionListener getCursoAgregar() {
		return cursoAgregar;
	}

	public void setCursoAgregar(ActionListener cursoAgregar) {
		this.cursoAgregar = cursoAgregar;
	}

	public ActionListener getUsuarioDesactivar() {
		return usuarioDesactivar;
	}

	public void setUsuarioDesactivar(ActionListener usuarioDesactivar) {
		this.usuarioDesactivar = usuarioDesactivar;
	}

	public ActionListener getCursoDesactivar() {
		return cursoDesactivar;
	}

	public void setCursoDesactivar(ActionListener cursoDesactivar) {
		this.cursoDesactivar = cursoDesactivar;
	}

	public ActionListener getUsuarioModificar() {
		return usuarioModificar;
	}

	public void setUsuarioModificar(ActionListener usuarioModificar) {
		this.usuarioModificar = usuarioModificar;
	}

	public ActionListener getCursoModificar() {
		return cursoModificar;
	}

	public void setCursoModificar(ActionListener cursoModificar) {
		this.cursoModificar = cursoModificar;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public ListSelectionListener getListener() {
		return listener;
	}

	public void setListener(ListSelectionListener listener) {
		this.listener = listener;
	}

	public KeyListener getKeyListenerbuscarUsuario() {
		return keyListenerbuscarUsuario;
	}

	public void setKeyListenerbuscarUsuario(KeyListener keyListenerbuscarUsuario) {
		this.keyListenerbuscarUsuario = keyListenerbuscarUsuario;
	}

	public KeyListener getKeyListenerBuscarCurso() {
		return keyListenerBuscarCurso;
	}

	public void setKeyListenerBuscarCurso(KeyListener keyListenerBuscarCurso) {
		this.keyListenerBuscarCurso = keyListenerBuscarCurso;
	}

	public ActionListener getBtnModificarUsuario() {
		return btnModificarUsuario;
	}

	public void setBtnModificarUsuario(ActionListener btnMoficarUsuario) {
		this.btnModificarUsuario = btnMoficarUsuario;
	}

	public ActionListener getBtnModificarCurso() {
		return btnModificarCurso;
	}

	public void setBtnModificarCurso(ActionListener btnModificarCurso) {
		this.btnModificarCurso = btnModificarCurso;
	}

	public ListSelectionListener getListenerCurso() {
		return listenerCurso;
	}

	public void setListenerCurso(ListSelectionListener listenerCurso) {
		this.listenerCurso = listenerCurso;
	}

	public String getNombreCurso() {
		return nombreCurso;
	}

	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}

	public ActionListener getListenerBtnGenerarReporte() {
		return listenerBtnGenerarReporte;
	}

	public void setListenerBtnGenerarReporte(ActionListener listenerBtnGenerarReporte) {
		this.listenerBtnGenerarReporte = listenerBtnGenerarReporte;
	}

}
