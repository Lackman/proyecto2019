package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import Modelo.BaseDeDatos;
import Modelo.Curso;
import Modelo.Usuario;
import Vista.VistaSecretaria;

public class ControladorVistaSecretaria extends ControladorPadre {
	private ActionListener usuarioAgregar;
	private ActionListener cursoAgregar;
	private ActionListener usuarioModificar;
	private ActionListener cursoModificar;
	private ActionListener salir;
	private static ControladorVistaSecretaria instance;
	private String idUsuario = null;
	private String idCurso = null;

	public static ControladorVistaSecretaria deleteInstance() {
		instance = null;
		return getInstance();
	}

	private ListSelectionListener listenerCurso = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			try {
				ControladorVistaSecretaria.getInstance()
						.setIdCurso(ControladorVistaSecretaria.getInstance().getVistaSecretaria().getTablaCurso()
								.getValueAt(ControladorVistaSecretaria.getInstance().getVistaSecretaria().getTablaCurso()
										.getSelectedRow(), 0)
								.toString());

			} catch (ArrayIndexOutOfBoundsException e2) {
				// TODO: handle exception
			}
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnModificar().setEnabled(true);
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregarACurso().setEnabled(true);
		}
	};
	private ListSelectionListener listenerUsuario = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			try {
				ControladorVistaSecretaria.getInstance()
						.setIdUsuario(ControladorVistaSecretaria.getInstance().getVistaSecretaria().getTablaAlumno()
								.getValueAt(ControladorVistaSecretaria.getInstance().getVistaSecretaria().getTablaAlumno()
										.getSelectedRow(), 2)
								.toString());

			} catch (ArrayIndexOutOfBoundsException e2) {
				// TODO: handle exception
			}
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnModificar().setEnabled(true);
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregarACurso().setEnabled(true);
			
		}
	};
	private ActionListener listenerBtnModificarCurso = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Curso c = Curso.getInstance().obtenerCurso(ControladorVistaSecretaria.getInstance().getIdCurso());
			c.setHorario(Curso.getInstance().obtenerHorario(c.getId()));
			c.setProfesor(Curso.getInstance().obtenerProfesor(c.getId()));
			ControladorModificarCurso.getInstance().setCurso(c);
			ControladorModificarCurso.getInstance().setProfesores(c.getProfesor());
			ControladorModificarCurso.getInstance().setHorario(Curso.getInstance().obtenerHorario(c.getId()));
			ControladorModificarCurso.getInstance().MostrarDatos();
			ControladorModificarCurso.getInstance().getVistaModificarCurso().setVisible(true);
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().setVisible(false);
		}
	};
private ActionListener listenerBtnModificarUsuario = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println(ControladorVistaSecretaria.getInstance().getIdUsuario());
			Usuario u = Usuario.getInstance()
					.ObtenerDatosUsuario(Integer.parseInt(ControladorVistaSecretaria.getInstance().getIdUsuario()));
			ControladorModificarUsuario.deleteInstance();
			ControladorModificarUsuario.getInstance().getVmu().getTxtNombre().setText(u.getNombre());
			ControladorModificarUsuario.getInstance().setContrasenia(u.getContrasenia());
			ControladorModificarUsuario.getInstance().setRespuesta(u.getRespuesta());
			ControladorModificarUsuario.getInstance().getVmu().getTxtApellido().setText(u.getApellido());
			ControladorModificarUsuario.getInstance().getVmu().getTxtDni().setText(u.getDNI().toString());
			ControladorModificarUsuario.getInstance().getVmu().getCbxTipoUsuario()
					.setSelectedIndex(Integer.parseInt(u.getTipo()));
			ControladorModificarUsuario.getInstance().getVmu().getTxtTelefono().setText(u.getTelefono());
			ControladorModificarUsuario.getInstance().getVmu().getCbxGenero()
					.setSelectedIndex(Integer.parseInt(u.getGenero()));
			ControladorModificarUsuario.getInstance().getVmu().getCbxLocalidad().setEnabled(true);
			ControladorModificarUsuario.getInstance().getVmu().getCbxLocalidad()
					.setSelectedIndex(Integer.parseInt(u.getLocalidad()));
			ControladorModificarUsuario.getInstance().getVmu().getCbxProvincia()
					.setSelectedIndex(Usuario.getInstance().ObtenerProvincia(u.getLocalidad()));
			ControladorModificarUsuario.getInstance().getVmu().getCbxPregunta()
					.setSelectedIndex(Integer.parseInt(u.getPregunta()));
			ControladorModificarUsuario.getInstance().getVmu().setVisible(true);
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().setVisible(false);
			//ControladorVistaSecretaria.getInstance().getVistaSecretaria().getTablaAlumno().setModel(Usuario.getInstance().consultaTablaUsuario());
		}
	};
	private KeyListener keyListenerbuscarUsuario = new KeyListener() {

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// aca va la llamada al actualizar tabla
			//ControladorVistaSecretaria.getInstance().actualizarBusqueda();
			ControladorVistaSecretaria.getInstance().actualizarTabla(ControladorVistaSecretaria.getInstance().getVistaSecretaria().getTxtBuscador().getText());
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
		}
	};
	
	private ActionListener listenerBtnAgregaralCurso = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorAgregaralCurso.getInstance().setIdUsuario(ControladorVistaSecretaria.getInstance().getIdUsuario());
			ControladorAgregaralCurso.getInstance().getAgregaralCurso().getTabladeCurso().requestFocus();
			ControladorAgregaralCurso.getInstance().getAgregaralCurso().setVisible(true);
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().setVisible(false);
		}
	};
	
	public DefaultTableModel obtenerCurso() {
		String consulta = "select nombre_curso, limite_cupos, inicio_curso, finalizacion_curso from curso WHERE estado_curso = true;";
		ResultSet rs = null;
		DefaultTableModel modeloTabla = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}
		};
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);

			String[] titulos = new String[] { "Nombre del curso", "Limite de cupos", "Fecha de Inicio",
					"Fecha de Finalizacion" };
			modeloTabla.setColumnIdentifiers(titulos);
			ResultSetMetaData rsmd = null;
			try {
				rsmd = rs.getMetaData();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer nroCol = 0;

			try {
				nroCol = rsmd.getColumnCount();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				while (rs.next()) {
					Object[] objects = new Object[nroCol];
					for (int i = 0; i < nroCol; i++) {
						objects[i] = rs.getObject(i + 1);
					}
					modeloTabla.addRow(objects);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return modeloTabla;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}
	
	private ActionListener listenerBtnNuevoUsuario = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().dispose();
			ControladorAgregarUsuario.deleteInstance().getAgregarUsuario().setVisible(true);
			//ControladorVistaSecretaria.getInstance().actualizarTabla();
		}
	};
	private ActionListener listenerBtnAgregarCurso = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().dispose();
			ControladorAgregarCurso.deleteInstance().getAgregarCurso().setVisible(true);
		}
	};
	private FocusListener listenerBtnCurso = new FocusListener() {

		@Override
		public void focusGained(FocusEvent e) {
			System.out.println("listenerBtnCurso");
			for (ActionListener i : ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregar().getActionListeners()) {
				ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregar().removeActionListener(i);
			}
			for (ActionListener i : ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnModificar().getActionListeners()) {
				ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnModificar().removeActionListener(i);
			}
			for(ActionListener i : ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregarACurso().getActionListeners()) {
				ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregarACurso().removeActionListener(i);
			}
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregar().addActionListener(ControladorVistaSecretaria.getInstance().getListenerBtnAgregarCurso());
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnModificar().addActionListener(ControladorVistaSecretaria.getInstance().getListenerBtnModificarCurso());
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregarACurso().addActionListener(ControladorVistaSecretaria.getInstance().getListenerBtnAgregaralCurso());
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregarACurso().setEnabled(false);
		}

		@Override
		public void focusLost(FocusEvent e) {
			// TODO Auto-generated method stub
			
		}
	};
private FocusListener listenerBtnUsuario = new FocusListener() {

	@Override
	public void focusGained(FocusEvent e) {
		System.out.println("listenerBtnUsuario");
		for (ActionListener i : ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregar().getActionListeners()) {
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregar().removeActionListener(i);
		}
		for (ActionListener i : ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnModificar().getActionListeners()) {
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnModificar().removeActionListener(i);
		}
		ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnAgregar().addActionListener(ControladorVistaSecretaria.getInstance().getListenerBtnNuevoUsuario());
		ControladorVistaSecretaria.getInstance().getVistaSecretaria().getBtnModificar().addActionListener(ControladorVistaSecretaria.getInstance().getListenerBtnModificarUsuario());
	}

	@Override
	public void focusLost(FocusEvent e) {
		
	}
	};
	public ControladorVistaSecretaria() {
		super.setVistaSecretaria(new VistaSecretaria(this));
		this.getVistaSecretaria().getTablaAlumno().getSelectionModel().addListSelectionListener(this.getListenerUsuario());
		this.getVistaSecretaria().getTablaCurso().getSelectionModel().addListSelectionListener(this.getListenerCurso());
		this.actualizarTabla("");
		//this.actualizarTabla();
		this.getVistaSecretaria().getBtnAgregarACurso().addActionListener(this.getListenerBtnAgregaralCurso());
		ControladorAgregaralCurso.getInstance().getAgregaralCurso().getTabladeCurso().setModel(this.obtenerCurso());
		this.getVistaSecretaria().getBtnAgregar().addActionListener(this.getListenerBtnAgregarCurso());
		super.getVistaSecretaria().getTxtBuscador().addKeyListener(this.getKeyListenerbuscarUsuario());
		super.getVistaSecretaria().setTitle("Secretaria");
		super.getVistaSecretaria().getTablaCurso().addFocusListener(this.getListenerBtnCurso());
		super.getVistaSecretaria().getTablaAlumno().addFocusListener(this.getListenerBtnUsuario());
		super.getVistaSecretaria().getBtnModificar().addActionListener(this.getListenerBtnModificarCurso());
		super.getVistaSecretaria().getBtnAgregar().addActionListener(getListenerBtnNuevoUsuario());
		Usuario.getInstance().ConsultarTablaAlumno("");
		//ControladorVistaSecretaria.getInstance().getVistaSecretaria().getTablaCurso();
	}
	public void actualizarTabla(String busq) {
		super.getVistaSecretaria().getTable().setModel(Curso.getInstance().consultarTablaCursos(busq));
		super.getVistaSecretaria().getTablaAlumno().setModel(Usuario.getInstance().ConsultarTablaAlumno(busq));
		
	}
	//no funca
	public void actualizarBusqueda() {
		super.getVistaSecretaria().getTable().setModel(Usuario.getInstance().buscarUsuario(super.getVistaSecretaria()
				.getTxtBuscador().getText()));
		
		}
	public static ControladorVistaSecretaria deleteinstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorVistaSecretaria getInstance() {
		if (instance == null) {
			instance = new ControladorVistaSecretaria();
		}
		return instance;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(super.getVistaSecretaria().getMntmUsuarios())) {
			super.getVistaSecretaria().setTitle("Usuarios");
			super.getVistaSecretaria().getBtnAgregar().removeActionListener(getUsuarioAgregar());
			super.getVistaSecretaria().getBtnAgregar().addActionListener(getUsuarioAgregar());
			super.getVistaSecretaria().getBtnAgregar().removeActionListener(getCursoAgregar());
			super.getVistaSecretaria().getBtnModificar().removeActionListener(getUsuarioModificar());
			super.getVistaSecretaria().getBtnModificar().addActionListener(getUsuarioModificar());
			super.getVistaSecretaria().getBtnModificar().removeActionListener(getCursoModificar());
		}
		if (e.getSource().equals(super.getVistaSecretaria().getMntmCursos())) {
			super.getVistaSecretaria().setTitle("Cursos");
			super.getVistaSecretaria().getBtnAgregar().removeActionListener(getCursoAgregar());
			super.getVistaSecretaria().getBtnAgregar().addActionListener(getCursoAgregar());
			super.getVistaSecretaria().getBtnAgregar().removeActionListener(getUsuarioAgregar());
			super.getVistaSecretaria().getBtnModificar().removeActionListener(getCursoModificar());
			super.getVistaSecretaria().getBtnModificar().addActionListener(getCursoModificar());
			super.getVistaSecretaria().getBtnModificar().removeActionListener(getUsuarioModificar());
			super.getVistaSecretaria().getTablaCurso().setModel(Curso.getInstance().consultarTablaCursos(""));
		}
		if (e.getSource().equals(super.getVistaSecretaria().getMntmUsuarios())) {
			super.getVistaSecretaria().getTablaAlumno().setModel(Usuario.getInstance().consultaTablaUsuario());;
		}
		if (e.getSource().equals(super.getVistaSecretaria().getMntmSalir())) {
			super.getVistaSecretaria().dispose();
			ControladorLogin.deleteInstance().getLogin().setVisible(true);

		}
	}
	public void actualizarTabla() {
		super.getVistaSecretaria().getTable().setModel(Usuario.getInstance().consultaTablaUsuario());
	}

	public ActionListener getUsuarioAgregar() {
		return usuarioAgregar;
	}

	public void setUsuarioAgregar(ActionListener usuarioAgregar) {
		this.usuarioAgregar = usuarioAgregar;
	}

	public ActionListener getCursoAgregar() {
		return cursoAgregar;
	}

	public void setCursoAgregar(ActionListener cursoAgregar) {
		this.cursoAgregar = cursoAgregar;
	}

	public ActionListener getUsuarioModificar() {
		return usuarioModificar;
	}

	public void setUsuarioModificar(ActionListener usuarioModificar) {
		this.usuarioModificar = usuarioModificar;
	}

	public ActionListener getCursoModificar() {
		return cursoModificar;
	}

	public void setCursoModificar(ActionListener cursoModificar) {
		this.cursoModificar = cursoModificar;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public ActionListener getSalir() {
		return salir;
	}
	public void setSalir(ActionListener salir) {
		this.salir = salir;
	}
	public KeyListener getKeyListenerbuscarUsuario() {
		return keyListenerbuscarUsuario;
	}
	public void setKeyListenerbuscarUsuario(KeyListener keyListenerbuscarUsuario) {
		this.keyListenerbuscarUsuario = keyListenerbuscarUsuario;
	}
	public ActionListener getListenerBtnNuevoUsuario() {
		return listenerBtnNuevoUsuario;
	}
	public void setListenerBtnNuevoUsuario(ActionListener listenerBtnNuevoUsuario) {
		this.listenerBtnNuevoUsuario = listenerBtnNuevoUsuario;
	}
	public ActionListener getListenerBtnAgregarCurso() {
		return listenerBtnAgregarCurso;
	}
	public void setListenerBtnAgregarCurso(ActionListener listenerBtnAgregarCurso) {
		this.listenerBtnAgregarCurso = listenerBtnAgregarCurso;
	}
	public ActionListener getListenerBtnAgregaralCurso() {
		return listenerBtnAgregaralCurso;
	}
	public void setListenerBtnAgregaralCurso(ActionListener listenerBtnAgregaralCurso) {
		this.listenerBtnAgregaralCurso = listenerBtnAgregaralCurso;
	}
	public FocusListener getListenerBtnCurso() {
		return listenerBtnCurso;
	}
	public FocusListener getListenerBtnUsuario() {
		return listenerBtnUsuario;
	}
	public void setListenerBtnCurso(FocusListener listenerBtnCurso) {
		this.listenerBtnCurso = listenerBtnCurso;
	}
	public void setListenerBtnUsuario(FocusListener listenerBtnUsuario) {
		this.listenerBtnUsuario = listenerBtnUsuario;
	}
	public ActionListener getListenerBtnModificarCurso() {
		return listenerBtnModificarCurso;
	}
	public void setListenerBtnModificarCurso(ActionListener listenerBtnModificarCurso) {
		this.listenerBtnModificarCurso = listenerBtnModificarCurso;
	}
	public ActionListener getListenerBtnModificarUsuario() {
		return listenerBtnModificarUsuario;
	}
	public void setListenerBtnModificarUsuario(ActionListener listenerBtnModificarUsuario) {
		this.listenerBtnModificarUsuario = listenerBtnModificarUsuario;
	}
	public ListSelectionListener getListenerCurso() {
		return listenerCurso;
	}
	public void setListenerCurso(ListSelectionListener listenerCurso) {
		this.listenerCurso = listenerCurso;
	}
	public String getIdCurso() {
		return idCurso;
	}
	public void setIdCurso(String idCurso) {
		this.idCurso = idCurso;
	}
	public ListSelectionListener getListenerUsuario() {
		return listenerUsuario;
	}
	public void setListenerUsuario(ListSelectionListener listenerUsuario) {
		this.listenerUsuario = listenerUsuario;
	}


}
