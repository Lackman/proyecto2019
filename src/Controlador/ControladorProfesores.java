package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

import Modelo.TxtCbx;
import Modelo.Usuario;
import Vista.AgregarProfesor;

public class ControladorProfesores extends ControladorPadre {
	private static ControladorProfesores instance;
	private ArrayList<String[]> profesores;
	private ArrayList<String[]> profesoresAux;
	private DefaultTableModel modelo;
	private Boolean arre;
	private ActionListener btnAgregar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			Boolean marca = false;
			Object[] c = new Object[] {
					ControladorProfesores.getInstance().getVistaAgregarProfesor().getTableProfesores()
							.getValueAt(ControladorProfesores.getInstance().getVistaAgregarProfesor()
									.getTableProfesores().getSelectedRow(), 0),
					ControladorProfesores.getInstance().getVistaAgregarProfesor().getTableProfesores()
							.getValueAt(ControladorProfesores.getInstance().getVistaAgregarProfesor()
									.getTableProfesores().getSelectedRow(), 1),
					ControladorProfesores.getInstance().getVistaAgregarProfesor().getTableProfesores()
							.getValueAt(ControladorProfesores.getInstance().getVistaAgregarProfesor()
									.getTableProfesores().getSelectedRow(), 2) };
			if (getProfesores() != null) {
				for (String[] profesor : getProfesores()) {
					if (c[0].equals(profesor[0])) {
						if (c[1].equals(profesor[1])) {
							marca = true;
							mostrarCartel("El profesor ya fue ingresado a este curso");
							break;
						}
					}
				}
			} else if (getProfesores() == null) {
				setProfesores(new ArrayList<>());
			}
			if (!marca) {
				getModelo().addRow(c);
				String dni = c[2].toString();
				getProfesores().add(new String[] { (String) c[0], (String) c[1], dni });
			}

		}
	};
	private ActionListener btnGuardar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				ControladorModificarCurso.getInstance().getCurso().setProfesor(new ArrayList<String[]>());
				for (String[] profesores : getProfesores()) {
					ControladorAgregarCurso.getInstance().getProfesores().add(profesores);
					ControladorModificarCurso.getInstance().getCurso().getProfesor().add(profesores);
				}
			} catch (NullPointerException e) {
				// TODO: handle exception
			}
			getVistaAgregarProfesor().dispose();
		}
	};
	private ActionListener btnCancelar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			for (String[] profesores : getProfesoresAux()) {
				ControladorAgregarCurso.getInstance().getProfesores().add(profesores);
			}
			getVistaAgregarProfesor().dispose();
		}
	};

	private ActionListener btnQuitar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			String[] c = {
					(String) getVistaAgregarProfesor().getTableProfesoresAgregados()
							.getValueAt(getVistaAgregarProfesor().getTableProfesoresAgregados().getSelectedRow(), 0),
					(String) getVistaAgregarProfesor().getTableProfesoresAgregados()
							.getValueAt(getVistaAgregarProfesor().getTableProfesoresAgregados().getSelectedRow(), 1) };
			Integer x = -1;
			for (int i = 0; i < getProfesores().size(); i++) {
				if (c[0].equals(getProfesores().get(i)[0])) {
					if (c[1].equals(getProfesores().get(i)[1])) {
						x = i;
						break;
					}
				}
			}
			getProfesores().remove(getProfesores().get(x));
			getModelo().removeRow(getVistaAgregarProfesor().getTableProfesoresAgregados().getSelectedRow());
		}
	};

	public ControladorProfesores() {
		super.setVistaAgregarProfesor(new AgregarProfesor());
		this.setModelo((DefaultTableModel) this.getVistaAgregarProfesor().getTableProfesoresAgregados().getModel());
		this.setProfesoresAux(new ArrayList<>());
		if (ControladorAgregarCurso.getInstance().getProfesores() != null) {
			this.setArre(true);
			this.setProfesores(new ArrayList<>());
			for (String[] strings : ControladorAgregarCurso.getInstance().getProfesores()) {
				this.getModelo().addRow(new Object[] { strings[0], strings[1] });
				this.getProfesores().add(strings);
				this.getProfesoresAux().add(strings);
			}
			ControladorAgregarCurso.getInstance().setProfesores(new ArrayList<>());
		} else {
			this.setArre(false);
			this.setProfesores(null);
			ControladorAgregarCurso.getInstance().setProfesores(new ArrayList<>());
		}
		try {
			if (ControladorModificarCurso.getInstance().getCurso().getProfesor() != null) {
				this.setArre(true);
				this.setProfesores(ControladorModificarCurso.getInstance().getCurso().getProfesor());
				this.setProfesoresAux(ControladorModificarCurso.getInstance().getCurso().getProfesor());
				ControladorModificarCurso.getInstance().getCurso().setProfesor(new ArrayList<>());
			} else {
				this.setArre(false);
				this.setProfesores(null);
				ControladorModificarCurso.getInstance().getCurso().setProfesor(new ArrayList<>());
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
		}

		super.getVistaAgregarProfesor().getBtnAgregar().addActionListener(getBtnAgregar());
		super.getVistaAgregarProfesor().getBtnGuardar().addActionListener(getBtnGuardar());
		super.getVistaAgregarProfesor().getBtnCancelar().addActionListener(getBtnCancelar());
		super.getVistaAgregarProfesor().getBtnQuitar().addActionListener(getBtnQuitar());
		super.getVistaAgregarProfesor().getTableProfesores().setModel(Usuario.getInstance().consultarTablaProfesor());
	}

	public static ControladorProfesores deleteInstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorProfesores getInstance() {
		if (instance == null) {
			instance = new ControladorProfesores();
		}
		return instance;
	}

	public ActionListener getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(ActionListener btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public ArrayList<String[]> getProfesores() {
		return profesores;
	}

	public void setProfesores(ArrayList<String[]> profesores) {
		this.profesores = profesores;
	}

	public ActionListener getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(ActionListener btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public DefaultTableModel getModelo() {
		return modelo;
	}

	public void setModelo(DefaultTableModel modelo) {
		this.modelo = modelo;
	}

	public ActionListener getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(ActionListener btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public Boolean getArre() {
		return arre;
	}

	public void setArre(Boolean arre) {
		this.arre = arre;
	}

	public ActionListener getBtnQuitar() {
		return btnQuitar;
	}

	public void setBtnQuitar(ActionListener btnQuitar) {
		this.btnQuitar = btnQuitar;
	}

	public ArrayList<String[]> getProfesoresAux() {
		return profesoresAux;
	}

	public void setProfesoresAux(ArrayList<String[]> profesoresAux) {
		this.profesoresAux = profesoresAux;
	}

}
