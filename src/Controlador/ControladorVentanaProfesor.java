package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import Modelo.Txt;
import Modelo.BaseDeDatos;
import Modelo.Reporte;
import Modelo.Usuario;
import Vista.VentanaProfesor;

public class ControladorVentanaProfesor extends ControladorPadre {
	private static ControladorVentanaProfesor instance;
	private String idUsuario;
	private ActionListener notasAlumnos;
	private String materia;
	public static ControladorVentanaProfesor deleteinstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorVentanaProfesor getInstance() {
		if (instance == null) {
			instance = new ControladorVentanaProfesor();
		}
		return instance;
	}

	// habilitar el boton desactivado
	private ListSelectionListener listener = new ListSelectionListener() {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			try {
				ControladorVentanaProfesor.getInstance()
						.setIdUsuario(ControladorVentanaProfesor.getInstance().getVentanaProfesor().getTable()
								.getValueAt(ControladorVentanaProfesor.getInstance().getVentanaProfesor().getTable()
										.getSelectedRow(), 2)
								.toString());
			} catch (ArrayIndexOutOfBoundsException e2) {
				// TODO: handle exception
			}
			ControladorVentanaProfesor.getInstance().getVentanaProfesor().getBtnCalificar().setEnabled(true);
		}
	};

	private ActionListener btnCalificar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorVentanaProfesor.getInstance().getVentanaProfesor().setVisible(true);
			;
			Usuario u = Usuario.getInstance()
					.ObtenerDatosUsuario(Integer.parseInt(ControladorVentanaProfesor.getInstance().idUsuario));
			ControladorNotas.getInstance().getVistaNotas().setVisible(true);
			ControladorNotas.getInstance().getVistaNotas().getTextCurso().setText(ControladorVentanaProfesor
					.getInstance().getVentanaProfesor().getComboBox().getSelectedItem().toString());
			ControladorNotas.getInstance().getVistaNotas().getTextNombre().setText(u.getNombre());
			ControladorNotas.getInstance().getVistaNotas().getTextApellido().setText(u.getApellido());
			ControladorNotas.getInstance().setDni(u.getDNI());
		}
	};

	public ControladorVentanaProfesor() {
		super.setVentanaProfesor(new VentanaProfesor(this));
		super.getVentanaProfesor().getTable().getSelectionModel().addListSelectionListener(this.getListener());
		super.getVentanaProfesor().getMntmSalir().addActionListener(this.getMntSalir());
		this.getVentanaProfesor().getComboBox().addItemListener(this.getLisenerCbx());
		this.getVentanaProfesor().getComboBox().setModel(this.creadorModelos());
		this.actualizarTabla("0");
		super.getVentanaProfesor().setTitle("Profesor");
		super.getVentanaProfesor().getBtnCalificar().removeAll();
		super.getVentanaProfesor().getBtnCalificar().addActionListener(this.getBtnCalificar());
		this.getVentanaProfesor().getComboBox().addItemListener(this.getCbxCursos());
		this.getVentanaProfesor().getBtnReporte().addActionListener(this.getListenerBtnGenerarReporte());
		this.setNotasAlumnos(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ControladorVentanaProfesor.getInstance().getVentanaProfesor().dispose();
				ControladorNotas.deleteInstance().getVistaNotas().setVisible(true);
				System.out.println("Hola");

			}
		});

	}
	
	private ActionListener listenerBtnGenerarReporte = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Reporte.ReporteAlumnos(ControladorVentanaProfesor.getInstance().getMateria());
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	};
	
	private ItemListener cbxCursos = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			//for (Txt txt : getVentanaProfesor().getCampos()) {
				if (getVentanaProfesor().getTxtMateria().ConsisitirCampo()) {
					ControladorVentanaProfesor.getInstance().getVentanaProfesor().getBtnReporte().setEnabled(true);
				}else {
					ControladorVentanaProfesor.getInstance().getVentanaProfesor().getBtnReporte().setEnabled(false);
					
				}

			}
		//}
	};

	@SuppressWarnings("unchecked")
	public DefaultComboBoxModel<Object> creadorModelos() {
		String[] str = Usuario.getInstance().consulColumna2();
		str[0] = "Selecciona un Curso";
		return new DefaultComboBoxModel<Object>(str);
	}

	private ItemListener lisenerCbx = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			System.out.println(e.getItem().toString());
			ControladorVentanaProfesor.getInstance().actualizarTabla(e.getItem().toString());
			ControladorVentanaProfesor.getInstance().setMateria(e.getItem().toString());
		}
		
	};

	public DefaultTableModel ObtenerDatosUsuario(Integer idcurso) {
		String consulta = "select distinct nombre, apellido, dni, curso_alumno.nota "
				+ "from curso_alumno inner join usuario on curso_alumno.id_alumno  = usuario.dni " + "where id_curso ="
				+ idcurso;
		
		ResultSet rs = null;
		DefaultTableModel modeloTabla = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int arg0, int arg1) {
				return false;
			}
		};
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);
			String[] titulos = new String[] { "NOMBRE", "APELLIDO", "D.N.I", "NOTAS" };
			modeloTabla.setColumnIdentifiers(titulos);
			ResultSetMetaData rsmd = null;
			try {
				rsmd = rs.getMetaData();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer nroCol = 0;

			try {
				nroCol = rsmd.getColumnCount();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				while (rs.next()) {
					Object[] objects = new Object[nroCol];
					for (int i = 0; i < nroCol; i++) {
						objects[i] = rs.getObject(i + 1);
					}
					modeloTabla.addRow(objects);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return modeloTabla;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
		
	}

	public Integer ObtenerIdCurso(String nombreCurso) {
		String consulta = "select id_curso from curso where nombre_curso = '" + nombreCurso + "';";
		ResultSet rs = null;
		Integer i = null;
		try {
			rs = BaseDeDatos.getInstance().consultar(consulta);
			if (rs.next()) {
				i = Integer.parseInt(rs.getObject(1).toString());
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return i;
	}

	public void actualizarTabla(String item) {
		// Consultas con el nombre a la bdd para conseguir el iddel curso
		super.getVentanaProfesor().getTable().setModel(this.ObtenerDatosUsuario(this.ObtenerIdCurso(item)));

	}

	private ActionListener mntSalir = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			ControladorVentanaProfesor.getInstance().getVentanaProfesor().dispose();
			ControladorLogin.deleteInstance().getLogin().setVisible(true);

		}
	};

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public ListSelectionListener getListener() {
		return listener;
	}

	public void setListener(ListSelectionListener listener) {
		this.listener = listener;
	}

	public ItemListener getLisenerCbx() {
		return lisenerCbx;
	}

	public void setLisenerCbx(ItemListener lisenerCbx) {
		this.lisenerCbx = lisenerCbx;
	}

	public ActionListener getMntSalir() {
		return mntSalir;
	}

	public void setMntSalir(ActionListener mntSalir) {
		this.mntSalir = mntSalir;
	}

	public ActionListener getNotasAlumnos() {
		return notasAlumnos;
	}

	public void setNotasAlumnos(ActionListener notasAlumnos) {
		this.notasAlumnos = notasAlumnos;
	}

	public ActionListener getBtnCalificar() {
		return btnCalificar;
	}

	public void setBtnCalificar(ActionListener btnCalificar) {
		this.btnCalificar = btnCalificar;
	}

	public ItemListener getCbxCursos() {
		return cbxCursos;
	}

	public void setCbxCursos(ItemListener cbxCursos) {
		this.cbxCursos = cbxCursos;
	}

	public ActionListener getListenerBtnGenerarReporte() {
		return listenerBtnGenerarReporte;
	}

	public void setListenerBtnGenerarReporte(ActionListener listenerBtnGenerarReporte) {
		this.listenerBtnGenerarReporte = listenerBtnGenerarReporte;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

}
