package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Modelo.BaseDeDatos;
import Modelo.Curso;
import Modelo.Usuario;
import Vista.VentanaAgregaralCurso;

public class ControladorAgregaralCurso extends ControladorPadre {

	private static ControladorAgregaralCurso instance;
	private String nombreCurso = null;
	private String idUsuario = null;
	private VentanaAgregaralCurso agregaralCurso;
	private FocusListener listenerFoco = new FocusListener() {

		@Override
		public void focusLost(FocusEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void focusGained(FocusEvent e) {
			actualizarTabla();
		}
	};
	private WindowListener wl = new WindowListener() {
		
		@Override
		public void windowOpened(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowIconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowDeiconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowDeactivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowClosing(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
			ControladorAgregaralCurso.getInstance().getAgregaralCurso().setVisible(false);
			ControladorVistaSecretaria.getInstance().getVistaSecretaria().setVisible(true);
		}
		
		@Override
		public void windowClosed(WindowEvent arg0) {
		}
		
		@Override
		public void windowActivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	};
	private ActionListener usuarioaAgregar = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (ControladorAgregaralCurso.getInstance().getNombreCurso() == null) {
				JOptionPane.showMessageDialog(null, "Seleccione un curso de la tabla");
			} else {
				if (insertarUsuarioCurso(ControladorAgregaralCurso.getInstance().getNombreCurso(),
						ControladorAgregaralCurso.getInstance().getIdUsuario())) {
					mostrarCartel("Se agrego al curso correctamente");
					ControladorAgregaralCurso.getInstance().getAgregaralCurso().setVisible(false);
					ControladorVistaSecretaria.getInstance().getVistaSecretaria().setVisible(true);
				}

			}
		}
	};

	public Boolean insertarUsuarioCurso(String nombreCurso, String idAlumno) {
		/*String xcc = "SELECT id_curso from curso_alumno WHERE id_alumno = " + idAlumno;
		String xc = "SELECT id_curso from curso WHERE nombre_curso = '"+nombreCurso+"'";
		System.out.println(xcc);
		ResultSet rs = null;
		Integer i = null;
		Integer id = null;
		try {
			rs = BaseDeDatos.getInstance().consultar(xcc);
			if (rs.next()) {
				i = rs.getInt(1);
				System.out.println(i);
			}
			rs = BaseDeDatos.getInstance().consultar(xc);
			if (rs.next()) {
				id = rs.getInt(1);
				System.out.println(id);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (i != id) {*/
		String xcc = "SELECT curso_alumno.id_curso, estado from curso_alumno inner join curso on curso.id_curso = curso_alumno.id_curso WHERE (id_alumno = " + idAlumno+") and (estado = true) and (nombre_curso = '"+nombreCurso+"');";
		System.out.println(xcc);
		ResultSet rs = null;
		String est = "";
		Integer i = 0;
		try {
			rs = BaseDeDatos.getInstance().consultar(xcc);
			if (rs.next()) {
				i = rs.getInt(1);
				est = rs.getObject(2).toString();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if ((i == 0) && !(est.equals("true"))) {
			String c = "SELECT id_curso, limite_cupos FROM curso WHERE nombre_curso = '" + nombreCurso
					+ "' and estado_curso = true";
			System.out.println(c);
			Integer limite_cupos = null;
			String id_curso = "";
			try {
				rs = BaseDeDatos.getInstance().consultar(c);
				if (rs.next()) {
					id_curso = rs.getObject(1).toString();
					limite_cupos = rs.getInt(2);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (!(limite_cupos <= 0)) {
				String consulta = "INSERT INTO public.curso_alumno(id_alumno, id_curso,nota ,estado) VALUES (" + idAlumno + ","
						+ id_curso +", '' ,true )";
				System.out.println(consulta);
				try {
					BaseDeDatos.getInstance().insertar(consulta);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String consultando = "UPDATE public.curso SET limite_cupos= (limite_cupos) -1 where id_curso ="
						+ id_curso + ";";
				try {
					BaseDeDatos.getInstance().insertar(consultando);
				} catch (SQLException e) {
				}
				return true;
			} else {
				super.mostrarCartel("No se puede ingresarr mas alumnos al curso.");
			}
		}
		super.mostrarCartel("El alumno ya esta inscripto al curso");
		return false;
	}

	public static ControladorAgregaralCurso deleteInstance() {
		instance = null;
		return getInstance();
	}

	public ControladorAgregaralCurso() {
		this.setAgregaralCurso(new VentanaAgregaralCurso(this));
		this.getAgregaralCurso().getBtnAgregar().addActionListener(this.getUsuarioaAgregar());
		this.getAgregaralCurso().getTabladeCurso().getSelectionModel().addListSelectionListener(this.getListener());
		this.getAgregaralCurso().getTabladeCurso().addFocusListener(this.getListenerFoco());
		this.getAgregaralCurso().addWindowListener(this.getWl());
	}

	// para activar los botones deshabilitados
	private ListSelectionListener listener = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			try {
				ControladorAgregaralCurso.getInstance()
						.setNombreCurso(ControladorAgregaralCurso.getInstance().getAgregaralCurso().getTabladeCurso()
								.getValueAt(ControladorAgregaralCurso.getInstance().getAgregaralCurso()
										.getTabladeCurso().getSelectedRow(), 0)
								.toString());
			} catch (ArrayIndexOutOfBoundsException e2) {
				// TODO: handle exception
			}
			ControladorAgregaralCurso.getInstance().getAgregaralCurso().getBtnAgregar().setEnabled(true);// activamos el

		}
	};

	public void actualizarTabla() {
		this.getAgregaralCurso().getTabladeCurso().setModel(ControladorVistaSecretaria.getInstance().obtenerCurso());

	}

	public static ControladorAgregaralCurso deleteinstance() {
		instance = null;
		return getInstance();
	}

	public static ControladorAgregaralCurso getInstance() {
		if (instance == null) {
			instance = new ControladorAgregaralCurso();
		}
		return instance;
	}

	public ActionListener getUsuarioaAgregar() {
		return usuarioaAgregar;
	}

	public void setUsuarioaAgregar(ActionListener usuarioaAgregar) {
		this.usuarioaAgregar = usuarioaAgregar;
	}

	public String getNombreCurso() {
		return nombreCurso;
	}

	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public VentanaAgregaralCurso getAgregaralCurso() {
		return agregaralCurso;
	}

	public void setAgregaralCurso(VentanaAgregaralCurso agregaralCurso) {
		this.agregaralCurso = agregaralCurso;
	}

	public ListSelectionListener getListener() {
		return listener;
	}

	public void setListener(ListSelectionListener listener) {
		this.listener = listener;
	}

	public FocusListener getListenerFoco() {
		return listenerFoco;
	}

	public void setListenerFoco(FocusListener listenerFoco) {
		this.listenerFoco = listenerFoco;
	}

	public WindowListener getWl() {
		return wl;
	}

	public void setWl(WindowListener wl) {
		this.wl = wl;
	}

}
